﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class EmployeeInfoModel
    {
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployeePicture { get; set; }
    }
}
