﻿using DMC.HRM.Entities;
using DMC.HRM.Entities.Models;
using DMC.HRM.UI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.Entities
{
    public class HrmTranEmployeeDependantModel : EmployeeInfoModel
    {
        public HrmTranEmployeeDependantModel()
        {
            Genders = new List<HrmSysGender>();
            AddressTypes = new List<HrmSysAddressType>();
            DependantTypes = new List<HrmSysDependantType>();
        }
        public int SNo { get; set; }       
        public long EmployeeDependantId { get; set; }
        [Required(ErrorMessage = "*Required")]
        public long? DependantTypeId { get; set; }
        public string DependantTypeNameEn { get; set; }
        public string DependantTypeNameAr { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string FirstNameEn { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameEn { get; set; }
        public string LastNameAr { get; set; }
        public long? GenderId { get; set; }
        public string GenderNameEn { get; set; }
        public string GenderNameAr { get; set; }
        public DateTime? DateOfBirthEn { get; set; }
        public string DateOfBirthAr { get; set; }
        public long? PhoneNumber { get; set; }
        public bool? EmergencyContact { get; set; }
        public long? EmployeeAddressId { get; set; }
        public long? AddressTypeId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string IqamaNumber { get; set; }
        public string NameInIqamaEn { get; set; }
        public string NameInIqamaAr { get; set; }
        public DateTime? IqamaExpiryDateEn { get; set; }
        public string IqamaExpiryDateAr { get; set; }
        public DateTime? PassportExpiryDateEn { get; set; }
        public string PassportExpiryDateAr { get; set; }
        public int? EligibleForTicket { get; set; }
        public bool? EligibleForTicketBool { get; set; }
        public int? EligibleForExitReentry { get; set; }
        public int? EligibleForInsurance { get; set; }
        public int? EligibleForSchooling { get; set; }
        public int? UseEmployeeAddress { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public List<HrmSysAddressType> AddressTypes { get; set; }
        public List<HrmSysGender> Genders { get; set; }
        public List<HrmSysDependantType> DependantTypes { get; set; }
        public List<DropDownListItem> YesOrNoDropDown { get; set; }
    }
}
