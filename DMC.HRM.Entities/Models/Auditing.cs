﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class Auditing
    {
        public string DateTime { get; set; }
        public string Action { get; set; }
        public string TableName { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
        public string OldValue { get; set; }
    }
}
