﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class DCMHrmContext : DbContext
    {
        public DCMHrmContext()
        {
        }

        public DCMHrmContext(DbContextOptions<DCMHrmContext> options) : base(options) { }

        public virtual DbSet<Audit> Audits { get; set; }
        public virtual DbSet<Auditing> Auditings { get; set; }
        public virtual DbSet<Delete> Deletes { get; set; }
        public virtual DbSet<DeleteEmployeeTable> DeleteEmployeeTables { get; set; }
        public virtual DbSet<HrmAuditServiceRequestStage> HrmAuditServiceRequestStages { get; set; }
        public virtual DbSet<HrmDefApprovalAuthorityMatrix> HrmDefApprovalAuthorityMatrices { get; set; }
        public virtual DbSet<HrmDefAsset> HrmDefAssets { get; set; }
        public virtual DbSet<HrmDefAssetAudit> HrmDefAssetAudits { get; set; }
        public virtual DbSet<HrmDefAssetType> HrmDefAssetTypes { get; set; }
        public virtual DbSet<HrmDefAssetTypeAudit> HrmDefAssetTypeAudits { get; set; }
        public virtual DbSet<HrmDefAttendance> HrmDefAttendances { get; set; }
        public virtual DbSet<HrmDefBankDetail> HrmDefBankDetails { get; set; }
        public virtual DbSet<HrmDefBranch> HrmDefBranches { get; set; }
        public virtual DbSet<HrmDefBusinessUnit> HrmDefBusinessUnits { get; set; }
        public virtual DbSet<HrmDefCity> HrmDefCities { get; set; }
        public virtual DbSet<HrmDefCompany> HrmDefCompanies { get; set; }
        public virtual DbSet<HrmDefContractType> HrmDefContractTypes { get; set; }
        public virtual DbSet<HrmDefCostCenter> HrmDefCostCenters { get; set; }
        public virtual DbSet<HrmDefCurrency> HrmDefCurrencies { get; set; }
        public virtual DbSet<HrmDefCurrentJob> HrmDefCurrentJobs { get; set; }
        public virtual DbSet<HrmDefCustomer> HrmDefCustomers { get; set; }
        public virtual DbSet<HrmDefDepartment> HrmDefDepartments { get; set; }
        public virtual DbSet<HrmDefDivision> HrmDefDivisions { get; set; }
        public virtual DbSet<HrmDefDocumentType> HrmDefDocumentTypes { get; set; }
        public virtual DbSet<HrmDefEmployeePayrolGroupMap> HrmDefEmployeePayrolGroupMaps { get; set; }
        public virtual DbSet<HrmDefEmployeeWeekDay> HrmDefEmployeeWeekDays { get; set; }
        public virtual DbSet<HrmDefFlightClass> HrmDefFlightClasses { get; set; }
        public virtual DbSet<HrmDefFlightType> HrmDefFlightTypes { get; set; }
        public virtual DbSet<HrmDefFrequency> HrmDefFrequencies { get; set; }
        public virtual DbSet<HrmDefGardeningLeave> HrmDefGardeningLeaves { get; set; }
        public virtual DbSet<HrmDefGrade> HrmDefGrades { get; set; }
        public virtual DbSet<HrmDefGrade1> HrmDefGrade1s { get; set; }
        public virtual DbSet<HrmDefHolidayMaster> HrmDefHolidayMasters { get; set; }
        public virtual DbSet<HrmDefInsuranceClass> HrmDefInsuranceClasses { get; set; }
        public virtual DbSet<HrmDefInsuranceProvider> HrmDefInsuranceProviders { get; set; }
        public virtual DbSet<HrmDefInsuranceType> HrmDefInsuranceTypes { get; set; }
        public virtual DbSet<HrmDefInvoiceStatus> HrmDefInvoiceStatuses { get; set; }
        public virtual DbSet<HrmDefLegalEntity> HrmDefLegalEntities { get; set; }
        public virtual DbSet<HrmDefLegalEntity1> HrmDefLegalEntity1s { get; set; }
        public virtual DbSet<HrmDefLineManager> HrmDefLineManagers { get; set; }
        public virtual DbSet<HrmDefListOfHoliday> HrmDefListOfHolidays { get; set; }
        public virtual DbSet<HrmDefLoanType> HrmDefLoanTypes { get; set; }
        public virtual DbSet<HrmDefMenu> HrmDefMenus { get; set; }
        public virtual DbSet<HrmDefOutofOfficeReason> HrmDefOutofOfficeReasons { get; set; }
        public virtual DbSet<HrmDefPackage> HrmDefPackages { get; set; }
        public virtual DbSet<HrmDefPayType> HrmDefPayTypes { get; set; }
        public virtual DbSet<HrmDefPayTypeFormulae> HrmDefPayTypeFormulaes { get; set; }
        public virtual DbSet<HrmDefPayTypeGroup> HrmDefPayTypeGroups { get; set; }
        public virtual DbSet<HrmDefPayTypeSetting> HrmDefPayTypeSettings { get; set; }
        public virtual DbSet<HrmDefPaymentMethod> HrmDefPaymentMethods { get; set; }
        public virtual DbSet<HrmDefPaymentType> HrmDefPaymentTypes { get; set; }
        public virtual DbSet<HrmDefPayrollGroup> HrmDefPayrollGroups { get; set; }
        public virtual DbSet<HrmDefPayrollPeriod> HrmDefPayrollPeriods { get; set; }
        public virtual DbSet<HrmDefPersonType> HrmDefPersonTypes { get; set; }
        public virtual DbSet<HrmDefPosition> HrmDefPositions { get; set; }
        public virtual DbSet<HrmDefProduct> HrmDefProducts { get; set; }
        public virtual DbSet<HrmDefProductType> HrmDefProductTypes { get; set; }
        public virtual DbSet<HrmDefProject> HrmDefProjects { get; set; }
        public virtual DbSet<HrmDefReport> HrmDefReports { get; set; }
        public virtual DbSet<HrmDefReportCommonFilter> HrmDefReportCommonFilters { get; set; }
        public virtual DbSet<HrmDefReportFilter> HrmDefReportFilters { get; set; }
        public virtual DbSet<HrmDefReportMappingByRole> HrmDefReportMappingByRoles { get; set; }
        public virtual DbSet<HrmDefReportMaster> HrmDefReportMasters { get; set; }
        public virtual DbSet<HrmDefReportOutputParam> HrmDefReportOutputParams { get; set; }
        public virtual DbSet<HrmDefRole> HrmDefRoles { get; set; }
        public virtual DbSet<HrmDefRoleMenuMapping> HrmDefRoleMenuMappings { get; set; }
        public virtual DbSet<HrmDefServiceRequestProcessStage> HrmDefServiceRequestProcessStages { get; set; }
        public virtual DbSet<HrmDefServiceRequestType> HrmDefServiceRequestTypes { get; set; }
        public virtual DbSet<HrmDefServiceRequestType2> HrmDefServiceRequestType2s { get; set; }
        public virtual DbSet<HrmDefSite> HrmDefSites { get; set; }
        public virtual DbSet<HrmDefSponsor> HrmDefSponsors { get; set; }
        public virtual DbSet<HrmDefSponsor1> HrmDefSponsor1s { get; set; }
        public virtual DbSet<HrmDefTax> HrmDefTaxes { get; set; }
        public virtual DbSet<HrmDefTicketBalanceSetup> HrmDefTicketBalanceSetups { get; set; }
        public virtual DbSet<HrmDefTicketFrequency> HrmDefTicketFrequencies { get; set; }
        public virtual DbSet<HrmDefTicketSetup> HrmDefTicketSetups { get; set; }
        public virtual DbSet<HrmDefTicketType> HrmDefTicketTypes { get; set; }
        public virtual DbSet<HrmDefTravelType> HrmDefTravelTypes { get; set; }
        public virtual DbSet<HrmDefUnitType> HrmDefUnitTypes { get; set; }
        public virtual DbSet<HrmDefUser> HrmDefUsers { get; set; }
        public virtual DbSet<HrmDefVacationType> HrmDefVacationTypes { get; set; }
        public virtual DbSet<HrmDefValueType> HrmDefValueTypes { get; set; }
        public virtual DbSet<HrmSysAddressType> HrmSysAddressTypes { get; set; }
        public virtual DbSet<HrmSysApprovalAuthority> HrmSysApprovalAuthorities { get; set; }
        public virtual DbSet<HrmSysBloodGroup> HrmSysBloodGroups { get; set; }
        public virtual DbSet<HrmSysContractType> HrmSysContractTypes { get; set; }
        public virtual DbSet<HrmSysCountry> HrmSysCountries { get; set; }
        public virtual DbSet<HrmSysCourseType> HrmSysCourseTypes { get; set; }
        public virtual DbSet<HrmSysDependantType> HrmSysDependantTypes { get; set; }
        public virtual DbSet<HrmSysDocumentOwner> HrmSysDocumentOwners { get; set; }
        public virtual DbSet<HrmSysEducationDegree> HrmSysEducationDegrees { get; set; }
        public virtual DbSet<HrmSysEmployeeStatus> HrmSysEmployeeStatuses { get; set; }
        public virtual DbSet<HrmSysEndOfServiceType> HrmSysEndOfServiceTypes { get; set; }
        public virtual DbSet<HrmSysFlightType> HrmSysFlightTypes { get; set; }
        public virtual DbSet<HrmSysGender> HrmSysGenders { get; set; }
        public virtual DbSet<HrmSysGosiConfiguration> HrmSysGosiConfigurations { get; set; }
        public virtual DbSet<HrmSysGosiFlag> HrmSysGosiFlags { get; set; }
        public virtual DbSet<HrmSysLeaveType> HrmSysLeaveTypes { get; set; }
        public virtual DbSet<HrmSysMaritalStatus> HrmSysMaritalStatuses { get; set; }
        public virtual DbSet<HrmSysModule> HrmSysModules { get; set; }
        public virtual DbSet<HrmSysNationality> HrmSysNationalities { get; set; }
        public virtual DbSet<HrmSysNoticePeriod> HrmSysNoticePeriods { get; set; }
        public virtual DbSet<HrmSysPaymentMethod> HrmSysPaymentMethods { get; set; }
        public virtual DbSet<HrmSysPhoneType> HrmSysPhoneTypes { get; set; }
        public virtual DbSet<HrmSysProbationPeriod> HrmSysProbationPeriods { get; set; }
        public virtual DbSet<HrmSysPurpouse> HrmSysPurpouses { get; set; }
        public virtual DbSet<HrmSysReligion> HrmSysReligions { get; set; }
        public virtual DbSet<HrmSysRuleEngine> HrmSysRuleEngines { get; set; }
        public virtual DbSet<HrmSysScreen> HrmSysScreens { get; set; }
        public virtual DbSet<HrmSysSystemConfiguration> HrmSysSystemConfigurations { get; set; }
        public virtual DbSet<HrmSysTitle> HrmSysTitles { get; set; }
        public virtual DbSet<HrmTranAccountInfo> HrmTranAccountInfos { get; set; }
        public virtual DbSet<HrmTranAccountInfoAudit> HrmTranAccountInfoAudits { get; set; }
        public virtual DbSet<HrmTranContactInfo> HrmTranContactInfos { get; set; }
        public virtual DbSet<HrmTranContactInfoAudit> HrmTranContactInfoAudits { get; set; }
        public virtual DbSet<HrmTranContract> HrmTranContracts { get; set; }
        public virtual DbSet<HrmTranContractAudit> HrmTranContractAudits { get; set; }
        public virtual DbSet<HrmTranDocumentsInfo> HrmTranDocumentsInfos { get; set; }
        public virtual DbSet<HrmTranDocumentsInfoAudit> HrmTranDocumentsInfoAudits { get; set; }
        public virtual DbSet<HrmTranEducationInfo> HrmTranEducationInfos { get; set; }
        public virtual DbSet<HrmTranEducationInfoAudit> HrmTranEducationInfoAudits { get; set; }
        public virtual DbSet<HrmTranEmployee> HrmTranEmployees { get; set; }
        public virtual DbSet<HrmTranEmployeeAddress> HrmTranEmployeeAddresses { get; set; }
        public virtual DbSet<HrmTranEmployeeAddressAudit> HrmTranEmployeeAddressAudits { get; set; }
        public virtual DbSet<HrmTranEmployeeAsset> HrmTranEmployeeAssets { get; set; }
        public virtual DbSet<HrmTranEmployeeAssetAudit> HrmTranEmployeeAssetAudits { get; set; }
        public virtual DbSet<HrmTranEmployeeAssetMap> HrmTranEmployeeAssetMaps { get; set; }
        public virtual DbSet<HrmTranEmployeeAssetMapAudit> HrmTranEmployeeAssetMapAudits { get; set; }
        public virtual DbSet<HrmTranEmployeeCancelRequestDetail> HrmTranEmployeeCancelRequestDetails { get; set; }
        public virtual DbSet<HrmTranEmployeeDependant> HrmTranEmployeeDependants { get; set; }
        public virtual DbSet<HrmTranEmployeeDependantAudit> HrmTranEmployeeDependantAudits { get; set; }
        public virtual DbSet<HrmTranEmployeeEndOfServiceDetail> HrmTranEmployeeEndOfServiceDetails { get; set; }
        public virtual DbSet<HrmTranEmployeeLoanRequestDetail> HrmTranEmployeeLoanRequestDetails { get; set; }
        public virtual DbSet<HrmTranEmployeeMonthTimeSheet> HrmTranEmployeeMonthTimeSheets { get; set; }
        public virtual DbSet<HrmTranEmployeeNormalServiceRequestLeaveDetail> HrmTranEmployeeNormalServiceRequestLeaveDetails { get; set; }
        public virtual DbSet<HrmTranEmployeeOutofOfficeReasonDetail> HrmTranEmployeeOutofOfficeReasonDetails { get; set; }
        public virtual DbSet<HrmTranEmployeePayrollPackage> HrmTranEmployeePayrollPackages { get; set; }
        public virtual DbSet<HrmTranEmployeePayrollPackageAudit> HrmTranEmployeePayrollPackageAudits { get; set; }
        public virtual DbSet<HrmTranEmployeePhone> HrmTranEmployeePhones { get; set; }
        public virtual DbSet<HrmTranEmployeePhoneAudit> HrmTranEmployeePhoneAudits { get; set; }
        public virtual DbSet<HrmTranEmployeeScheduleManagement> HrmTranEmployeeScheduleManagements { get; set; }
        public virtual DbSet<HrmTranEmployeeServiceRequest> HrmTranEmployeeServiceRequests { get; set; }
        public virtual DbSet<HrmTranEmployeeTicketServiceRequestDetail> HrmTranEmployeeTicketServiceRequestDetails { get; set; }
        public virtual DbSet<HrmTranEmployeeTimeSheetCalculation> HrmTranEmployeeTimeSheetCalculations { get; set; }
        public virtual DbSet<HrmTranEmployeeTravelRequest> HrmTranEmployeeTravelRequests { get; set; }
        public virtual DbSet<HrmTranEmployeeTravelServiceRequestDetail> HrmTranEmployeeTravelServiceRequestDetails { get; set; }
        public virtual DbSet<HrmTranEmployeeVacationBalanceUpdate> HrmTranEmployeeVacationBalanceUpdates { get; set; }
        public virtual DbSet<HrmTranEmployeeVacationServiceRequestDetail> HrmTranEmployeeVacationServiceRequestDetails { get; set; }
        public virtual DbSet<HrmTranEmployeeVacationServiceRequestLeaveDetail> HrmTranEmployeeVacationServiceRequestLeaveDetails { get; set; }
        public virtual DbSet<HrmTranEmployeeVsrreturnDetail> HrmTranEmployeeVsrreturnDetails { get; set; }
        public virtual DbSet<HrmTranExitReEntyServiceRequestDetail> HrmTranExitReEntyServiceRequestDetails { get; set; }
        public virtual DbSet<HrmTranHroverView> HrmTranHroverViews { get; set; }
        public virtual DbSet<HrmTranInsurance> HrmTranInsurances { get; set; }
        public virtual DbSet<HrmTranInsuranceAudit> HrmTranInsuranceAudits { get; set; }
        public virtual DbSet<HrmTranInvoice> HrmTranInvoices { get; set; }
        public virtual DbSet<HrmTranInvoiceItem> HrmTranInvoiceItems { get; set; }
        public virtual DbSet<HrmTranMapToContactInfo> HrmTranMapToContactInfos { get; set; }
        public virtual DbSet<HrmTranMapToContactInfoAudit> HrmTranMapToContactInfoAudits { get; set; }
        public virtual DbSet<HrmTranPayroll> HrmTranPayrolls { get; set; }
        public virtual DbSet<HrmTranPayrollEntry> HrmTranPayrollEntries { get; set; }
        public virtual DbSet<HrmTranPayrollEntryAudit> HrmTranPayrollEntryAudits { get; set; }
        public virtual DbSet<HrmTranPayrollGroupMapDelete> HrmTranPayrollGroupMapDeletes { get; set; }
        public virtual DbSet<HrmTranPayrollProcess> HrmTranPayrollProcesses { get; set; }
        public virtual DbSet<HrmTranPayrollProcessDetail> HrmTranPayrollProcessDetails { get; set; }
        public virtual DbSet<HrmTranPayscale> HrmTranPayscales { get; set; }
        public virtual DbSet<HrmTranPersonalInfo> HrmTranPersonalInfos { get; set; }
        public virtual DbSet<HrmTranPersonalInfoAudit> HrmTranPersonalInfoAudits { get; set; }
        public virtual DbSet<HrmTranPreparation> HrmTranPreparations { get; set; }
        public virtual DbSet<HrmTranPreparationAudit> HrmTranPreparationAudits { get; set; }
        public virtual DbSet<HrmTranServiceRequestDocumentDetail> HrmTranServiceRequestDocumentDetails { get; set; }
        public virtual DbSet<Invitation> Invitations { get; set; }
        public virtual DbSet<PracticeTable> PracticeTables { get; set; }
        public virtual DbSet<Table2> Table2s { get; set; }
        public virtual DbSet<TblAudit> TblAudits { get; set; }
        public virtual DbSet<TestForPractice> TestForPractices { get; set; }
        public virtual DbSet<Testtable> Testtables { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.\\MSSQLSERVER2019;Database=DMC_HRM_Db;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1256_CI_AS");

            modelBuilder.Entity<Audit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Audit");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.DateTime)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.Field)
                    .IsRequired()
                    .HasMaxLength(90)
                    .IsUnicode(false);

                entity.Property(e => e.OldValue)
                    .IsRequired()
                    .HasMaxLength(90)
                    .IsUnicode(false)
                    .HasColumnName("Old_Value");

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(90)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Auditing>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Auditing");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.DateTime)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.Field)
                    .IsRequired()
                    .HasMaxLength(90)
                    .IsUnicode(false);

                entity.Property(e => e.OldValue)
                    .IsRequired()
                    .HasMaxLength(90)
                    .IsUnicode(false)
                    .HasColumnName("Old_Value");

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasMaxLength(90)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(90)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Delete>(entity =>
            {
                entity.HasKey(e => e.EmployeeAddressId)
                    .HasName("PK_HRM_TRAN_EmployeeAddress");

                entity.ToTable("Delete");

                entity.Property(e => e.EmployeeAddressId).HasColumnName("EmployeeAddressID");

                entity.Property(e => e.AddressLine1).HasMaxLength(500);

                entity.Property(e => e.AddressLine2).HasMaxLength(500);

                entity.Property(e => e.AddressLine3).HasMaxLength(500);

                entity.Property(e => e.AddressTypeId).HasColumnName("AddressTypeID");

                entity.Property(e => e.City)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Pobox)
                    .HasMaxLength(30)
                    .HasColumnName("POBox");
            });

            modelBuilder.Entity<DeleteEmployeeTable>(entity =>
            {
                entity.HasKey(e => e.EmployeeId);

                entity.ToTable("DeleteEmployeeTable");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.EmployeeName).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmAuditServiceRequestStage>(entity =>
            {
                entity.HasKey(e => e.ServiceRequestStageId)
                    .HasName("PK__HRM_AUDI__3852430146136164");

                entity.ToTable("HRM_AUDIT_ServiceRequestStage");

                entity.Property(e => e.ServiceRequestStageId).HasColumnName("ServiceRequestStageID");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.AssignedEmployeeId).HasColumnName("AssignedEmployeeID");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");
            });

            modelBuilder.Entity<HrmDefApprovalAuthorityMatrix>(entity =>
            {
                entity.HasKey(e => e.ApprovalAuthorityMatrixId)
                    .HasName("PK__HRM_DEF___09474BC064999E5F");

                entity.ToTable("HRM_DEF_ApprovalAuthorityMatrix");

                entity.Property(e => e.ApprovalAuthorityMatrixId).HasColumnName("ApprovalAuthorityMatrixID");

                entity.Property(e => e.ApprovalAuthorityId).HasColumnName("ApprovalAuthorityID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.ManagerEmployeeId).HasColumnName("ManagerEmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PositionId).HasColumnName("PositionID");
            });

            modelBuilder.Entity<HrmDefAsset>(entity =>
            {
                entity.HasKey(e => e.AssetId)
                    .HasName("PK__HRM_DEF___1BBF9F25EB07D2AA");

                entity.ToTable("HRM_DEF_Asset");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.AssetNumber).HasMaxLength(20);

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.Barcode).HasMaxLength(500);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Make).HasMaxLength(20);

                entity.Property(e => e.MakeYearAr)
                    .HasMaxLength(30)
                    .HasColumnName("MakeYear_AR");

                entity.Property(e => e.MakeYearEn)
                    .HasColumnType("datetime")
                    .HasColumnName("MakeYear_EN");

                entity.Property(e => e.ModelNumber).HasMaxLength(20);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.HasOne(d => d.AssetType)
                    .WithMany(p => p.HrmDefAssets)
                    .HasForeignKey(d => d.AssetTypeId)
                    .HasConstraintName("FK_HRM_DEF_AssetDetails_HRM_DEF_AssetType");
            });

            modelBuilder.Entity<HrmDefAssetAudit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_DEF_Asset_Audit");

                entity.Property(e => e.AssetAuditId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("AssetAuditID");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.AssetNumber).HasMaxLength(20);

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.Barcode).HasMaxLength(500);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.Make).HasMaxLength(20);

                entity.Property(e => e.MakeYearAr)
                    .HasMaxLength(30)
                    .HasColumnName("MakeYear_AR");

                entity.Property(e => e.MakeYearEn)
                    .HasColumnType("datetime")
                    .HasColumnName("MakeYear_EN");

                entity.Property(e => e.ModelNumber).HasMaxLength(20);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmDefAssetType>(entity =>
            {
                entity.HasKey(e => e.AssetTypeId)
                    .HasName("PK__HRM_DEF___FD33C22217F790F9");

                entity.ToTable("HRM_DEF_AssetType");

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.AssetNameAr)
                    .HasMaxLength(30)
                    .HasColumnName("AssetName_AR");

                entity.Property(e => e.AssetNameEn)
                    .HasMaxLength(30)
                    .HasColumnName("AssetName_EN");

                entity.Property(e => e.AssetTypeDescription).HasMaxLength(30);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefAssetTypeAudit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_DEF_AssetType_Audit");

                entity.Property(e => e.AssetNameAr)
                    .HasMaxLength(30)
                    .HasColumnName("AssetName_AR");

                entity.Property(e => e.AssetNameEn)
                    .HasMaxLength(30)
                    .HasColumnName("AssetName_EN");

                entity.Property(e => e.AssetTypeAuditId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("AssetTypeAuditID");

                entity.Property(e => e.AssetTypeDescription).HasMaxLength(30);

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmDefAttendance>(entity =>
            {
                entity.ToTable("HRM_DEF_Attendance");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.NetWorkTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TotNetWorkTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HrmDefBankDetail>(entity =>
            {
                entity.HasKey(e => e.AccountId);

                entity.ToTable("HRM_DEF_BankDetails");

                entity.Property(e => e.AccountNumber).HasMaxLength(50);

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.BankNameAr)
                    .HasMaxLength(250)
                    .HasColumnName("BankNameAR");

                entity.Property(e => e.BankNameEn)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("BankNameEN");

                entity.Property(e => e.BranchName).HasMaxLength(250);

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Iban)
                    .HasMaxLength(150)
                    .HasColumnName("IBAN");

                entity.Property(e => e.NameAr)
                    .HasMaxLength(250)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefBranch>(entity =>
            {
                entity.HasKey(e => e.BranchId);

                entity.ToTable("HRM_DEF_Branch");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.BranchDescription).HasMaxLength(100);

                entity.Property(e => e.BranchNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("BranchName_AR");

                entity.Property(e => e.BranchNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("BranchName_EN");

                entity.Property(e => e.BusinessUnitId).HasColumnName("BusinessUnitID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);
            });

            modelBuilder.Entity<HrmDefBusinessUnit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_DEF_BusinessUnit");

                entity.Property(e => e.BusinessUnitDescription).HasMaxLength(100);

                entity.Property(e => e.BusinessUnitId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("BusinessUnitID");

                entity.Property(e => e.BusinessUnitNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("BusinessUnitName_AR");

                entity.Property(e => e.BusinessUnitNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("BusinessUnitName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefCity>(entity =>
            {
                entity.HasKey(e => e.CityId)
                    .HasName("PK__HRM_DEF___F2D21A968F2DCCD5");

                entity.ToTable("HRM_DEF_City");

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.CityDescription).HasMaxLength(100);

                entity.Property(e => e.CityNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("CityName_AR");

                entity.Property(e => e.CityNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("CityName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefCompany>(entity =>
            {
                entity.HasKey(e => e.CompanyId)
                    .HasName("PK__HRM_DEF___2D971C4C9CCB0E03");

                entity.ToTable("HRM_DEF_Company");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.AddressAr)
                    .HasMaxLength(500)
                    .HasColumnName("AddressAR");

                entity.Property(e => e.AddressEn)
                    .HasMaxLength(500)
                    .HasColumnName("AddressEN");

                entity.Property(e => e.Ccnumber)
                    .HasMaxLength(50)
                    .HasColumnName("CCNumber");

                entity.Property(e => e.City).HasMaxLength(150);

                entity.Property(e => e.CompanyDescription).HasMaxLength(100);

                entity.Property(e => e.CompanyNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("CompanyName_AR");

                entity.Property(e => e.CompanyNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("CompanyName_EN");

                entity.Property(e => e.Country).HasMaxLength(150);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Crnumber)
                    .HasMaxLength(50)
                    .HasColumnName("CRNumber");

                entity.Property(e => e.EmailId).HasMaxLength(150);

                entity.Property(e => e.Fax).HasMaxLength(50);

                entity.Property(e => e.InvoiceNote).HasMaxLength(500);

                entity.Property(e => e.Logo).HasMaxLength(500);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PaymentNote).HasMaxLength(500);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.StateProvince).HasMaxLength(150);

                entity.Property(e => e.Vatnumber)
                    .HasMaxLength(50)
                    .HasColumnName("VATNumber");

                entity.Property(e => e.Website).HasMaxLength(150);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HrmDefContractType>(entity =>
            {
                entity.HasKey(e => e.ContractTypeId)
                    .HasName("PK__HRM_DEF___68A615459C3A291D");

                entity.ToTable("HRM_DEF_ContractType");

                entity.Property(e => e.ContractTypeId).HasColumnName("ContractTypeID");

                entity.Property(e => e.ContractTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ContractTypeName_AR");

                entity.Property(e => e.ContractTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ContractTypeName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefCostCenter>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_DEF_CostCenter");

                entity.Property(e => e.CostCenterDescription).HasMaxLength(100);

                entity.Property(e => e.CostCenterId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("CostCenterID");

                entity.Property(e => e.CostCenterNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("CostCenterName_AR");

                entity.Property(e => e.CostCenterNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("CostCenterName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefCurrency>(entity =>
            {
                entity.HasKey(e => e.CurrencyId);

                entity.ToTable("HRM_DEF_Currency");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.NameAr)
                    .HasMaxLength(50)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.Symbol)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefCurrentJob>(entity =>
            {
                entity.HasKey(e => e.CurrentJobId)
                    .HasName("PK__HRM_DEF___292BCD646CE9A62F");

                entity.ToTable("HRM_DEF_CurrentJob");

                entity.Property(e => e.CurrentJobId).HasColumnName("CurrentJobID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CurrentJobDescription).HasMaxLength(500);

                entity.Property(e => e.CurrentJobNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("CurrentJobName_AR");

                entity.Property(e => e.CurrentJobNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("CurrentJobName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefCustomer>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.ToTable("HRM_DEF_Customer");

                entity.Property(e => e.AddressAr)
                    .HasMaxLength(500)
                    .HasColumnName("AddressAR");

                entity.Property(e => e.AddressEn)
                    .HasMaxLength(500)
                    .HasColumnName("AddressEN");

                entity.Property(e => e.City).HasMaxLength(150);

                entity.Property(e => e.ContactName).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(150);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailId)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Fax).HasMaxLength(20);

                entity.Property(e => e.Industry).HasMaxLength(150);

                entity.Property(e => e.MobileNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NameAr)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.StateProvince).HasMaxLength(150);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.Property(e => e.Vatnumber)
                    .HasMaxLength(50)
                    .HasColumnName("VATNumber");

                entity.Property(e => e.Website)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmDefDepartment>(entity =>
            {
                entity.HasKey(e => e.DepartmentId);

                entity.ToTable("HRM_DEF_Department");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.CostCenterId).HasColumnName("CostCenterID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentDescription).HasMaxLength(100);

                entity.Property(e => e.DepartmentNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("DepartmentName_AR");

                entity.Property(e => e.DepartmentNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("DepartmentName_EN");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefDivision>(entity =>
            {
                entity.HasKey(e => e.DivisionId);

                entity.ToTable("HRM_DEF_Division");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DivisionDescription).HasMaxLength(100);

                entity.Property(e => e.DivisionNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("DivisionName_AR");

                entity.Property(e => e.DivisionNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("DivisionName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefDocumentType>(entity =>
            {
                entity.HasKey(e => e.DocumentTypeId);

                entity.ToTable("HRM_DEF_DocumentType");

                entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DocumentDescription).HasMaxLength(500);

                entity.Property(e => e.DocumentTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("DocumentTypeName_AR");

                entity.Property(e => e.DocumentTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("DocumentTypeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefEmployeePayrolGroupMap>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_DEF_EmployeePayrolGroupMAP");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayrollGroupId).HasColumnName("PayrollGroupID");

                entity.HasOne(d => d.PayrollGroup)
                    .WithMany()
                    .HasForeignKey(d => d.PayrollGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HRM_DEF_EmployeePayrolGroupMAP_HRM_DEF_PayrollGroup");
            });

            modelBuilder.Entity<HrmDefEmployeeWeekDay>(entity =>
            {
                entity.HasKey(e => e.EmployeeWeekDaysId)
                    .HasName("PK_HRM_DEFEmployeeWeekDays");

                entity.ToTable("HRM_DEF_EmployeeWeekDays");

                entity.Property(e => e.EmployeeWeekDaysId).HasColumnName("EmployeeWeekDaysID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmplyeeWeekDayAr)
                    .HasMaxLength(25)
                    .HasColumnName("EmplyeeWeekDay_AR");

                entity.Property(e => e.EmplyeeWeekDayEn)
                    .HasMaxLength(25)
                    .HasColumnName("EmplyeeWeekDay_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefFlightClass>(entity =>
            {
                entity.HasKey(e => e.FlightClassId);

                entity.ToTable("HRM_DEF_FlightClass");

                entity.Property(e => e.FlightClassId).HasColumnName("FlightClassID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FlightClassNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("FlightClassName_AR");

                entity.Property(e => e.FlightClassNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("FlightClassName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefFlightType>(entity =>
            {
                entity.HasKey(e => e.FlightTypeId)
                    .HasName("PK__HRM_DEF___68CE86FD3DC6D8BF");

                entity.ToTable("HRM_DEF_FlightType");

                entity.Property(e => e.FlightTypeId).HasColumnName("FlightTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FlightTypeNameAr)
                    .HasMaxLength(50)
                    .HasColumnName("FlightTypeName_AR");

                entity.Property(e => e.FlightTypeNameEn)
                    .HasMaxLength(50)
                    .HasColumnName("FlightTypeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefFrequency>(entity =>
            {
                entity.HasKey(e => e.FrequencyId);

                entity.ToTable("HRM_DEF_Frequency");

                entity.Property(e => e.FrequencyId).HasColumnName("FrequencyID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FrequencyDescription).HasMaxLength(100);

                entity.Property(e => e.FrequencyNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("FrequencyName_AR");

                entity.Property(e => e.FrequencyNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("FrequencyName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefGardeningLeave>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_DEF_GardeningLeave");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.GardeningLeaveId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("GardeningLeaveID");

                entity.Property(e => e.GardeningLeaveNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("GardeningLeaveName_AR");

                entity.Property(e => e.GardeningLeaveNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("GardeningLeaveName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefGrade>(entity =>
            {
                entity.HasKey(e => e.GradeId);

                entity.ToTable("HRM_DEF_Grade");

                entity.Property(e => e.GradeId).HasColumnName("GradeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.GradeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("GradeName_AR");

                entity.Property(e => e.GradeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("GradeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefGrade1>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_DEF_Grade1");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.GradeId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("GradeID");

                entity.Property(e => e.GradeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("GradeName_AR");

                entity.Property(e => e.GradeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("GradeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefHolidayMaster>(entity =>
            {
                entity.HasKey(e => e.HolidayCalanderId);

                entity.ToTable("HRM_DEF_HolidayMaster");

                entity.Property(e => e.HolidayCalanderId).HasColumnName("HolidayCalanderID");

                entity.Property(e => e.CalanderNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("CalanderName_AR");

                entity.Property(e => e.CalanderNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("CalanderName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefInsuranceClass>(entity =>
            {
                entity.HasKey(e => e.InsuranceClassId);

                entity.ToTable("HRM_DEF_InsuranceClass");

                entity.Property(e => e.InsuranceClassId).HasColumnName("InsuranceClassID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.InsuranceClassNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("InsuranceClassName_AR");

                entity.Property(e => e.InsuranceClassNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("InsuranceClassName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefInsuranceProvider>(entity =>
            {
                entity.HasKey(e => e.InsuranceProviderId);

                entity.ToTable("HRM_DEF_InsuranceProvider");

                entity.Property(e => e.InsuranceProviderId).HasColumnName("InsuranceProviderID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.InsuranceProviderNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("InsuranceProviderName_AR");

                entity.Property(e => e.InsuranceProviderNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("InsuranceProviderName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefInsuranceType>(entity =>
            {
                entity.HasKey(e => e.InsuranceTypeId);

                entity.ToTable("HRM_DEF_InsuranceType");

                entity.Property(e => e.InsuranceTypeId).HasColumnName("InsuranceTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.InsuranceTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("InsuranceTypeName_AR");

                entity.Property(e => e.InsuranceTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("InsuranceTypeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefInvoiceStatus>(entity =>
            {
                entity.HasKey(e => e.InvoiceStatusId);

                entity.ToTable("HRM_DEF_InvoiceStatus");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NameAr)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefLegalEntity>(entity =>
            {
                entity.HasKey(e => e.LegalEntityId)
                    .HasName("PK__HRM_DEF___5266B1A270E74673");

                entity.ToTable("HRM_DEF_LegalEntity");

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LegalEntityDescription).HasMaxLength(100);

                entity.Property(e => e.LegalEntityNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("LegalEntityName_AR");

                entity.Property(e => e.LegalEntityNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("LegalEntityName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefLegalEntity1>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_DEF_LegalEntity1");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LegalEntityDescription).HasMaxLength(100);

                entity.Property(e => e.LegalEntityId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("LegalEntityID");

                entity.Property(e => e.LegalEntityNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("LegalEntityName_AR");

                entity.Property(e => e.LegalEntityNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("LegalEntityName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefLineManager>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("HRM_DEF_LineManager");

                entity.Property(e => e.LineManagerId).HasColumnName("LineManagerID");

                entity.Property(e => e.LineManagerNameAr)
                    .HasMaxLength(50)
                    .HasColumnName("LineManagerName_AR");

                entity.Property(e => e.LineManagerNameEn)
                    .HasMaxLength(50)
                    .HasColumnName("LineManagerName_EN");
            });

            modelBuilder.Entity<HrmDefListOfHoliday>(entity =>
            {
                entity.HasKey(e => e.HolidayId);

                entity.ToTable("HRM_DEF_ListOfHolidays");

                entity.Property(e => e.HolidayId).HasColumnName("HolidayID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.HolidayCalanderId).HasColumnName("HolidayCalanderID");

                entity.Property(e => e.HolidayDate).HasColumnType("date");

                entity.Property(e => e.HolidayDescriptionAr)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("HolidayDescription_AR");

                entity.Property(e => e.HolidayDescriptionEn)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("HolidayDescription_EN");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.IsSystem).HasColumnName("isSystem");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.HolidayCalander)
                    .WithMany(p => p.HrmDefListOfHolidays)
                    .HasForeignKey(d => d.HolidayCalanderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HRM_DEF_ListOfHolidays_HRM_DEF_HolidayMaster");
            });

            modelBuilder.Entity<HrmDefLoanType>(entity =>
            {
                entity.HasKey(e => e.LoanTypeId);

                entity.ToTable("HRM_DEF_LoanType");

                entity.Property(e => e.LoanTypeId).HasColumnName("LoanTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LoanTypeDescription).HasMaxLength(100);

                entity.Property(e => e.LoanTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("LoanTypeName_AR");

                entity.Property(e => e.LoanTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("LoanTypeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefMenu>(entity =>
            {
                entity.HasKey(e => e.MenuId)
                    .HasName("PK__HRM_DEF___C99ED250182419AA");

                entity.ToTable("HRM_DEF_Menu");

                entity.Property(e => e.MenuId).HasColumnName("MenuID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CssClass).HasMaxLength(50);

                entity.Property(e => e.MenuDescription)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.MenuLabelAr)
                    .HasMaxLength(100)
                    .HasColumnName("MenuLabel_AR");

                entity.Property(e => e.MenuLabelEn)
                    .HasMaxLength(100)
                    .HasColumnName("MenuLabel_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ParentMenuId).HasColumnName("ParentMenuID");

                entity.Property(e => e.ScreenId).HasColumnName("ScreenID");

                entity.HasOne(d => d.ParentMenu)
                    .WithMany(p => p.InverseParentMenu)
                    .HasForeignKey(d => d.ParentMenuId)
                    .HasConstraintName("FK__HRM_DEF_M__Paren__5812160E");

                entity.HasOne(d => d.Screen)
                    .WithMany(p => p.HrmDefMenus)
                    .HasForeignKey(d => d.ScreenId)
                    .HasConstraintName("FK__HRM_DEF_M__Scree__59063A47");
            });

            modelBuilder.Entity<HrmDefOutofOfficeReason>(entity =>
            {
                entity.HasKey(e => e.OutofOfficeReasonId);

                entity.ToTable("HRM_DEF_OutofOfficeReason");

                entity.Property(e => e.OutofOfficeReasonId).HasColumnName("OutofOfficeReasonID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OutofOfficeReasonNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("OutofOfficeReasonName_AR");

                entity.Property(e => e.OutofOfficeReasonNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("OutofOfficeReasonName_EN");
            });

            modelBuilder.Entity<HrmDefPackage>(entity =>
            {
                entity.HasKey(e => e.PackageId)
                    .HasName("PK__HRM_DEF___322035EC3C0D968B");

                entity.ToTable("HRM_DEF_Package");

                entity.Property(e => e.PackageId).HasColumnName("PackageID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PackageDescription).HasMaxLength(500);

                entity.Property(e => e.PackageNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("PackageName_AR");

                entity.Property(e => e.PackageNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("PackageName_EN");
            });

            modelBuilder.Entity<HrmDefPayType>(entity =>
            {
                entity.HasKey(e => e.PayTypeId);

                entity.ToTable("HRM_DEF_PayType");

                entity.HasIndex(e => e.PayTypeNameEn, "UQ__HRM_DEF___B3697C196B0FDBE9")
                    .IsUnique();

                entity.Property(e => e.PayTypeId).HasColumnName("PayTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayTypeDescription).HasMaxLength(100);

                entity.Property(e => e.PayTypeGroupId).HasColumnName("PayTypeGroupID");

                entity.Property(e => e.PayTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("PayTypeName_AR");

                entity.Property(e => e.PayTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("PayTypeName_EN");
            });

            modelBuilder.Entity<HrmDefPayTypeFormulae>(entity =>
            {
                entity.HasKey(e => e.FormulaeId);

                entity.ToTable("HRM_DEF_PayTypeFormulae");

                entity.Property(e => e.FormulaeId).HasColumnName("FormulaeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FormulaPayTypeId).HasColumnName("FormulaPayTypeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayTypeGroupId).HasColumnName("PayTypeGroupID");

                entity.Property(e => e.PayTypeId).HasColumnName("PayTypeID");

                entity.HasOne(d => d.PayTypeGroup)
                    .WithMany(p => p.HrmDefPayTypeFormulaes)
                    .HasForeignKey(d => d.PayTypeGroupId)
                    .HasConstraintName("FK_HRM_DEF_PayTypeFormulae_HRM_DEF_PayTypeGroup");

                entity.HasOne(d => d.PayType)
                    .WithMany(p => p.HrmDefPayTypeFormulaes)
                    .HasForeignKey(d => d.PayTypeId)
                    .HasConstraintName("FK_HRM_DEF_PayTypeFormulae_HRM_DEF_PayType");
            });

            modelBuilder.Entity<HrmDefPayTypeGroup>(entity =>
            {
                entity.HasKey(e => e.PayTypeGroupId);

                entity.ToTable("HRM_DEF_PayTypeGroup");

                entity.Property(e => e.PayTypeGroupId).HasColumnName("PayTypeGroupID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayTypeGroupNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("PayTypeGroupName_AR");

                entity.Property(e => e.PayTypeGroupNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("PayTypeGroupName_EN");
            });

            modelBuilder.Entity<HrmDefPayTypeSetting>(entity =>
            {
                entity.HasKey(e => e.PaytypeSettingId);

                entity.ToTable("HRM_DEF_PayTypeSetting");

                entity.Property(e => e.PaytypeSettingId).HasColumnName("PaytypeSettingID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Multiplier).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.PayTypeGroupId).HasColumnName("PayTypeGroupID");

                entity.Property(e => e.PayTypeId).HasColumnName("PayTypeID");

                entity.HasOne(d => d.PayTypeGroup)
                    .WithMany(p => p.HrmDefPayTypeSettings)
                    .HasForeignKey(d => d.PayTypeGroupId)
                    .HasConstraintName("FK_HRM_DEF_PayTypeSetting_HRM_DEF_PayTypeGroup");

                entity.HasOne(d => d.PayType)
                    .WithMany(p => p.HrmDefPayTypeSettings)
                    .HasForeignKey(d => d.PayTypeId)
                    .HasConstraintName("FK_HRM_DEF_PayTypeSetting_HRM_DEF_PayType");
            });

            modelBuilder.Entity<HrmDefPaymentMethod>(entity =>
            {
                entity.HasKey(e => e.PaymentMethodId);

                entity.ToTable("HRM_DEF_PaymentMethod");

                entity.Property(e => e.AccountNumber).HasMaxLength(150);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.NameAr)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.Provider).HasMaxLength(150);

                entity.Property(e => e.Reference).HasMaxLength(150);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefPaymentType>(entity =>
            {
                entity.HasKey(e => e.PaymentTypeId);

                entity.ToTable("HRM_DEF_PaymentType");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NameAr)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefPayrollGroup>(entity =>
            {
                entity.HasKey(e => e.PayrollGroupId);

                entity.ToTable("HRM_DEF_PayrollGroup");

                entity.Property(e => e.PayrollGroupId).HasColumnName("PayrollGroupID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.BusinessUnitId).HasColumnName("BusinessUnitID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CurrentPayRollMonth).HasColumnType("datetime");

                entity.Property(e => e.CurrentPayRollYear).HasColumnType("datetime");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EndPayRollDate).HasColumnType("date");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayrollGroupNameAr)
                    .HasMaxLength(200)
                    .HasColumnName("PayrollGroupName_AR");

                entity.Property(e => e.PayrollGroupNameEn)
                    .HasMaxLength(200)
                    .HasColumnName("PayrollGroupName_EN");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.SiteId).HasColumnName("SiteID");

                entity.Property(e => e.StartPayRollDate).HasColumnType("date");
            });

            modelBuilder.Entity<HrmDefPayrollPeriod>(entity =>
            {
                entity.HasKey(e => e.PeriodId)
                    .HasName("PK__HRM_DEF___E521BB36DA8757F1");

                entity.ToTable("HRM_DEF_PayrollPeriod");

                entity.Property(e => e.PeriodId).HasColumnName("PeriodID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PeriodMonth).HasColumnType("date");

                entity.Property(e => e.Remarks).HasMaxLength(500);
            });

            modelBuilder.Entity<HrmDefPersonType>(entity =>
            {
                entity.HasKey(e => e.PersonTypeId)
                    .HasName("PK__HRM_DEF___D95DCAFD46F4877C");

                entity.ToTable("HRM_DEF_PersonType");

                entity.Property(e => e.PersonTypeId).HasColumnName("PersonTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PersonTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("PersonTypeName_AR");

                entity.Property(e => e.PersonTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("PersonTypeName_EN");
            });

            modelBuilder.Entity<HrmDefPosition>(entity =>
            {
                entity.HasKey(e => e.PositionId);

                entity.ToTable("HRM_DEF_Position");

                entity.Property(e => e.PositionId).HasColumnName("PositionID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PositionDescription).HasMaxLength(100);

                entity.Property(e => e.PositionNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("PositionName_AR");

                entity.Property(e => e.PositionNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("PositionName_EN");
            });

            modelBuilder.Entity<HrmDefProduct>(entity =>
            {
                entity.HasKey(e => e.ProductId);

                entity.ToTable("HRM_DEF_Product");

                entity.Property(e => e.Barcode)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CostPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.NameAr)
                    .HasMaxLength(150)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.ProductCode).HasMaxLength(150);

                entity.Property(e => e.UnitPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefProductType>(entity =>
            {
                entity.HasKey(e => e.ProductTypeId)
                    .HasName("PK_HRF_DEF_ProductType");

                entity.ToTable("HRM_DEF_ProductType");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NameAr)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefProject>(entity =>
            {
                entity.HasKey(e => e.ProjectId);

                entity.ToTable("HRM_DEF_Project");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ProjectDescription).HasMaxLength(100);

                entity.Property(e => e.ProjectNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ProjectName_AR");

                entity.Property(e => e.ProjectNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ProjectName_EN");

                entity.Property(e => e.ProjectSiteId).HasColumnName("ProjectSiteID");
            });

            modelBuilder.Entity<HrmDefReport>(entity =>
            {
                entity.HasKey(e => e.ReportId);

                entity.ToTable("HRM_DEF_Reports");

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ReportNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ReportName_AR");

                entity.Property(e => e.ReportNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ReportName_EN");
            });

            modelBuilder.Entity<HrmDefReportCommonFilter>(entity =>
            {
                entity.HasKey(e => e.CommonFilterId);

                entity.ToTable("HRM_DEF_ReportCommonFilters");

                entity.Property(e => e.CommonFilterId).HasColumnName("CommonFilterID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DisplayLabelNameAr)
                    .HasMaxLength(250)
                    .HasColumnName("DisplayLabelName_AR");

                entity.Property(e => e.DisplayLabelNameEn)
                    .HasMaxLength(250)
                    .HasColumnName("DisplayLabelName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ParameterColumnName).HasMaxLength(100);

                entity.Property(e => e.RelationFilterId).HasColumnName("RelationFilterID");
            });

            modelBuilder.Entity<HrmDefReportFilter>(entity =>
            {
                entity.HasKey(e => e.ReportFilterId);

                entity.ToTable("HRM_DEF_ReportFilters");

                entity.Property(e => e.ReportFilterId).HasColumnName("ReportFilterID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DisplayLabelNameAr)
                    .HasMaxLength(250)
                    .HasColumnName("DisplayLabelName_AR");

                entity.Property(e => e.DisplayLabelNameEn)
                    .HasMaxLength(250)
                    .HasColumnName("DisplayLabelName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ParameterColumnName).HasMaxLength(100);

                entity.Property(e => e.RelationFilterId).HasColumnName("RelationFilterID");

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.HrmDefReportFilters)
                    .HasForeignKey(d => d.ReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HRM_DEF_ReportFilters_HRM_DEF_Reports");
            });

            modelBuilder.Entity<HrmDefReportMappingByRole>(entity =>
            {
                entity.HasKey(e => e.ReportMappingByRoleId);

                entity.ToTable("HRM_DEF_ReportMappingByRole");

                entity.Property(e => e.ReportMappingByRoleId).HasColumnName("ReportMappingByRoleID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.HrmDefReportMappingByRoles)
                    .HasForeignKey(d => d.ReportId)
                    .HasConstraintName("FK_HRM_DEF_ReportMappingByRole_HRM_DEF_ReportMappingByRole");
            });

            modelBuilder.Entity<HrmDefReportMaster>(entity =>
            {
                entity.ToTable("HRM_DEF_ReportMaster");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.ReportName).HasMaxLength(50);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.StoreProcName).HasMaxLength(150);

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.HrmDefReportMasters)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("FK_HRM_DEF_ReportMaster_HRM_SYS_Module");
            });

            modelBuilder.Entity<HrmDefReportOutputParam>(entity =>
            {
                entity.ToTable("HRM_DEF_Report_OutputParams");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.OutputParamName).HasMaxLength(500);

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.HrmDefReportOutputParams)
                    .HasForeignKey(d => d.ReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HRM_DEF_Report_OutputParams_HRM_DEF_Reports");
            });

            modelBuilder.Entity<HrmDefRole>(entity =>
            {
                entity.HasKey(e => e.RoleId)
                    .HasName("PK__HRM_DEF___8AFACE3AD520041C");

                entity.ToTable("HRM_DEF_Role");

                entity.HasIndex(e => e.RoleNameEn, "IX_HRM_DEF_Role")
                    .IsUnique();

                entity.HasIndex(e => e.RoleNameAr, "IX_HRM_DEF_Role_1")
                    .IsUnique();

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RoleDescription).HasMaxLength(500);

                entity.Property(e => e.RoleNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("RoleName_AR");

                entity.Property(e => e.RoleNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("RoleName_EN");

                entity.Property(e => e.Type).HasComment("1.Admin , 2.HR , 4.Employee , 5.");
            });

            modelBuilder.Entity<HrmDefRoleMenuMapping>(entity =>
            {
                entity.HasKey(e => e.RoleMenuMappingId)
                    .HasName("PK__HRM_DEF___B804E07DF8662758");

                entity.ToTable("HRM_DEF_RoleMenuMapping");

                entity.Property(e => e.RoleMenuMappingId).HasColumnName("RoleMenuMappingID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MenuId).HasColumnName("MenuID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.HrmDefRoleMenuMappings)
                    .HasForeignKey(d => d.MenuId)
                    .HasConstraintName("FK__HRM_DEF_R__MenuI__5CD6CB2B");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.HrmDefRoleMenuMappings)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__HRM_DEF_R__RoleI__5BE2A6F2");
            });

            modelBuilder.Entity<HrmDefServiceRequestProcessStage>(entity =>
            {
                entity.HasKey(e => e.ServiceRequestProcessStageId)
                    .HasName("PK_HRM_DEF_ServiceRequestProcessStep");

                entity.ToTable("HRM_DEF_ServiceRequestProcessStage");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.Property(e => e.ApplicableBranch).HasMaxLength(300);

                entity.Property(e => e.ApplicableCompany).HasMaxLength(300);

                entity.Property(e => e.ApplicableDepartment).HasMaxLength(300);

                entity.Property(e => e.ApplicableEmployee).HasMaxLength(300);

                entity.Property(e => e.ApprovalAuthorityId).HasColumnName("ApprovalAuthorityID");

                entity.Property(e => e.ApprovalEmployeeId).HasColumnName("ApprovalEmployeeID");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasMaxLength(100);

                entity.Property(e => e.ModifiedBy).HasMaxLength(100);

                entity.Property(e => e.ModifiedDate).HasMaxLength(100);

                entity.Property(e => e.ServiceRequestTypeId).HasColumnName("ServiceRequestTypeID");
            });

            modelBuilder.Entity<HrmDefServiceRequestType>(entity =>
            {
                entity.HasKey(e => e.ServiceRequestTypeId)
                    .HasName("PK__HRM_DEF___59EC08217AA20610");

                entity.ToTable("HRM_DEF_ServiceRequestType");

                entity.Property(e => e.ServiceRequestTypeId).HasColumnName("ServiceRequestTypeID");

                entity.Property(e => e.ActionName).HasMaxLength(100);

                entity.Property(e => e.ControllerName).HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CssClass).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Icon).HasMaxLength(100);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceRequestTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ServiceRequestTypeName_AR");

                entity.Property(e => e.ServiceRequestTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ServiceRequestTypeName_EN");
            });

            modelBuilder.Entity<HrmDefServiceRequestType2>(entity =>
            {
                entity.HasKey(e => e.ServiceRequestTypeId)
                    .HasName("PK_HRM_TRAN_ServiceRequestTypes");

                entity.ToTable("HRM_DEF_ServiceRequestType2");

                entity.Property(e => e.ServiceRequestTypeId).HasColumnName("ServiceRequestTypeID");

                entity.Property(e => e.ActionName).HasMaxLength(100);

                entity.Property(e => e.ControllerName).HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Icon).HasMaxLength(100);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceRequestTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ServiceRequestTypeName_AR");

                entity.Property(e => e.ServiceRequestTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ServiceRequestTypeName_EN");
            });

            modelBuilder.Entity<HrmDefSite>(entity =>
            {
                entity.HasKey(e => e.SiteId)
                    .HasName("PK__HRM_DEF___B9DCB9035C7000CD");

                entity.ToTable("HRM_DEF_Site");

                entity.Property(e => e.SiteId).HasColumnName("SiteID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.SiteDescription).HasMaxLength(100);

                entity.Property(e => e.SiteNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("SiteName_AR");

                entity.Property(e => e.SiteNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("SiteName_EN");
            });

            modelBuilder.Entity<HrmDefSponsor>(entity =>
            {
                entity.HasKey(e => e.SponsorId);

                entity.ToTable("HRM_DEF_Sponsor");

                entity.Property(e => e.SponsorId).HasColumnName("SponsorID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SponsorDescription).HasMaxLength(100);

                entity.Property(e => e.SponsorNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("SponsorName_AR");

                entity.Property(e => e.SponsorNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("SponsorName_EN");
            });

            modelBuilder.Entity<HrmDefSponsor1>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_DEF_Sponsor1");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SponsorDescription).HasMaxLength(100);

                entity.Property(e => e.SponsorId).HasColumnName("SponsorID");

                entity.Property(e => e.SponsorNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("SponsorName_AR");

                entity.Property(e => e.SponsorNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("SponsorName_EN");
            });

            modelBuilder.Entity<HrmDefTax>(entity =>
            {
                entity.HasKey(e => e.TaxId);

                entity.ToTable("HRM_DEF_Tax");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NameAr)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.Symbol)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.TaxTariffPercentage)
                    .HasColumnType("decimal(18, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefTicketBalanceSetup>(entity =>
            {
                entity.HasKey(e => e.TicketBalanceSetupId);

                entity.ToTable("HRM_DEF_TicketBalanceSetup");

                entity.Property(e => e.TicketBalanceSetupId).HasColumnName("TicketBalanceSetupID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DependantTypeId).HasColumnName("DependantTypeID");

                entity.Property(e => e.EmployeeDependantId).HasColumnName("EmployeeDependantID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Remarks).HasMaxLength(50);

                entity.HasOne(d => d.DependantType)
                    .WithMany(p => p.HrmDefTicketBalanceSetups)
                    .HasForeignKey(d => d.DependantTypeId)
                    .HasConstraintName("FK_HRM_DEF_TicketBalanceSetup_HRM_SYS_DependantType");

                entity.HasOne(d => d.EmployeeDependant)
                    .WithMany(p => p.HrmDefTicketBalanceSetups)
                    .HasForeignKey(d => d.EmployeeDependantId)
                    .HasConstraintName("FK_HRM_DEF_TicketBalanceSetup_HRM_TRAN_EmployeeDependant");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmDefTicketBalanceSetups)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_HRM_DEF_TicketBalanceSetup_HRM_TRAN_Employee");
            });

            modelBuilder.Entity<HrmDefTicketFrequency>(entity =>
            {
                entity.HasKey(e => e.TicketFrequencyId)
                    .HasName("PK__HRM_DEF___BFC7D4E6489AC854");

                entity.ToTable("HRM_DEF_TicketFrequency");

                entity.Property(e => e.TicketFrequencyId).HasColumnName("TicketFrequencyID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TicketFrequencyNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("TicketFrequencyName_AR");

                entity.Property(e => e.TicketFrequencyNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("TicketFrequencyName_EN");
            });

            modelBuilder.Entity<HrmDefTicketSetup>(entity =>
            {
                entity.HasKey(e => e.TicketId)
                    .HasName("PK__HRM_DEF___712CC6271A1FE3F1");

                entity.ToTable("HRM_DEF_TicketSetup");

                entity.Property(e => e.TicketId).HasColumnName("TicketID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FromCityId).HasColumnName("FromCityID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TicketDescription).HasMaxLength(200);

                entity.Property(e => e.TicketNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("TicketName_AR");

                entity.Property(e => e.TicketNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("TicketName_EN");

                entity.Property(e => e.ToCityId).HasColumnName("ToCityID");
            });

            modelBuilder.Entity<HrmDefTicketType>(entity =>
            {
                entity.HasKey(e => e.TicketTypeId)
                    .HasName("PK__HRM_DEF___6CD6845179354AAD");

                entity.ToTable("HRM_DEF_TicketType");

                entity.Property(e => e.TicketTypeId).HasColumnName("TicketTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TicketTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("TicketTypeName_AR");

                entity.Property(e => e.TicketTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("TicketTypeName_EN");
            });

            modelBuilder.Entity<HrmDefTravelType>(entity =>
            {
                entity.HasKey(e => e.TravelTypeId)
                    .HasName("PK_HRM_DEF_PurposeType");

                entity.ToTable("HRM_DEF_TravelType");

                entity.Property(e => e.TravelTypeId).HasColumnName("TravelTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TravelTypeNameAr)
                    .HasMaxLength(50)
                    .HasColumnName("TravelTypeName_AR");

                entity.Property(e => e.TravelTypeNameEn)
                    .HasMaxLength(50)
                    .HasColumnName("TravelTypeName_EN");
            });

            modelBuilder.Entity<HrmDefUnitType>(entity =>
            {
                entity.HasKey(e => e.UnitId)
                    .HasName("PK_HRM_DEF_Unit");

                entity.ToTable("HRM_DEF_UnitType");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NameAr)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("NameAR");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("NameEN");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmDefUser>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("HRM_DEF_User");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PasswordExpirationDate).HasColumnType("datetime");

                entity.Property(e => e.ProfileName).HasMaxLength(100);

                entity.Property(e => e.ProfileNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ProfileName_AR");

                entity.Property(e => e.ProfilePicture)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.UserDescription).HasMaxLength(500);

                entity.Property(e => e.UserName).HasMaxLength(100);

                entity.Property(e => e.UserPassword).HasMaxLength(100);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmDefUsers)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_DEF_U__Emplo__4F7CD00D");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.HrmDefUsers)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__HRM_DEF_U__RoleI__4E88ABD4");
            });

            modelBuilder.Entity<HrmDefVacationType>(entity =>
            {
                entity.HasKey(e => e.VacationTypeId)
                    .HasName("PK__HRM_DEF___22E546565224328E");

                entity.ToTable("HRM_DEF_VacationType");

                entity.Property(e => e.VacationTypeId).HasColumnName("VacationTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PerMonthVacationBalanceValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.VacationTypeDescription).HasMaxLength(500);

                entity.Property(e => e.VacationTypeNameAr)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("VacationTypeName_AR");

                entity.Property(e => e.VacationTypeNameEn)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("VacationTypeName_EN");
            });

            modelBuilder.Entity<HrmDefValueType>(entity =>
            {
                entity.HasKey(e => e.ValueTypeId);

                entity.ToTable("HRM_DEF_ValueType");

                entity.Property(e => e.ValueTypeId).HasColumnName("ValueTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ValueTypeDescription).HasMaxLength(100);

                entity.Property(e => e.ValueTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ValueTypeName_AR");

                entity.Property(e => e.ValueTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ValueTypeName_EN");
            });

            modelBuilder.Entity<HrmSysAddressType>(entity =>
            {
                entity.HasKey(e => e.AddressTypeId)
                    .HasName("PK__HRM_SYS___8BF56CC1CF01E403");

                entity.ToTable("HRM_SYS_AddressType");

                entity.Property(e => e.AddressTypeId).HasColumnName("AddressTypeID");

                entity.Property(e => e.AddressTypeNameAr)
                    .HasMaxLength(50)
                    .HasColumnName("AddressTypeName_AR");

                entity.Property(e => e.AddressTypeNameEn)
                    .HasMaxLength(50)
                    .HasColumnName("AddressTypeName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysApprovalAuthority>(entity =>
            {
                entity.HasKey(e => e.ApprovalAuthorityId)
                    .HasName("PK_HRM_ApprovalAuthority");

                entity.ToTable("HRM_SYS_ApprovalAuthority");

                entity.Property(e => e.ApprovalAuthorityId)
                    .ValueGeneratedNever()
                    .HasColumnName("ApprovalAuthorityID");

                entity.Property(e => e.ApprovalAuthorityNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ApprovalAuthorityName_AR");

                entity.Property(e => e.ApprovalAuthorityNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ApprovalAuthorityName_EN");

                entity.Property(e => e.RelationColumn).HasMaxLength(50);
            });

            modelBuilder.Entity<HrmSysBloodGroup>(entity =>
            {
                entity.HasKey(e => e.BloodGroupId)
                    .HasName("PK__HRM_DEF___4398C68FC6C9F052");

                entity.ToTable("HRM_SYS_BloodGroup");

                entity.Property(e => e.BloodGroupNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("BloodGroupName_AR");

                entity.Property(e => e.BloodGroupNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("BloodGroupName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysContractType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_SYS_ContractType");

                entity.Property(e => e.ContractTypeId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ContractTypeID");

                entity.Property(e => e.ContractTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ContractTypeName_AR");

                entity.Property(e => e.ContractTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ContractTypeName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysCountry>(entity =>
            {
                entity.HasKey(e => e.CountryId);

                entity.ToTable("HRM_SYS_Country");

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CountryNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("CountryName_AR");

                entity.Property(e => e.CountryNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("CountryName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysCourseType>(entity =>
            {
                entity.HasKey(e => e.CourseTypeId)
                    .HasName("PK__HRM_SYS___8173695206FDF9D8");

                entity.ToTable("HRM_SYS_CourseType");

                entity.Property(e => e.CourseTypeId).HasColumnName("CourseTypeID");

                entity.Property(e => e.CourseTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("CourseTypeName_AR");

                entity.Property(e => e.CourseTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("CourseTypeName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysDependantType>(entity =>
            {
                entity.HasKey(e => e.DependantTypeId)
                    .HasName("PK__HRM_SYS___7EA1BC2CBCD3DE14");

                entity.ToTable("HRM_SYS_DependantType");

                entity.Property(e => e.DependantTypeId).HasColumnName("DependantTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DependantTypeNameAr)
                    .HasMaxLength(50)
                    .HasColumnName("DependantTypeName_AR");

                entity.Property(e => e.DependantTypeNameEn)
                    .HasMaxLength(50)
                    .HasColumnName("DependantTypeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysDocumentOwner>(entity =>
            {
                entity.HasKey(e => e.DocumentOwnerId);

                entity.ToTable("HRM_SYS_DocumentOwner");

                entity.Property(e => e.DocumentOwnerId).HasColumnName("DocumentOwnerID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DocumentOwnerNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("DocumentOwnerName_AR");

                entity.Property(e => e.DocumentOwnerNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("DocumentOwnerName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysEducationDegree>(entity =>
            {
                entity.HasKey(e => e.DegreeId)
                    .HasName("PK__HRM_SYS___4D9492CE187C8F58");

                entity.ToTable("HRM_SYS_EducationDegree");

                entity.Property(e => e.DegreeId).HasColumnName("DegreeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DegreeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("DegreeName_AR");

                entity.Property(e => e.DegreeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("DegreeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysEmployeeStatus>(entity =>
            {
                entity.HasKey(e => e.EmployeeStatusId);

                entity.ToTable("HRM_SYS_EmployeeStatus");

                entity.Property(e => e.EmployeeStatusId)
                    .ValueGeneratedNever()
                    .HasColumnName("EmployeeStatusID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeStatusNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("EmployeeStatusName_AR");

                entity.Property(e => e.EmployeeStatusNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("EmployeeStatusName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysEndOfServiceType>(entity =>
            {
                entity.HasKey(e => e.EndofServiceTypeId)
                    .HasName("PK__HRM_SYS___0CD2B712721D3D8C");

                entity.ToTable("HRM_SYS_EndOfServiceType");

                entity.Property(e => e.EndofServiceTypeId).HasColumnName("EndofServiceTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EndofServiceTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("EndofServiceTypeName_AR");

                entity.Property(e => e.EndofServiceTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("EndofServiceTypeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysFlightType>(entity =>
            {
                entity.HasKey(e => e.FlightTypeId)
                    .HasName("PK__HRM_SYS___68CE86FDC411220D");

                entity.ToTable("HRM_SYS_FlightType");

                entity.Property(e => e.FlightTypeId).HasColumnName("FlightTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FlightTypeNameAr)
                    .HasMaxLength(50)
                    .HasColumnName("FlightTypeName_AR");

                entity.Property(e => e.FlightTypeNameEn)
                    .HasMaxLength(50)
                    .HasColumnName("FlightTypeName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysGender>(entity =>
            {
                entity.HasKey(e => e.GenderId)
                    .HasName("PK__HRM_DEF___4E24E9F7723DD639");

                entity.ToTable("HRM_SYS_Gender");

                entity.Property(e => e.GenderId).HasColumnName("GenderID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.GenderNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("GenderName_AR");

                entity.Property(e => e.GenderNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("GenderName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysGosiConfiguration>(entity =>
            {
                entity.HasKey(e => e.GosiConfigurationId)
                    .HasName("PK__HRM_SYS___2F2D5EB638B76300");

                entity.ToTable("HRM_SYS_GosiConfiguration");

                entity.Property(e => e.GosiConfigurationId).HasColumnName("GosiConfigurationID");

                entity.Property(e => e.Formula)
                    .HasMaxLength(200)
                    .HasColumnName("formula");

                entity.Property(e => e.GosiFlagId).HasColumnName("GosiFlagID");
            });

            modelBuilder.Entity<HrmSysGosiFlag>(entity =>
            {
                entity.HasKey(e => e.GosiFlagId)
                    .HasName("PK__HRM_SYS___360DC2E973A84270");

                entity.ToTable("HRM_SYS_GosiFlag");

                entity.Property(e => e.GosiFlagId).HasColumnName("GosiFlagID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.GosiFlagNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("GosiFlagName_AR");

                entity.Property(e => e.GosiFlagNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("GosiFlagName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysLeaveType>(entity =>
            {
                entity.HasKey(e => e.LeaveTypesId)
                    .HasName("PK__HRM_SYS___3D028C3E42B6ABEC");

                entity.ToTable("HRM_SYS_LeaveType");

                entity.Property(e => e.LeaveTypesId).HasColumnName("LeaveTypesID");

                entity.Property(e => e.LeaveTypeAr)
                    .HasMaxLength(200)
                    .HasColumnName("LeaveType_AR");

                entity.Property(e => e.LeaveTypeEn)
                    .HasMaxLength(200)
                    .HasColumnName("LeaveType_EN");
            });

            modelBuilder.Entity<HrmSysMaritalStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_SYS_MaritalStatus");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MaritalStatusId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("MaritalStatusID");

                entity.Property(e => e.MaritalStatusNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("MaritalStatusName_AR");

                entity.Property(e => e.MaritalStatusNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("MaritalStatusName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmSysModule>(entity =>
            {
                entity.ToTable("HRM_SYS_Module");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ModuleName).HasMaxLength(150);

                entity.Property(e => e.Remarks).HasMaxLength(500);
            });

            modelBuilder.Entity<HrmSysNationality>(entity =>
            {
                entity.HasKey(e => e.NationalityId)
                    .HasName("PK__HRM_DEF___F628E7441F479E4C");

                entity.ToTable("HRM_SYS_Nationality");

                entity.Property(e => e.NationalityId).HasColumnName("NationalityID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NationalityNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("NationalityName_AR");

                entity.Property(e => e.NationalityNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("NationalityName_EN");
            });

            modelBuilder.Entity<HrmSysNoticePeriod>(entity =>
            {
                entity.HasKey(e => e.NoticePeriodId)
                    .HasName("PK__HRM_DEF___A3B86728108E96FD");

                entity.ToTable("HRM_SYS_NoticePeriod");

                entity.Property(e => e.NoticePeriodId).HasColumnName("NoticePeriodID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NoticePeriodNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("NoticePeriodName_AR");

                entity.Property(e => e.NoticePeriodNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("NoticePeriodName_EN");
            });

            modelBuilder.Entity<HrmSysPaymentMethod>(entity =>
            {
                entity.HasKey(e => e.PaymentMethodId);

                entity.ToTable("HRM_SYS_PaymentMethod");

                entity.Property(e => e.PaymentMethodId).HasColumnName("PaymentMethodID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PaymentName).HasMaxLength(100);
            });

            modelBuilder.Entity<HrmSysPhoneType>(entity =>
            {
                entity.HasKey(e => e.PhoneTypeId)
                    .HasName("PK_HRM_TRAN_PhoneType");

                entity.ToTable("HRM_SYS_PhoneType");

                entity.Property(e => e.PhoneTypeId).HasColumnName("PhoneTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PhoneTypeNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("PhoneTypeName_AR");

                entity.Property(e => e.PhoneTypeNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("PhoneTypeName_EN");
            });

            modelBuilder.Entity<HrmSysProbationPeriod>(entity =>
            {
                entity.HasKey(e => e.ProbationPeriodId)
                    .HasName("PK__HRM_DEF___FD6411107A3306DA");

                entity.ToTable("HRM_SYS_ProbationPeriod");

                entity.Property(e => e.ProbationPeriodId).HasColumnName("ProbationPeriodID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ProbationPeriodNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ProbationPeriodName_AR");

                entity.Property(e => e.ProbationPeriodNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ProbationPeriodName_EN");
            });

            modelBuilder.Entity<HrmSysPurpouse>(entity =>
            {
                entity.HasKey(e => e.PurpouseId)
                    .HasName("PK__HRM_SYS___6322CD5BB1B22FCE");

                entity.ToTable("HRM_SYS_Purpouse");

                entity.Property(e => e.PurpouseId).HasColumnName("PurpouseID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PurpouseAr)
                    .HasMaxLength(100)
                    .HasColumnName("Purpouse_AR");

                entity.Property(e => e.PurpouseEn)
                    .HasMaxLength(100)
                    .HasColumnName("Purpouse_EN");
            });

            modelBuilder.Entity<HrmSysReligion>(entity =>
            {
                entity.HasKey(e => e.ReligionId)
                    .HasName("PK__HRM_DEF___D3ADAB6A239456F4");

                entity.ToTable("HRM_SYS_Religion");

                entity.Property(e => e.ReligionId).HasColumnName("ReligionID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ReligionNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ReligionName_AR");

                entity.Property(e => e.ReligionNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ReligionName_EN");
            });

            modelBuilder.Entity<HrmSysRuleEngine>(entity =>
            {
                entity.HasKey(e => e.RuleEngineId)
                    .HasName("PK__HRM_SYS___43072CCB00B23E93");

                entity.ToTable("HRM_SYS_RuleEngine");

                entity.Property(e => e.RuleEngineId).HasColumnName("RuleEngineID");

                entity.Property(e => e.Condition).HasMaxLength(30);

                entity.Property(e => e.ConditionValue).HasMaxLength(30);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DependentPropertyName).HasMaxLength(30);

                entity.Property(e => e.ErrorMessageAr)
                    .HasMaxLength(100)
                    .HasColumnName("ErrorMessage_AR");

                entity.Property(e => e.ErrorMessageEn)
                    .HasMaxLength(100)
                    .HasColumnName("ErrorMessage_EN");

                entity.Property(e => e.GroupKey).HasMaxLength(30);

                entity.Property(e => e.Language).HasMaxLength(30);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PropertyName).HasMaxLength(30);

                entity.Property(e => e.RegularExpression).HasMaxLength(500);

                entity.Property(e => e.ScreenId).HasColumnName("ScreenID");

                entity.HasOne(d => d.Screen)
                    .WithMany(p => p.HrmSysRuleEngines)
                    .HasForeignKey(d => d.ScreenId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HRM_SYS_RuleEngine_HRM_SYS_Screen");
            });

            modelBuilder.Entity<HrmSysScreen>(entity =>
            {
                entity.HasKey(e => e.ScreenId)
                    .HasName("PK__HRM_SYS___0AB60F852B9C7DE2");

                entity.ToTable("HRM_SYS_Screen");

                entity.Property(e => e.ScreenId).HasColumnName("ScreenID");

                entity.Property(e => e.ActionName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AdditionalData)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ControllerName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ScreenNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("ScreenName_AR");

                entity.Property(e => e.ScreenNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("ScreenName_EN");
            });

            modelBuilder.Entity<HrmSysSystemConfiguration>(entity =>
            {
                entity.HasKey(e => e.SystemConfigurationId);

                entity.ToTable("HRM_SYS_SystemConfiguration");

                entity.Property(e => e.SystemConfigurationId).HasColumnName("SystemConfigurationID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.ModuleName).HasMaxLength(50);

                entity.Property(e => e.ModuleNameLabel).HasMaxLength(50);

                entity.Property(e => e.ParameterLabel).HasMaxLength(50);

                entity.Property(e => e.ParameterName).HasMaxLength(50);

                entity.Property(e => e.ParameterValue).HasMaxLength(50);

                entity.Property(e => e.SectionId).HasColumnName("SectionID");

                entity.Property(e => e.SectionName).HasMaxLength(50);

                entity.Property(e => e.SectionNameLabel).HasMaxLength(50);
            });

            modelBuilder.Entity<HrmSysTitle>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_SYS_Title");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TitleId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("TitleID");

                entity.Property(e => e.TitleNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("TitleName_AR");

                entity.Property(e => e.TitleNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("TitleName_EN");
            });

            modelBuilder.Entity<HrmTranAccountInfo>(entity =>
            {
                entity.HasKey(e => e.BankId)
                    .HasName("PK_HRM_TRAN_BankDetails");

                entity.ToTable("HRM_TRAN_AccountInfo");

                entity.Property(e => e.BankId).HasColumnName("BankID");

                entity.Property(e => e.AccountHolder).HasMaxLength(50);

                entity.Property(e => e.AccountNo).HasMaxLength(50);

                entity.Property(e => e.BankBranch).HasMaxLength(50);

                entity.Property(e => e.BankCode).HasMaxLength(50);

                entity.Property(e => e.BankName).HasMaxLength(30);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Ibanno)
                    .HasMaxLength(50)
                    .HasColumnName("IBANno");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranAccountInfos)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_HRM_TRAN_AccountInfo_HRM_TRAN_Employee");
            });

            modelBuilder.Entity<HrmTranAccountInfoAudit>(entity =>
            {
                entity.HasKey(e => e.AccountInfoAuditId)
                    .HasName("PK__HRM_TRAN__4B60A63D6014F6D2");

                entity.ToTable("HRM_TRAN_AccountInfo_Audit");

                entity.Property(e => e.AccountInfoAuditId).HasColumnName("AccountInfoAuditID");

                entity.Property(e => e.AccountHolder).HasMaxLength(50);

                entity.Property(e => e.BankBranch).HasMaxLength(50);

                entity.Property(e => e.BankCode).HasMaxLength(50);

                entity.Property(e => e.BankId).HasColumnName("BankID");

                entity.Property(e => e.BankName).HasMaxLength(30);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Ibanno)
                    .HasMaxLength(50)
                    .HasColumnName("IBANno");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmTranContactInfo>(entity =>
            {
                entity.HasKey(e => e.ContactInfoId);

                entity.ToTable("HRM_TRAN_ContactInfo");

                entity.Property(e => e.ContactInfoId).HasColumnName("ContactInfoID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(500);

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PrimaryMobileNumber).HasMaxLength(500);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranContactInfos)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__05A3D694");
            });

            modelBuilder.Entity<HrmTranContactInfoAudit>(entity =>
            {
                entity.HasKey(e => e.AuditContactInfoId)
                    .HasName("PK__HRM_TRAN__AA98479C532704D4");

                entity.ToTable("HRM_TRAN_ContactInfo_Audit");

                entity.Property(e => e.AuditContactInfoId).HasColumnName("AuditContactInfoID");

                entity.Property(e => e.ContactInfoId).HasColumnName("ContactInfoID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(500);

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LogDateTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PrimaryMobileNumber).HasMaxLength(500);

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmTranContract>(entity =>
            {
                entity.HasKey(e => e.ContractInformationId);

                entity.ToTable("HRM_TRAN_Contract");

                entity.Property(e => e.ContractInformationId).HasColumnName("ContractInformationID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.BusinessUnitId).HasColumnName("BusinessUnitID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.ContractEndDateAr)
                    .HasMaxLength(100)
                    .HasColumnName("ContractEndDate_AR");

                entity.Property(e => e.ContractEndDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("ContractEndDate_EN");

                entity.Property(e => e.ContractStartDateAr)
                    .HasMaxLength(100)
                    .HasColumnName("ContractStartDate_AR");

                entity.Property(e => e.ContractStartDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("ContractStartDate_EN");

                entity.Property(e => e.ContractTypeId).HasColumnName("ContractTypeID");

                entity.Property(e => e.CostCenterId).HasColumnName("CostCenterID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CurrentJobId).HasColumnName("CurrentJobID");

                entity.Property(e => e.DateOfHiringAr)
                    .HasMaxLength(100)
                    .HasColumnName("DateOfHiring_AR");

                entity.Property(e => e.DateOfHiringEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfHiring_EN");

                entity.Property(e => e.DateOfJoiningAr)
                    .HasMaxLength(100)
                    .HasColumnName("DateOfJoining_AR");

                entity.Property(e => e.DateOfJoiningEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfJoining_EN");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeeStatusId).HasColumnName("EmployeeStatusID");

                entity.Property(e => e.ExitReEntryVisa).HasColumnName("Exit_ReEntryVisa");

                entity.Property(e => e.GosiFlagId).HasColumnName("GosiFlagID");

                entity.Property(e => e.Gosinumber)
                    .HasMaxLength(200)
                    .HasColumnName("GOSINumber");

                entity.Property(e => e.Gosipoll)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("GOSIPoll");

                entity.Property(e => e.GosipollCompany)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("GOSIPollCompany");

                entity.Property(e => e.GosipollEmployee)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("GOSIPollEmployee");

                entity.Property(e => e.GradeId).HasColumnName("GradeID");

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.LineManagerId).HasColumnName("LineManagerID");

                entity.Property(e => e.MaxTicketRate).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaximumPerdiem).HasMaxLength(500);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NoticePeriodId).HasColumnName("NoticePeriodID");

                entity.Property(e => e.PayrollHoldComments).HasMaxLength(500);

                entity.Property(e => e.PositionId).HasColumnName("PositionID");

                entity.Property(e => e.ProbationPeriodEn)
                    .HasMaxLength(200)
                    .HasColumnName("ProbationPeriod_EN");

                entity.Property(e => e.ProbationPeriodId).HasColumnName("ProbationPeriodID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.SiteId).HasColumnName("SiteID");

                entity.Property(e => e.SponsorId).HasColumnName("SponsorID");

                entity.Property(e => e.TicketClass).HasMaxLength(500);

                entity.Property(e => e.TicketEndRoute).HasMaxLength(500);

                entity.Property(e => e.TicketFrequencyId).HasColumnName("TicketFrequencyID");

                entity.Property(e => e.TicketRemarks).HasMaxLength(500);

                entity.Property(e => e.TicketStartRoute).HasMaxLength(500);

                entity.Property(e => e.TicketTypeId).HasColumnName("TicketTypeID");

                entity.Property(e => e.VacationTypeId).HasColumnName("VacationTypeID");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranContracts)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__0C50D423");
            });

            modelBuilder.Entity<HrmTranContractAudit>(entity =>
            {
                entity.HasKey(e => e.ContractInformationAuditId)
                    .HasName("PK__HRM_TRAN__CB1DE5C7268586AE");

                entity.ToTable("HRM_TRAN_Contract_Audit");

                entity.Property(e => e.ContractInformationAuditId).HasColumnName("ContractInformationAuditID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.BusinessUnitId).HasColumnName("BusinessUnitID");

                entity.Property(e => e.ContractEndDateAr)
                    .HasMaxLength(100)
                    .HasColumnName("ContractEndDate_AR");

                entity.Property(e => e.ContractEndDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("ContractEndDate_EN");

                entity.Property(e => e.ContractInformationId).HasColumnName("ContractInformationID");

                entity.Property(e => e.ContractStartDateAr)
                    .HasMaxLength(100)
                    .HasColumnName("ContractStartDate_AR");

                entity.Property(e => e.ContractStartDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("ContractStartDate_EN");

                entity.Property(e => e.ContractTypeId).HasColumnName("ContractTypeID");

                entity.Property(e => e.CostCenterId).HasColumnName("CostCenterID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CurrentJobId).HasColumnName("CurrentJobID");

                entity.Property(e => e.DateOfHiringAr)
                    .HasMaxLength(100)
                    .HasColumnName("DateOfHiring_AR");

                entity.Property(e => e.DateOfHiringEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfHiring_EN");

                entity.Property(e => e.DateOfJoiningAr)
                    .HasMaxLength(100)
                    .HasColumnName("DateOfJoining_AR");

                entity.Property(e => e.DateOfJoiningEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfJoining_EN");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ExitReEntryVisa).HasColumnName("Exit_ReEntryVisa");

                entity.Property(e => e.GosiFlagId).HasColumnName("GosiFlagID");

                entity.Property(e => e.Gosinumber)
                    .HasMaxLength(200)
                    .HasColumnName("GOSINumber");

                entity.Property(e => e.Gosipoll)
                    .HasMaxLength(500)
                    .HasColumnName("GOSIPoll");

                entity.Property(e => e.GradeId).HasColumnName("GradeID");

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.LineManagerId).HasColumnName("LineManagerID");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.MaxTicketRate).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaximumPerdiem).HasMaxLength(500);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NoticePeriodId).HasColumnName("NoticePeriodID");

                entity.Property(e => e.PositionId).HasColumnName("PositionID");

                entity.Property(e => e.ProbationPeriodEn)
                    .HasMaxLength(200)
                    .HasColumnName("ProbationPeriod_EN");

                entity.Property(e => e.ProbationPeriodId).HasColumnName("ProbationPeriodID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.SiteId).HasColumnName("SiteID");

                entity.Property(e => e.SponsorId).HasColumnName("SponsorID");

                entity.Property(e => e.TicketClass).HasMaxLength(500);

                entity.Property(e => e.TicketEndRoute).HasMaxLength(500);

                entity.Property(e => e.TicketFrequencyId).HasColumnName("TicketFrequencyID");

                entity.Property(e => e.TicketRemarks).HasMaxLength(500);

                entity.Property(e => e.TicketStartRoute).HasMaxLength(500);

                entity.Property(e => e.TicketTypeId).HasColumnName("TicketTypeID");

                entity.Property(e => e.UserAction).HasMaxLength(20);

                entity.Property(e => e.VacationTypeId).HasColumnName("VacationTypeID");
            });

            modelBuilder.Entity<HrmTranDocumentsInfo>(entity =>
            {
                entity.HasKey(e => e.DocumentId);

                entity.ToTable("HRM_TRAN_DocumentsInfo");

                entity.Property(e => e.DocumentId).HasColumnName("DocumentID");

                entity.Property(e => e.Costing).HasMaxLength(30);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DocumentNumber).HasMaxLength(30);

                entity.Property(e => e.DocumentOwnerId).HasColumnName("DocumentOwnerID");

                entity.Property(e => e.DocumentStatus).HasMaxLength(30);

                entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentTypeID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ExpiryDateAr)
                    .HasMaxLength(30)
                    .HasColumnName("ExpiryDate_AR");

                entity.Property(e => e.ExpiryDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("ExpiryDate_EN");

                entity.Property(e => e.InsuranceClassId).HasColumnName("InsuranceClassID");

                entity.Property(e => e.InsurancePolicyNo).HasMaxLength(30);

                entity.Property(e => e.InsuranceProviderId).HasColumnName("InsuranceProviderID");

                entity.Property(e => e.InsuranceTypeId).HasColumnName("InsuranceTypeID");

                entity.Property(e => e.IssueDateAr)
                    .HasMaxLength(30)
                    .HasColumnName("IssueDate_AR");

                entity.Property(e => e.IssueDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("IssueDate_EN");

                entity.Property(e => e.IssueLocation).HasMaxLength(30);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NameInDocument).HasMaxLength(30);

                entity.Property(e => e.ProfessionInIqama).HasMaxLength(30);

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.ScannedDocument).HasMaxLength(500);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranDocumentsInfos)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__078C1F06");
            });

            modelBuilder.Entity<HrmTranDocumentsInfoAudit>(entity =>
            {
                entity.HasKey(e => e.DocumentAuditId)
                    .HasName("PK__HRM_TRAN__06C08E85388B6CD5");

                entity.ToTable("HRM_TRAN_DocumentsInfo_Audit");

                entity.Property(e => e.DocumentAuditId).HasColumnName("DocumentAuditID");

                entity.Property(e => e.Costing).HasMaxLength(30);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DocumentId).HasColumnName("DocumentID");

                entity.Property(e => e.DocumentNumber).HasMaxLength(30);

                entity.Property(e => e.DocumentOwnerId).HasColumnName("DocumentOwnerID");

                entity.Property(e => e.DocumentStatus).HasMaxLength(30);

                entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentTypeID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ExpiryDateAr)
                    .HasMaxLength(30)
                    .HasColumnName("ExpiryDate_AR");

                entity.Property(e => e.ExpiryDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("ExpiryDate_EN");

                entity.Property(e => e.InsuranceClassId).HasColumnName("InsuranceClassID");

                entity.Property(e => e.InsurancePolicyNo).HasMaxLength(30);

                entity.Property(e => e.InsuranceProviderId).HasColumnName("InsuranceProviderID");

                entity.Property(e => e.InsuranceTypeId).HasColumnName("InsuranceTypeID");

                entity.Property(e => e.IssueDateAr)
                    .HasMaxLength(30)
                    .HasColumnName("IssueDate_AR");

                entity.Property(e => e.IssueDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("IssueDate_EN");

                entity.Property(e => e.IssueLocation).HasMaxLength(30);

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NameInDocument).HasMaxLength(30);

                entity.Property(e => e.ProfessionInIqama).HasMaxLength(30);

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.ScannedDocument).HasMaxLength(500);

                entity.Property(e => e.UserAction).HasMaxLength(30);
            });

            modelBuilder.Entity<HrmTranEducationInfo>(entity =>
            {
                entity.HasKey(e => e.EducationId);

                entity.ToTable("HRM_TRAN_EducationInfo");

                entity.Property(e => e.EducationId).HasColumnName("EducationID");

                entity.Property(e => e.CollegeOrUniversity).HasMaxLength(50);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CourseTypeId).HasColumnName("CourseTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CurrentDegreeId).HasColumnName("CurrentDegreeID");

                entity.Property(e => e.DegreeId).HasColumnName("DegreeID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Qualification).HasMaxLength(50);

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.YearOfCertification).HasColumnType("datetime");

                entity.Property(e => e.YearOfCertificationAr)
                    .HasMaxLength(50)
                    .HasColumnName("YearOfCertification_AR");

                entity.Property(e => e.YearOfCertificationEn)
                    .HasColumnType("datetime")
                    .HasColumnName("YearOfCertification_EN");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranEducationInfos)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__09746778");
            });

            modelBuilder.Entity<HrmTranEducationInfoAudit>(entity =>
            {
                entity.HasKey(e => e.EducationInfoAuditId)
                    .HasName("PK__HRM_TRAN__AC5CD4A8546CF3CF");

                entity.ToTable("HRM_TRAN_EducationInfo_Audit");

                entity.Property(e => e.EducationInfoAuditId).HasColumnName("EducationInfoAuditID");

                entity.Property(e => e.CollegeOrUniversity).HasMaxLength(50);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CourseTypeId).HasColumnName("CourseTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CurrentDegreeId).HasColumnName("CurrentDegreeID");

                entity.Property(e => e.DegreeId).HasColumnName("DegreeID");

                entity.Property(e => e.EducationId).HasColumnName("EducationID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Qualification).HasMaxLength(50);

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.UserAction).HasMaxLength(20);

                entity.Property(e => e.YearOfCertification).HasColumnType("datetime");

                entity.Property(e => e.YearOfCertificationAr)
                    .HasMaxLength(50)
                    .HasColumnName("YearOfCertification_AR");

                entity.Property(e => e.YearOfCertificationEn)
                    .HasColumnType("datetime")
                    .HasColumnName("YearOfCertification_EN");
            });

            modelBuilder.Entity<HrmTranEmployee>(entity =>
            {
                entity.HasKey(e => e.EmployeeId)
                    .HasName("PK__HRM_TRAN__7AD04FF1B21CE8A9");

                entity.ToTable("HRM_TRAN_Employee");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeName).HasMaxLength(50);

                entity.Property(e => e.EmployeeNumber)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmTranEmployeeAddress>(entity =>
            {
                entity.HasKey(e => e.EmployeeAddressId)
                    .HasName("PK__HRM_TRAN__4EC9378B23BCEA75");

                entity.ToTable("HRM_TRAN_EmployeeAddress");

                entity.Property(e => e.EmployeeAddressId).HasColumnName("EmployeeAddressID");

                entity.Property(e => e.AddressLine1).HasMaxLength(500);

                entity.Property(e => e.AddressLine2).HasMaxLength(500);

                entity.Property(e => e.AddressTypeId).HasColumnName("AddressTypeID");

                entity.Property(e => e.City)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Pobox)
                    .HasMaxLength(30)
                    .HasColumnName("POBox");

                entity.HasOne(d => d.AddressType)
                    .WithMany(p => p.HrmTranEmployeeAddresses)
                    .HasForeignKey(d => d.AddressTypeId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeAddress_HRM_SYS_AddressType");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.HrmTranEmployeeAddresses)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK__HRM_TRAN___Count__6501FCD8");
            });

            modelBuilder.Entity<HrmTranEmployeeAddressAudit>(entity =>
            {
                entity.HasKey(e => e.EmployeeAddressAuditId)
                    .HasName("PK__HRM_TRAN__2D26A5978F4A951B");

                entity.ToTable("HRM_TRAN_EmployeeAddress_Audit");

                entity.Property(e => e.EmployeeAddressAuditId).HasColumnName("EmployeeAddressAuditID");

                entity.Property(e => e.AddressLine1).HasMaxLength(500);

                entity.Property(e => e.AddressLine2).HasMaxLength(500);

                entity.Property(e => e.AddressTypeId).HasColumnName("AddressTypeID");

                entity.Property(e => e.City)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeAddressId).HasColumnName("EmployeeAddressID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Pobox)
                    .HasMaxLength(30)
                    .HasColumnName("POBox");

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmTranEmployeeAsset>(entity =>
            {
                entity.HasKey(e => e.EmployeeAssetId)
                    .HasName("PK__HRM_TRAN__0028F42183ABEFE7");

                entity.ToTable("HRM_TRAN_EmployeeAsset");

                entity.Property(e => e.EmployeeAssetId).HasColumnName("EmployeeAssetID");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.DateOfIssueAr)
                    .HasMaxLength(30)
                    .HasColumnName("DateOfIssue_AR");

                entity.Property(e => e.DateOfIssueEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfIssue_EN");

                entity.Property(e => e.GeneralRemarks).HasMaxLength(500);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SharedRemarks).HasMaxLength(500);

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.HrmTranEmployeeAssets)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeAsset_HRM_DEF_AssetDetails");
            });

            modelBuilder.Entity<HrmTranEmployeeAssetAudit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_TRAN_EmployeeAsset_Audit");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.DateOfIssueAr)
                    .HasMaxLength(30)
                    .HasColumnName("DateOfIssue_AR");

                entity.Property(e => e.DateOfIssueEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfIssue_EN");

                entity.Property(e => e.EmployeeAssetAuditId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("EmployeeAssetAuditID");

                entity.Property(e => e.EmployeeAssetId).HasColumnName("EmployeeAssetID");

                entity.Property(e => e.GeneralRemarks).HasMaxLength(500);

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SharedRemarks).HasMaxLength(500);

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmTranEmployeeAssetMap>(entity =>
            {
                entity.HasKey(e => e.EmployeeAssetMapId)
                    .HasName("PK__HRM_TRAN__BFE18C4F382F5661");

                entity.ToTable("HRM_TRAN_EmployeeAssetMap");

                entity.Property(e => e.EmployeeAssetMapId).HasColumnName("EmployeeAssetMapID");

                entity.Property(e => e.DateOfReturnAr)
                    .HasMaxLength(100)
                    .HasColumnName("DateOfReturn_AR");

                entity.Property(e => e.DateOfReturnEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfReturn_EN");

                entity.Property(e => e.EmployeeAssetId).HasColumnName("EmployeeAssetID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ReasonOfReturn).HasMaxLength(500);

                entity.HasOne(d => d.EmployeeAsset)
                    .WithMany(p => p.HrmTranEmployeeAssetMaps)
                    .HasForeignKey(d => d.EmployeeAssetId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeAssetMap_HRM_TRAN_EmployeeAsset");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranEmployeeAssetMaps)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__3A179ED3");
            });

            modelBuilder.Entity<HrmTranEmployeeAssetMapAudit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_TRAN_EmployeeAssetMap_Audit");

                entity.Property(e => e.DateOfReturnAr)
                    .HasMaxLength(100)
                    .HasColumnName("DateOfReturn_AR");

                entity.Property(e => e.DateOfReturnEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfReturn_EN");

                entity.Property(e => e.EmployeeAssetId).HasColumnName("EmployeeAssetID");

                entity.Property(e => e.EmployeeAssetMapAuditId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("EmployeeAssetMapAuditID");

                entity.Property(e => e.EmployeeAssetMapId).HasColumnName("EmployeeAssetMapID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ReasonOfReturn).HasMaxLength(500);

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmTranEmployeeCancelRequestDetail>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_TRAN_EmployeeCancelRequestDetails");

                entity.Property(e => e.CancelServiceRequestId).HasColumnName("CancelServiceRequestID");

                entity.Property(e => e.CancelServiceRequestReferenceNo).HasMaxLength(200);

                entity.Property(e => e.CancelServiceRequestTypeId).HasColumnName("CancelServiceRequestTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.HasOne(d => d.EmployeeServiceRequest)
                    .WithMany()
                    .HasForeignKey(d => d.EmployeeServiceRequestId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeCancelRequestDetails_HRM_TRAN_EmployeeServiceRequest");
            });

            modelBuilder.Entity<HrmTranEmployeeDependant>(entity =>
            {
                entity.HasKey(e => e.EmployeeDependantId)
                    .HasName("PK_HRM_TRAN_EmployeeDependants");

                entity.ToTable("HRM_TRAN_EmployeeDependant");

                entity.Property(e => e.EmployeeDependantId).HasColumnName("EmployeeDependantID");

                entity.Property(e => e.AddressLine1).HasMaxLength(200);

                entity.Property(e => e.AddressLine2).HasMaxLength(200);

                entity.Property(e => e.AddressTypeId).HasColumnName("AddressTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirthAr)
                    .HasMaxLength(100)
                    .HasColumnName("DateOfBirth_AR");

                entity.Property(e => e.DateOfBirthEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfBirth_EN");

                entity.Property(e => e.DependantTypeId).HasColumnName("DependantTypeID");

                entity.Property(e => e.EmployeeAddressId).HasColumnName("EmployeeAddressID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.FirstNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("FirstName_AR");

                entity.Property(e => e.FirstNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("FirstName_EN");

                entity.Property(e => e.GenderId).HasColumnName("GenderID");

                entity.Property(e => e.IqamaExpiryDateAr)
                    .HasMaxLength(100)
                    .HasColumnName("IqamaExpiryDate_AR");

                entity.Property(e => e.IqamaExpiryDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("IqamaExpiryDate_EN");

                entity.Property(e => e.IqamaNumber).HasMaxLength(50);

                entity.Property(e => e.LastNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("LastName_AR");

                entity.Property(e => e.LastNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("LastName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NameInIqamaAr)
                    .HasMaxLength(200)
                    .HasColumnName("NameInIqama_AR");

                entity.Property(e => e.NameInIqamaEn)
                    .HasMaxLength(100)
                    .HasColumnName("NameInIqama_EN");

                entity.Property(e => e.PassportExpiryDateAr)
                    .HasMaxLength(100)
                    .HasColumnName("PassportExpiryDate_AR");

                entity.Property(e => e.PassportExpiryDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("PassportExpiryDate_EN");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranEmployeeDependants)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeDependant_HRM_TRAN_EmployeeAddress");
            });

            modelBuilder.Entity<HrmTranEmployeeDependantAudit>(entity =>
            {
                entity.HasKey(e => e.EmployeeDependantAuditId)
                    .HasName("PK__HRM_TRAN__5D4A8C27FBE7A52F");

                entity.ToTable("HRM_TRAN_EmployeeDependant_Audit");

                entity.Property(e => e.EmployeeDependantAuditId).HasColumnName("EmployeeDependantAuditID");

                entity.Property(e => e.AddressLine1).HasMaxLength(200);

                entity.Property(e => e.AddressLine2).HasMaxLength(200);

                entity.Property(e => e.AddressTypeId).HasColumnName("AddressTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirthAr)
                    .HasMaxLength(100)
                    .HasColumnName("DateOfBirth_AR");

                entity.Property(e => e.DateOfBirthEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfBirth_EN");

                entity.Property(e => e.DependantTypeId).HasColumnName("DependantTypeID");

                entity.Property(e => e.EmployeeAddressId).HasColumnName("EmployeeAddressID");

                entity.Property(e => e.EmployeeDependantId).HasColumnName("EmployeeDependantID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.FirstNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("FirstName_AR");

                entity.Property(e => e.FirstNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("FirstName_EN");

                entity.Property(e => e.GenderId).HasColumnName("GenderID");

                entity.Property(e => e.IqamaExpiryDateAr)
                    .HasMaxLength(100)
                    .HasColumnName("IqamaExpiryDate_AR");

                entity.Property(e => e.IqamaExpiryDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("IqamaExpiryDate_EN");

                entity.Property(e => e.IqamaNumber).HasMaxLength(50);

                entity.Property(e => e.LastNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("LastName_AR");

                entity.Property(e => e.LastNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("LastName_EN");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NameInIqamaAr)
                    .HasMaxLength(200)
                    .HasColumnName("NameInIqama_AR");

                entity.Property(e => e.NameInIqamaEn)
                    .HasMaxLength(100)
                    .HasColumnName("NameInIqama_EN");

                entity.Property(e => e.PassportExpiryDateAr)
                    .HasMaxLength(100)
                    .HasColumnName("PassportExpiryDate_AR");

                entity.Property(e => e.PassportExpiryDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("PassportExpiryDate_EN");

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmTranEmployeeEndOfServiceDetail>(entity =>
            {
                entity.HasKey(e => e.EmployeeEndOfServiceDetailsId);

                entity.ToTable("HRM_TRAN_EmployeeEndOfServiceDetails");

                entity.Property(e => e.EmployeeEndOfServiceDetailsId).HasColumnName("EmployeeEndOfServiceDetailsID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.EndofServiceTypeId).HasColumnName("EndofServiceTypeID");

                entity.Property(e => e.LastDateOfDuty).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OutofOfficeReasonId).HasColumnName("OutofOfficeReasonID");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");
            });

            modelBuilder.Entity<HrmTranEmployeeLoanRequestDetail>(entity =>
            {
                entity.HasKey(e => e.EmployeeLoanRequestDetailsId)
                    .HasName("PK__HRM_TRAN__534656680F183235");

                entity.ToTable("HRM_TRAN_EmployeeLoanRequestDetails");

                entity.Property(e => e.EmployeeLoanRequestDetailsId).HasColumnName("EmployeeLoanRequestDetailsID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.ExpectedDateOfLoan).HasColumnType("datetime");

                entity.Property(e => e.LoanAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.MonthlyRepaymentAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RepaymentEndOfDate).HasColumnType("datetime");

                entity.Property(e => e.RepaymentStartFrom).HasColumnType("datetime");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");
            });

            modelBuilder.Entity<HrmTranEmployeeMonthTimeSheet>(entity =>
            {
                entity.HasKey(e => e.EmployeeMonthTimeSheetId);

                entity.ToTable("HRM_TRAN_EmployeeMonthTimeSheet");

                entity.Property(e => e.EmployeeMonthTimeSheetId).HasColumnName("EmployeeMonthTimeSheetID");

                entity.Property(e => e.AbsAbsent).HasColumnName("ABS_Absent");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Day1).HasMaxLength(3);

                entity.Property(e => e.Day10).HasMaxLength(3);

                entity.Property(e => e.Day11).HasMaxLength(3);

                entity.Property(e => e.Day12).HasMaxLength(3);

                entity.Property(e => e.Day13).HasMaxLength(3);

                entity.Property(e => e.Day14).HasMaxLength(3);

                entity.Property(e => e.Day15).HasMaxLength(3);

                entity.Property(e => e.Day16).HasMaxLength(3);

                entity.Property(e => e.Day17).HasMaxLength(3);

                entity.Property(e => e.Day18).HasMaxLength(3);

                entity.Property(e => e.Day19).HasMaxLength(3);

                entity.Property(e => e.Day2).HasMaxLength(3);

                entity.Property(e => e.Day20).HasMaxLength(3);

                entity.Property(e => e.Day21).HasMaxLength(3);

                entity.Property(e => e.Day22).HasMaxLength(3);

                entity.Property(e => e.Day23).HasMaxLength(3);

                entity.Property(e => e.Day24).HasMaxLength(3);

                entity.Property(e => e.Day25).HasMaxLength(3);

                entity.Property(e => e.Day26).HasMaxLength(3);

                entity.Property(e => e.Day27).HasMaxLength(3);

                entity.Property(e => e.Day28).HasMaxLength(3);

                entity.Property(e => e.Day29).HasMaxLength(3);

                entity.Property(e => e.Day3).HasMaxLength(3);

                entity.Property(e => e.Day30).HasMaxLength(3);

                entity.Property(e => e.Day31).HasMaxLength(3);

                entity.Property(e => e.Day4).HasMaxLength(3);

                entity.Property(e => e.Day5).HasMaxLength(3);

                entity.Property(e => e.Day6).HasMaxLength(3);

                entity.Property(e => e.Day7).HasMaxLength(3);

                entity.Property(e => e.Day8).HasMaxLength(3);

                entity.Property(e => e.Day9).HasMaxLength(3);

                entity.Property(e => e.Doubleduty).HasMaxLength(50);

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeeName).HasMaxLength(500);

                entity.Property(e => e.EmployeeNumber).HasMaxLength(503);

                entity.Property(e => e.Ex2Excuse).HasColumnName("EX2_Excuse");

                entity.Property(e => e.ExeExcuse).HasColumnName("EXE_Excuse");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NhHired).HasColumnName("NH_Hired");

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.OtOverTime).HasColumnName("OT_OverTime");

                entity.Property(e => e.PayrollGroupId).HasColumnName("PayrollGroupID");

                entity.Property(e => e.RResigned).HasColumnName("R_Resigned");

                entity.Property(e => e.SicSickLeave).HasColumnName("SIC_SickLeave");

                entity.Property(e => e.UtTransfer).HasColumnName("UT_Transfer");

                entity.Property(e => e.UvUnpaidVacation).HasColumnName("UV_UnpaidVacation");

                entity.Property(e => e.VacVacation).HasColumnName("VAC_Vacation");

                entity.Property(e => e.WiWithdraw).HasColumnName("WI_Withdraw");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranEmployeeMonthTimeSheets)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeMonthTimeSheet_HRM_TRAN_Employee");
            });

            modelBuilder.Entity<HrmTranEmployeeNormalServiceRequestLeaveDetail>(entity =>
            {
                entity.HasKey(e => e.EmployeeNormalServiceRequestLeaveDetailsId);

                entity.ToTable("HRM_TRAN_EmployeeNormalServiceRequestLeaveDetails");

                entity.Property(e => e.EmployeeNormalServiceRequestLeaveDetailsId).HasColumnName("EmployeeNormalServiceRequestLeaveDetailsID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.LeaveTypesId).HasColumnName("LeaveTypesID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ReturnOn).HasColumnType("datetime");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.Property(e => e.ToDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmTranEmployeeOutofOfficeReasonDetail>(entity =>
            {
                entity.HasKey(e => e.EmployeeOutofOfficeReasonId);

                entity.ToTable("HRM_TRAN_EmployeeOutofOfficeReasonDetails");

                entity.Property(e => e.EmployeeOutofOfficeReasonId).HasColumnName("EmployeeOutofOfficeReasonID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.FromDate).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OutofOfficeReasonId).HasColumnName("OutofOfficeReasonID");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.Property(e => e.ToDate).HasMaxLength(50);

                entity.HasOne(d => d.OutofOfficeReason)
                    .WithMany(p => p.HrmTranEmployeeOutofOfficeReasonDetails)
                    .HasForeignKey(d => d.OutofOfficeReasonId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeOutofOfficeReasonDetails_HRM_DEF_OutofOfficeReason");
            });

            modelBuilder.Entity<HrmTranEmployeePayrollPackage>(entity =>
            {
                entity.HasKey(e => e.PayrollPackageId)
                    .HasName("PK__HRM_TRAN__99DFC69201D345B0000");

                entity.ToTable("HRM_TRAN_EmployeePayrollPackage");

                entity.Property(e => e.PayrollPackageId).HasColumnName("PayrollPackageID");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Formulae).HasMaxLength(200);

                entity.Property(e => e.FormulaeValue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.FrequencyId).HasColumnName("FrequencyID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PackageId).HasColumnName("PackageID");

                entity.Property(e => e.PayTypeId).HasColumnName("PayTypeID");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.ShortName)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.ValueTypeId).HasColumnName("ValueTypeID");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranEmployeePayrollPackages)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__1D7B600025");
            });

            modelBuilder.Entity<HrmTranEmployeePayrollPackageAudit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_TRAN_EmployeePayrollPackage_Audit");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Formulae).HasMaxLength(200);

                entity.Property(e => e.FormulaeValue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.FrequencyId).HasColumnName("FrequencyID");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PackageId).HasColumnName("PackageID");

                entity.Property(e => e.PayTypeId).HasColumnName("PayTypeID");

                entity.Property(e => e.PayrollPackageAuditId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("PayrollPackageAuditID");

                entity.Property(e => e.PayrollPackageId).HasColumnName("PayrollPackageID");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.ShortName)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.UserAction).HasMaxLength(500);

                entity.Property(e => e.ValueTypeId).HasColumnName("ValueTypeID");
            });

            modelBuilder.Entity<HrmTranEmployeePhone>(entity =>
            {
                entity.HasKey(e => e.PhoneId);

                entity.ToTable("HRM_TRAN_EmployeePhone");

                entity.Property(e => e.PhoneId).HasColumnName("PhoneID");

                entity.Property(e => e.AreaCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PhoneTypeId).HasColumnName("PhoneTypeID");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.HrmTranEmployeePhones)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeePhone_HRM_SYS_Country");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranEmployeePhones)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeePhone_HRM_TRAN_Employee");

                entity.HasOne(d => d.PhoneType)
                    .WithMany(p => p.HrmTranEmployeePhones)
                    .HasForeignKey(d => d.PhoneTypeId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeePhone_HRM_SYS_PhoneType");
            });

            modelBuilder.Entity<HrmTranEmployeePhoneAudit>(entity =>
            {
                entity.HasKey(e => e.EmployeePhoneAuditId)
                    .HasName("PK__HRM_TRAN__69EAEF36B99D83DD");

                entity.ToTable("HRM_TRAN_EmployeePhone_Audit");

                entity.Property(e => e.EmployeePhoneAuditId).HasColumnName("EmployeePhoneAuditID");

                entity.Property(e => e.AreaCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PhoneId).HasColumnName("PhoneID");

                entity.Property(e => e.PhoneTypeId).HasColumnName("PhoneTypeID");

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmTranEmployeeScheduleManagement>(entity =>
            {
                entity.HasKey(e => e.EmployeeSmid);

                entity.ToTable("HRM_TRAN_EmployeeScheduleManagement");

                entity.Property(e => e.EmployeeSmid).HasColumnName("EmployeeSMID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Day).HasMaxLength(100);

                entity.Property(e => e.EmployeeDaysId).HasColumnName("EmployeeDaysID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ToDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmTranEmployeeServiceRequest>(entity =>
            {
                entity.HasKey(e => e.EmployeeServiceRequestId)
                    .HasName("PK__HRM_TRAN__CA5A203E2F2FFC0C");

                entity.ToTable("HRM_TRAN_EmployeeServiceRequest");

                entity.HasIndex(e => e.EmployeeServiceRequestId, "IX_HRM_TRAN_EmployeeServiceRequest")
                    .IsUnique();

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NextEscalatonTime).HasColumnType("datetime");

                entity.Property(e => e.ReferenceDate).HasColumnType("datetime");

                entity.Property(e => e.ReferenceEmailId)
                    .HasMaxLength(100)
                    .HasColumnName("ReferenceEmailID");

                entity.Property(e => e.ReferenceId).HasColumnName("ReferenceID");

                entity.Property(e => e.ReferenceMobile).HasMaxLength(11);

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.Property(e => e.ServiceRequestRefNo).HasMaxLength(200);

                entity.Property(e => e.ServiceRequestTypeId).HasColumnName("ServiceRequestTypeID");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranEmployeeServiceRequests)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeServiceRequest_HRM_TRAN_Employee");
            });

            modelBuilder.Entity<HrmTranEmployeeTicketServiceRequestDetail>(entity =>
            {
                entity.HasKey(e => e.EmployeeTicketServiceRequestLeaveDetailsId)
                    .HasName("PK_HRM_TRAN_EmployeeTicketServiceRequestLeaveDetails");

                entity.ToTable("HRM_TRAN_EmployeeTicketServiceRequestDetails");

                entity.Property(e => e.EmployeeTicketServiceRequestLeaveDetailsId).HasColumnName("EmployeeTicketServiceRequestLeaveDetailsID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DependantTypeId).HasColumnName("DependantTypeID");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.ExpectedDepartureDate).HasColumnType("datetime");

                entity.Property(e => e.FlightClassId).HasColumnName("FlightClassID");

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.Property(e => e.SpecialComments).HasMaxLength(500);

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.Property(e => e.TravelTypeId).HasColumnName("TravelTypeID");

                entity.HasOne(d => d.FlightClass)
                    .WithMany(p => p.HrmTranEmployeeTicketServiceRequestDetails)
                    .HasForeignKey(d => d.FlightClassId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeTicketServiceRequestLeaveDetails_HRM_DEF_FlightClass");

                entity.HasOne(d => d.TravelType)
                    .WithMany(p => p.HrmTranEmployeeTicketServiceRequestDetails)
                    .HasForeignKey(d => d.TravelTypeId)
                    .HasConstraintName("FK_HRM_TRAN_EmployeeTicketServiceRequestDetails_HRM_DEF_TravelType");
            });

            modelBuilder.Entity<HrmTranEmployeeTimeSheetCalculation>(entity =>
            {
                entity.HasKey(e => e.EtcalculationId)
                    .HasName("PK__HRM_TRAN__BE3BB55F9AB1A71D");

                entity.ToTable("HRM_TRAN_EmployeeTimeSheetCalculation");

                entity.Property(e => e.EtcalculationId).HasColumnName("ETCalculationID");

                entity.Property(e => e.AbsAbsent).HasColumnName("ABS_Absent");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeeMonthTimeSheetId).HasColumnName("EmployeeMonthTimeSheetID");

                entity.Property(e => e.Ex2).HasColumnName("EX2");

                entity.Property(e => e.Exe).HasColumnName("EXE");

                entity.Property(e => e.NhHired).HasColumnName("NH_Hired");

                entity.Property(e => e.Ot).HasColumnName("OT");

                entity.Property(e => e.Sic).HasColumnName("SIC");

                entity.Property(e => e.Ut).HasColumnName("UT");

                entity.Property(e => e.Uv).HasColumnName("UV");

                entity.Property(e => e.Vac).HasColumnName("VAC");

                entity.Property(e => e.Wi).HasColumnName("WI");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranEmployeeTimeSheetCalculations)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__5C37ACAD");

                entity.HasOne(d => d.EmployeeMonthTimeSheet)
                    .WithMany(p => p.HrmTranEmployeeTimeSheetCalculations)
                    .HasForeignKey(d => d.EmployeeMonthTimeSheetId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__5D2BD0E6");
            });

            modelBuilder.Entity<HrmTranEmployeeTravelRequest>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_TRAN_EmployeeTravelRequest");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PurpouseId).HasColumnName("PurpouseID");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");
            });

            modelBuilder.Entity<HrmTranEmployeeTravelServiceRequestDetail>(entity =>
            {
                entity.HasKey(e => e.EmployeeTravelRequestDetailsId)
                    .HasName("PK__HRM_TRAN__0ACA1A576B9B8BD3");

                entity.ToTable("HRM_TRAN_EmployeeTravelServiceRequestDetails");

                entity.Property(e => e.EmployeeTravelRequestDetailsId).HasColumnName("EmployeeTravelRequestDetailsID");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.DepartureDateTime).HasMaxLength(200);

                entity.Property(e => e.DependantTypeId).HasColumnName("DependantTypeID");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.FlightClassId).HasColumnName("FlightClassID");

                entity.Property(e => e.FromCityId).HasColumnName("FromCityID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(200);

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.Property(e => e.ToCityId).HasColumnName("ToCityID");
            });

            modelBuilder.Entity<HrmTranEmployeeVacationBalanceUpdate>(entity =>
            {
                entity.HasKey(e => e.VacationBalanceId)
                    .HasName("PK__HRM_TRAN__CF37465117B37A04");

                entity.ToTable("HRM_TRAN_EmployeeVacationBalanceUpdate");

                entity.Property(e => e.VacationBalanceId).HasColumnName("VacationBalanceID");

                entity.Property(e => e.Days).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.EntryUserId).HasColumnName("EntryUserID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayrollId).HasColumnName("PayrollID");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranEmployeeVacationBalanceUpdates)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__644DCFC1");
            });

            modelBuilder.Entity<HrmTranEmployeeVacationServiceRequestDetail>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_TRAN_EmployeeVacationServiceRequestDetails");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NextEscalatonTime).HasColumnType("datetime");

                entity.Property(e => e.ReplacementEmployeeId).HasColumnName("ReplacementEmployeeID");

                entity.Property(e => e.ReturnDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.Property(e => e.TrafficViolation).HasMaxLength(100);

                entity.HasOne(d => d.EmployeeServiceRequest)
                    .WithMany()
                    .HasForeignKey(d => d.EmployeeServiceRequestId)
                    .HasConstraintName("FK__HRM_TRAN___Retur__37C5420D");
            });

            modelBuilder.Entity<HrmTranEmployeeVacationServiceRequestLeaveDetail>(entity =>
            {
                entity.HasKey(e => e.EmployeeVacationServiceRequestLeaveDetailsId)
                    .HasName("PK__HRM_TRAN__F48290B5AF72FAE2");

                entity.ToTable("HRM_TRAN_EmployeeVacationServiceRequestLeaveDetails");

                entity.Property(e => e.EmployeeVacationServiceRequestLeaveDetailsId).HasColumnName("EmployeeVacationServiceRequestLeaveDetailsID");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.LeaveTypesId).HasColumnName("LeaveTypesID");

                entity.Property(e => e.ReturnDate).HasColumnType("datetime");

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                entity.HasOne(d => d.LeaveTypes)
                    .WithMany(p => p.HrmTranEmployeeVacationServiceRequestLeaveDetails)
                    .HasForeignKey(d => d.LeaveTypesId)
                    .HasConstraintName("FK__HRM_TRAN___Leave__00AA174D");
            });

            modelBuilder.Entity<HrmTranEmployeeVsrreturnDetail>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_TRAN_EmployeeVSRReturnDetails");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ReturnOn).HasColumnType("datetime");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.HasOne(d => d.EmployeeServiceRequest)
                    .WithMany()
                    .HasForeignKey(d => d.EmployeeServiceRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HRM_TRAN_EMPVSRReturnDetails_HRM_TRAN_EmployeeServiceRequest");
            });

            modelBuilder.Entity<HrmTranExitReEntyServiceRequestDetail>(entity =>
            {
                entity.HasKey(e => e.ExitReEntryId);

                entity.ToTable("HRM_TRAN_ExitReEntyServiceRequestDetails");

                entity.Property(e => e.ExitReEntryId).HasColumnName("ExitReEntryID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RequiredExitReEntryDuration).HasColumnType("datetime");

                entity.Property(e => e.ReturnDateOfTravel).HasColumnType("datetime");

                entity.Property(e => e.ServiceRequestProcessStageId).HasColumnName("ServiceRequestProcessStageID");

                entity.Property(e => e.StartDateOfTravel).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmTranHroverView>(entity =>
            {
                entity.HasKey(e => e.HroverViewId)
                    .HasName("PK__HRM_TRAN__2DC415B52F3E8EF6");

                entity.ToTable("HRM_TRAN_HROverView");

                entity.Property(e => e.HroverViewId).HasColumnName("HROverViewID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ScreenSection)
                    .HasMaxLength(500)
                    .HasColumnName("Screen_Section");

                entity.Property(e => e.Warning).HasMaxLength(500);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranHroverViews)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__1B9317B3");
            });

            modelBuilder.Entity<HrmTranInsurance>(entity =>
            {
                entity.HasKey(e => e.InsuranceId);

                entity.ToTable("HRM_TRAN_Insurance");

                entity.Property(e => e.InsuranceId).HasColumnName("InsuranceID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ExpiryDateAr)
                    .HasMaxLength(30)
                    .HasColumnName("ExpiryDate_AR");

                entity.Property(e => e.ExpiryDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("ExpiryDate_EN");

                entity.Property(e => e.InsuranceClassId).HasColumnName("InsuranceClassID");

                entity.Property(e => e.InsuranceProviderId).HasColumnName("InsuranceProviderID");

                entity.Property(e => e.InsuranceTypeId).HasColumnName("InsuranceTypeID");

                entity.Property(e => e.IssueDateAr)
                    .HasMaxLength(30)
                    .HasColumnName("IssueDate_AR");

                entity.Property(e => e.IssueDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("IssueDate_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PolicyHolderName).HasMaxLength(100);

                entity.Property(e => e.PolicyNumber).HasMaxLength(30);

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.YearlyCost).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranInsurances)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_HRM_TRAN_Insurance_HRM_TRAN_Employee");

                entity.HasOne(d => d.InsuranceClass)
                    .WithMany(p => p.HrmTranInsurances)
                    .HasForeignKey(d => d.InsuranceClassId)
                    .HasConstraintName("FK_HRM_TRAN_Insurance_HRM_DEF_InsuranceClass");

                entity.HasOne(d => d.InsuranceProvider)
                    .WithMany(p => p.HrmTranInsurances)
                    .HasForeignKey(d => d.InsuranceProviderId)
                    .HasConstraintName("FK_HRM_TRAN_Insurance_HRM_DEF_InsuranceProvider");

                entity.HasOne(d => d.InsuranceType)
                    .WithMany(p => p.HrmTranInsurances)
                    .HasForeignKey(d => d.InsuranceTypeId)
                    .HasConstraintName("FK_HRM_TRAN_Insurance_HRM_DEF_InsuranceType");
            });

            modelBuilder.Entity<HrmTranInsuranceAudit>(entity =>
            {
                entity.HasKey(e => e.InsuranceAuditId)
                    .HasName("PK__HRM_TRAN__A18FF89CAC1B5BAE");

                entity.ToTable("HRM_TRAN_Insurance_Audit");

                entity.Property(e => e.InsuranceAuditId).HasColumnName("InsuranceAuditID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ExpiryDateAr)
                    .HasMaxLength(30)
                    .HasColumnName("ExpiryDate_AR");

                entity.Property(e => e.ExpiryDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("ExpiryDate_EN");

                entity.Property(e => e.InsuranceClassId).HasColumnName("InsuranceClassID");

                entity.Property(e => e.InsuranceId).HasColumnName("InsuranceID");

                entity.Property(e => e.InsuranceProviderId).HasColumnName("InsuranceProviderID");

                entity.Property(e => e.InsuranceTypeId).HasColumnName("InsuranceTypeID");

                entity.Property(e => e.IssueDateAr)
                    .HasMaxLength(30)
                    .HasColumnName("IssueDate_AR");

                entity.Property(e => e.IssueDateEn)
                    .HasColumnType("datetime")
                    .HasColumnName("IssueDate_EN");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PolicyHolderName).HasMaxLength(100);

                entity.Property(e => e.PolicyNumber).HasMaxLength(30);

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.UserAction).HasMaxLength(20);

                entity.Property(e => e.YearlyCost).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<HrmTranInvoice>(entity =>
            {
                entity.HasKey(e => e.InvoiceId);

                entity.ToTable("HRM_TRAN_Invoice");

                entity.Property(e => e.AmountBeforeTax).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.AmountDue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DiscountAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.InvoiceDueDate).HasColumnType("date");

                entity.Property(e => e.InvoiceNumber)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(150)
                    .HasColumnName("PONumber");

                entity.Property(e => e.SubTotal).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TaxAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TaxTariffPercentage).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TotalAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TotalPayment).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmTranInvoiceItem>(entity =>
            {
                entity.HasKey(e => e.ProductInvoiceMapId)
                    .HasName("PK_HRM_TRAN_ProductInvoiceMap");

                entity.ToTable("HRM_TRAN_InvoiceItems");

                entity.Property(e => e.ProductInvoiceMapId).HasColumnName("ProductInvoiceMapID");

                entity.Property(e => e.AmountBeforeTax).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.DiscountAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InvoiceNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SubTotal).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TaxAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TaxTariffPercentage).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TotalAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UnitPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmTranMapToContactInfo>(entity =>
            {
                entity.HasKey(e => e.MapContactInfoId)
                    .HasName("PK__HRM_TRAN__A77B38DBDE1A4B3A");

                entity.ToTable("HRM_TRAN_MapToContactInfo");

                entity.Property(e => e.MapContactInfoId).HasColumnName("MapContactInfoID");

                entity.Property(e => e.ContactAddress).HasMaxLength(500);

                entity.Property(e => e.ContactInfoId).HasColumnName("ContactInfoID");

                entity.Property(e => e.ContactPersonName).HasMaxLength(500);

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranMapToContactInfos)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_HRM_TRAN_MapToContactInfo_HRM_TRAN_Employee");
            });

            modelBuilder.Entity<HrmTranMapToContactInfoAudit>(entity =>
            {
                entity.HasKey(e => e.MapContactInfoAuditId)
                    .HasName("PK__HRM_TRAN__2C619B7FE2728E45");

                entity.ToTable("HRM_TRAN_MapToContactInfo_Audit");

                entity.Property(e => e.MapContactInfoAuditId).HasColumnName("MapContactInfoAuditID");

                entity.Property(e => e.ContactAddress).HasMaxLength(500);

                entity.Property(e => e.ContactInfoId).HasColumnName("ContactInfoID");

                entity.Property(e => e.ContactPersonName).HasMaxLength(500);

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.MapContactInfoId).HasColumnName("MapContactInfoID");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmTranPayroll>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_TRAN_Payroll");

                entity.Property(e => e.PayrollId).HasColumnName("PayrollID");

                entity.Property(e => e.PayrollMonth).HasColumnType("date");
            });

            modelBuilder.Entity<HrmTranPayrollEntry>(entity =>
            {
                entity.HasKey(e => e.PayrollEntryId);

                entity.ToTable("HRM_TRAN_PayrollEntry");

                entity.Property(e => e.PayrollEntryId).HasColumnName("PayrollEntryID");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayrollMonth).HasColumnType("date");

                entity.Property(e => e.PostedDate).HasColumnType("datetime");

                entity.Property(e => e.PreparationId).HasColumnName("PreparationID");
            });

            modelBuilder.Entity<HrmTranPayrollEntryAudit>(entity =>
            {
                entity.HasKey(e => e.AuditPayrollEntryId)
                    .HasName("PK__HRM_TRAN__0DF41217AFA4C6C0");

                entity.ToTable("HRM_TRAN_PayrollEntry_Audit");

                entity.Property(e => e.AuditPayrollEntryId).HasColumnName("AuditPayrollEntryID");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LogDateTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayrollEntryId).HasColumnName("PayrollEntryID");

                entity.Property(e => e.PayrollMonth).HasColumnType("date");

                entity.Property(e => e.PostedDate).HasColumnType("datetime");

                entity.Property(e => e.PreparationId).HasColumnName("PreparationID");

                entity.Property(e => e.UserAction).HasMaxLength(100);
            });

            modelBuilder.Entity<HrmTranPayrollGroupMapDelete>(entity =>
            {
                entity.HasKey(e => e.PayrollGroupMapId)
                    .HasName("PK__HRM_TRAN__B588238681EBC134");

                entity.ToTable("HRM_TRAN_PayrollGroupMap_Delete");

                entity.Property(e => e.PayrollGroupMapId).HasColumnName("PayrollGroupMapID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayrollGroupId).HasColumnName("PayrollGroupID");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranPayrollGroupMapDeletes)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__HRM_TRAN___Emplo__075714DC");
            });

            modelBuilder.Entity<HrmTranPayrollProcess>(entity =>
            {
                entity.HasKey(e => e.PayrollProcessId)
                    .HasName("PK__HRM_TRAN__343E7E2B1C300BB5");

                entity.ToTable("HRM_TRAN_PayrollProcess");

                entity.Property(e => e.PayrollProcessId).HasColumnName("PayrollProcessID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.BusinessUnitId).HasColumnName("BusinessUnitID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.NetPay).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PayrollGroupId).HasColumnName("PayrollGroupID");

                entity.Property(e => e.PayrollPeriodId).HasColumnName("PayrollPeriodID");

                entity.Property(e => e.PostedDate).HasColumnType("datetime");

                entity.Property(e => e.ProcessDate).HasColumnType("datetime");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.SiteId).HasColumnName("SiteID");

                entity.Property(e => e.TotalDeductions).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TotalEarnings).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<HrmTranPayrollProcessDetail>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HRM_TRAN_PayrollProcessDetails");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.PayTypeId).HasColumnName("PayTypeID");

                entity.Property(e => e.PayrollGroupId).HasColumnName("PayrollGroupID");

                entity.Property(e => e.PayrollProcessDetailsId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("PayrollProcessDetailsID");

                entity.Property(e => e.PayrollProcessId).HasColumnName("PayrollProcessID");
            });

            modelBuilder.Entity<HrmTranPayscale>(entity =>
            {
                entity.HasKey(e => e.PayscaleId);

                entity.ToTable("HRM_TRAN_Payscale");

                entity.Property(e => e.PayscaleId).HasColumnName("PayscaleID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Formulae).HasMaxLength(500);

                entity.Property(e => e.FormulaeValue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.FrequencyId).HasColumnName("FrequencyID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PackageId).HasColumnName("PackageID");

                entity.Property(e => e.PayTypeId).HasColumnName("PayTypeID");

                entity.Property(e => e.PayValue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ValueTypeId).HasColumnName("ValueTypeID");

                entity.HasOne(d => d.Package)
                    .WithMany(p => p.HrmTranPayscales)
                    .HasForeignKey(d => d.PackageId)
                    .HasConstraintName("FK_HRM_TRAN_Payscale_HRM_DEF_Package1");
            });

            modelBuilder.Entity<HrmTranPersonalInfo>(entity =>
            {
                entity.HasKey(e => e.PersonalInfoId)
                    .HasName("PK__HRM_TRAN__3214EC0727639C5F");

                entity.ToTable("HRM_TRAN_PersonalInfo");

                entity.Property(e => e.PersonalInfoId).HasColumnName("PersonalInfoID");

                entity.Property(e => e.BloodGroupId).HasColumnName("BloodGroupID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirthAr)
                    .HasMaxLength(30)
                    .HasColumnName("DateOfBirth_AR");

                entity.Property(e => e.DateOfBirthEn)
                    .HasColumnType("date")
                    .HasColumnName("DateOfBirth_EN");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeePicture).HasMaxLength(100);

                entity.Property(e => e.FirstNameAr)
                    .HasMaxLength(30)
                    .HasColumnName("FirstName_AR");

                entity.Property(e => e.FirstNameEn)
                    .HasMaxLength(30)
                    .HasColumnName("FirstName_EN");

                entity.Property(e => e.FullNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("FullName_AR");

                entity.Property(e => e.FullNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("FullName_EN");

                entity.Property(e => e.GenderId).HasColumnName("GenderID");

                entity.Property(e => e.LastNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("LastName_AR");

                entity.Property(e => e.LastNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("LastName_EN");

                entity.Property(e => e.MaritalStatusId).HasColumnName("MaritalStatusID");

                entity.Property(e => e.MiddleNameAr)
                    .HasMaxLength(30)
                    .HasColumnName("MiddleName_AR");

                entity.Property(e => e.MiddleNameEn)
                    .HasMaxLength(30)
                    .HasColumnName("MiddleName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NationalityId).HasColumnName("NationalityID");

                entity.Property(e => e.PersonTypeId).HasColumnName("PersonTypeID");

                entity.Property(e => e.ReligionId).HasColumnName("ReligionID");

                entity.Property(e => e.TitleId).HasColumnName("TitleID");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.HrmTranPersonalInfos)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_HRM_TRAN_PersonalInfo_HRM_TRAN_Employee");
            });

            modelBuilder.Entity<HrmTranPersonalInfoAudit>(entity =>
            {
                entity.HasKey(e => e.AuditPersonalInfoId)
                    .HasName("PK__HRM_TRAN__18588E5D691425F1");

                entity.ToTable("HRM_TRAN_PersonalInfo_AUDIT");

                entity.Property(e => e.AuditPersonalInfoId).HasColumnName("AuditPersonalInfoID");

                entity.Property(e => e.BloodGroupId).HasColumnName("BloodGroupID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirthAr)
                    .HasMaxLength(30)
                    .HasColumnName("DateOfBirth_AR");

                entity.Property(e => e.DateOfBirthEn)
                    .HasColumnType("date")
                    .HasColumnName("DateOfBirth_EN");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeePicture).HasMaxLength(100);

                entity.Property(e => e.FirstNameAr)
                    .HasMaxLength(30)
                    .HasColumnName("FirstName_AR");

                entity.Property(e => e.FirstNameEn)
                    .HasMaxLength(30)
                    .HasColumnName("FirstName_EN");

                entity.Property(e => e.FullNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("FullName_AR");

                entity.Property(e => e.FullNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("FullName_EN");

                entity.Property(e => e.GenderId).HasColumnName("GenderID");

                entity.Property(e => e.LastNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("LastName_AR");

                entity.Property(e => e.LastNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("LastName_EN");

                entity.Property(e => e.LogDateTime).HasColumnType("datetime");

                entity.Property(e => e.MaritalStatusId).HasColumnName("MaritalStatusID");

                entity.Property(e => e.MiddleNameAr)
                    .HasMaxLength(30)
                    .HasColumnName("MiddleName_AR");

                entity.Property(e => e.MiddleNameEn)
                    .HasMaxLength(30)
                    .HasColumnName("MiddleName_EN");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NationalityId).HasColumnName("NationalityID");

                entity.Property(e => e.PersonTypeId).HasColumnName("PersonTypeID");

                entity.Property(e => e.PersonalInfoId).HasColumnName("PersonalInfoID");

                entity.Property(e => e.ReligionId).HasColumnName("ReligionID");

                entity.Property(e => e.TitleId).HasColumnName("TitleID");

                entity.Property(e => e.UserAction).HasMaxLength(20);
            });

            modelBuilder.Entity<HrmTranPreparation>(entity =>
            {
                entity.HasKey(e => e.PreparationId);

                entity.ToTable("HRM_TRAN_Preparation");

                entity.Property(e => e.PreparationId).HasColumnName("PreparationID");

                entity.Property(e => e.AdjustmentAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DaysofBasic).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.FrequencyId).HasColumnName("FrequencyID");

                entity.Property(e => e.HoursofBasic).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayTypeId).HasColumnName("PayTypeID");

                entity.Property(e => e.PaymentMethodId).HasColumnName("PaymentMethodID");

                entity.Property(e => e.PayrollPeriodId).HasColumnName("PayrollPeriodID");

                entity.Property(e => e.PercentageOfBasic).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ReferenceDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.UntilDate).HasColumnType("date");
            });

            modelBuilder.Entity<HrmTranPreparationAudit>(entity =>
            {
                entity.HasKey(e => e.AuditPreparationId)
                    .HasName("PK__HRM_TRAN__BC6AD6A4F1BCCF57");

                entity.ToTable("HRM_TRAN_Preparation_Audit");

                entity.Property(e => e.AuditPreparationId).HasColumnName("AuditPreparationID");

                entity.Property(e => e.AdjustmentAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DaysofBasic).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.FrequencyId).HasColumnName("FrequencyID");

                entity.Property(e => e.HoursofBasic).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.LogDateTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PayTypeId).HasColumnName("PayTypeID");

                entity.Property(e => e.PaymentMethodId).HasColumnName("PaymentMethodID");

                entity.Property(e => e.PayrollPeriodId).HasColumnName("PayrollPeriodID");

                entity.Property(e => e.PercentageOfBasic).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PreparationId).HasColumnName("PreparationID");

                entity.Property(e => e.ReferenceDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.UntilDate).HasColumnType("date");

                entity.Property(e => e.UserAction).HasMaxLength(100);
            });

            modelBuilder.Entity<HrmTranServiceRequestDocumentDetail>(entity =>
            {
                entity.HasKey(e => e.ServiceRequestDocumentsId)
                    .HasName("PK__HRM_TRAN__EA52F2F349E3F248");

                entity.ToTable("HRM_TRAN_ServiceRequestDocumentDetails");

                entity.Property(e => e.ServiceRequestDocumentsId).HasColumnName("ServiceRequestDocumentsID");

                entity.Property(e => e.DocumentName).HasMaxLength(500);

                entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentTypeID");

                entity.Property(e => e.EmployeeServiceRequestId).HasColumnName("EmployeeServiceRequestID");

                entity.Property(e => e.FilePath).HasMaxLength(500);

                entity.Property(e => e.ServiceRequestStageId).HasColumnName("ServiceRequestStageID");

                entity.Property(e => e.UploadedDate).HasColumnType("datetime");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.HrmTranServiceRequestDocumentDetails)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .HasConstraintName("FK_HRM_TRAN_ServiceRequestDocumentDetails_HRM_DEF_DocumentType");

                entity.HasOne(d => d.EmployeeServiceRequest)
                    .WithMany(p => p.HrmTranServiceRequestDocumentDetails)
                    .HasForeignKey(d => d.EmployeeServiceRequestId)
                    .HasConstraintName("FK_HRM_TRAN_ServiceRequestDocumentDetails_HRM_TRAN_EmployeeServiceRequest");
            });

            modelBuilder.Entity<Invitation>(entity =>
            {
                entity.Property(e => e.CreationDateTime).HasPrecision(0);

                entity.Property(e => e.ResponseDateTime).HasPrecision(0);
            });

            modelBuilder.Entity<PracticeTable>(entity =>
            {
                entity.HasKey(e => e.EmployeeId);

                entity.ToTable("PracticeTable");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.DateOfBirthAr)
                    .HasMaxLength(50)
                    .HasColumnName("DateOfBirth_AR");

                entity.Property(e => e.DateOfBirthEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfBirth_EN");

                entity.Property(e => e.EmployeeName).HasMaxLength(50);

                entity.HasOne(d => d.AssetType)
                    .WithMany(p => p.PracticeTables)
                    .HasForeignKey(d => d.AssetTypeId)
                    .HasConstraintName("FK_PracticeTable_HRM_DEF_AssetType");
            });

            modelBuilder.Entity<Table2>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Table_2");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Value).HasMaxLength(50);
            });

            modelBuilder.Entity<TblAudit>(entity =>
            {
                entity.HasKey(e => e.AuditId)
                    .HasName("PK__TBL_AUDI__A17F23B809789D8B");

                entity.ToTable("TBL_AUDIT");

                entity.Property(e => e.AuditId).HasColumnName("AuditID");

                entity.Property(e => e.ColumnName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentValue)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LoginId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("LoginID");

                entity.Property(e => e.NewValue)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pkvalue)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("PKValue");

                entity.Property(e => e.TableName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TestForPractice>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TestForPractice");

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.CountryId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("CountryID");

                entity.Property(e => e.CountryNameEn)
                    .HasMaxLength(100)
                    .HasColumnName("CountryName_EN");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirthAr)
                    .HasMaxLength(30)
                    .HasColumnName("DateOfBirth_AR");

                entity.Property(e => e.DateOfBirthEn)
                    .HasColumnType("datetime")
                    .HasColumnName("DateOfBirth_EN");

                entity.Property(e => e.DocumentOwnerNameAr)
                    .HasMaxLength(100)
                    .HasColumnName("DocumentOwnerName_AR");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.AssetType)
                    .WithMany()
                    .HasForeignKey(d => d.AssetTypeId)
                    .HasConstraintName("FK__TestForPr__Asset__2057CCD0");
            });

            modelBuilder.Entity<Testtable>(entity =>
            {
                entity.ToTable("testtable");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Col1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("col1");

                entity.Property(e => e.Col2)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("col2");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
