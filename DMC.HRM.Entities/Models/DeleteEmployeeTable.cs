﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class DeleteEmployeeTable
    {
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long? AssetTypeId { get; set; }
        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
    }
}
