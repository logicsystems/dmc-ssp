﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmAuditServiceRequestStage
    {
        public long ServiceRequestStageId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? EntryBy { get; set; }
        public string Remarks { get; set; }
        public bool? InProcess { get; set; }
        public int? ActionId { get; set; }
        public int? ServiceRequestProcessStageId { get; set; }
        public long? AssignedEmployeeId { get; set; }
        public bool? IsCancel { get; set; }
        public bool? IsRejected { get; set; }
        public bool? IsApproved { get; set; }
    }
}
