﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefApprovalAuthorityMatrix
    {
        public long ApprovalAuthorityMatrixId { get; set; }
        public long ApprovalAuthorityId { get; set; }
        public long CompanyId { get; set; }
        public long? BranchId { get; set; }
        public long? DepartmentId { get; set; }
        public long? PositionId { get; set; }
        public long ManagerEmployeeId { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
