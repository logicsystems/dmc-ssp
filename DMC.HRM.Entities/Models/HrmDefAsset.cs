﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefAsset
    {
        public HrmDefAsset()
        {
            HrmTranEmployeeAssets = new HashSet<HrmTranEmployeeAsset>();
        }

        public long AssetId { get; set; }
        public long? AssetTypeId { get; set; }
        public string AssetNumber { get; set; }
        public string Make { get; set; }
        public string ModelNumber { get; set; }
        public DateTime? MakeYearEn { get; set; }
        public string MakeYearAr { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Barcode { get; set; }
        public string Remarks { get; set; }
        public bool? AllowSharing { get; set; }

        public virtual HrmDefAssetType AssetType { get; set; }
        public virtual ICollection<HrmTranEmployeeAsset> HrmTranEmployeeAssets { get; set; }
    }
}
