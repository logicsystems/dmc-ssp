﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefAssetType
    {
        public HrmDefAssetType()
        {
            HrmDefAssets = new HashSet<HrmDefAsset>();
            PracticeTables = new HashSet<PracticeTable>();
        }

        public long AssetTypeId { get; set; }
        public string AssetNameEn { get; set; }
        public string AssetNameAr { get; set; }
        public string AssetTypeDescription { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmDefAsset> HrmDefAssets { get; set; }
        public virtual ICollection<PracticeTable> PracticeTables { get; set; }
    }
}
