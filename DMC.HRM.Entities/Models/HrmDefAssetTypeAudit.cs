﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefAssetTypeAudit
    {
        public long AssetTypeAuditId { get; set; }
        public long? AssetTypeId { get; set; }
        public string AssetNameEn { get; set; }
        public string AssetNameAr { get; set; }
        public string AssetTypeDescription { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public DateTime? LogDate { get; set; }
        public string UserAction { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
