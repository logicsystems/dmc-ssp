﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefAttendance
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan? InTime { get; set; }
        public TimeSpan? OutTime { get; set; }
        public string TotNetWorkTime { get; set; }
        public string NetWorkTime { get; set; }
        public int Status { get; set; }
        public string Remarks { get; set; }
        public long? CreatedBy { get; set; }
        public long? CreatedOn { get; set; }
        public bool? IsActive { get; set; }
    }
}
