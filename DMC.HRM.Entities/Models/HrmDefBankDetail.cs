﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefBankDetail
    {
        public int AccountId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string BankNameEn { get; set; }
        public string BankNameAr { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string AccountNumber { get; set; }
        public string Iban { get; set; }
        public int? CompanyId { get; set; }
        public bool? IsDefaultConfig { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
