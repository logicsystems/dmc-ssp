﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefBranch
    {
        public long BranchId { get; set; }
        public string BranchNameEn { get; set; }
        public string BranchNameAr { get; set; }
        public string BranchDescription { get; set; }
        public bool? BusinessUnitId { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Remarks { get; set; }
        public long? CompanyId { get; set; }
    }
}
