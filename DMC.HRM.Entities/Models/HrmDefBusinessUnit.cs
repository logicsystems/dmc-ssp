﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefBusinessUnit
    {
        public long BusinessUnitId { get; set; }
        public string BusinessUnitNameEn { get; set; }
        public string BusinessUnitNameAr { get; set; }
        public string BusinessUnitDescription { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
