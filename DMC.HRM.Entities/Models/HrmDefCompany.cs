﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefCompany
    {
        public long CompanyId { get; set; }
        public string CompanyNameEn { get; set; }
        public string CompanyNameAr { get; set; }
        public string CompanyDescription { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? IsSystem { get; set; }
        public string AddressEn { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string ZipCode { get; set; }
        public string AddressAr { get; set; }
        public string Country { get; set; }
        public string Vatnumber { get; set; }
        public string Logo { get; set; }
        public string Crnumber { get; set; }
        public string Ccnumber { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string EmailId { get; set; }
        public string Website { get; set; }
        public bool? IsDefault { get; set; }
        public string PaymentNote { get; set; }
        public string InvoiceNote { get; set; }
        public bool? IsInvoice { get; set; }
    }
}
