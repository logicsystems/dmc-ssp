﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefCurrency
    {
        public int CurrencyId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string Symbol { get; set; }
        public string Description { get; set; }
        public bool? IsDefault { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
