﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefCustomer
    {
        public long CustomerId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public int? CompanyId { get; set; }
        public string AddressEn { get; set; }
        public string AddressAr { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Industry { get; set; }
        public string Vatnumber { get; set; }
        public string ContactName { get; set; }
        public string MobileNumber { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string EmailId { get; set; }
        public string Website { get; set; }
        public bool? IsDefaultConfig { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
