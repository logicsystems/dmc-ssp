﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefDocumentType
    {
        public HrmDefDocumentType()
        {
            HrmTranServiceRequestDocumentDetails = new HashSet<HrmTranServiceRequestDocumentDetail>();
        }

        public long DocumentTypeId { get; set; }
        public string DocumentTypeNameEn { get; set; }
        public string DocumentTypeNameAr { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string DocumentDescription { get; set; }

        public virtual ICollection<HrmTranServiceRequestDocumentDetail> HrmTranServiceRequestDocumentDetails { get; set; }
    }
}
