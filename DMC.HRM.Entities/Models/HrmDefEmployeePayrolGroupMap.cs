﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefEmployeePayrolGroupMap
    {
        public long PayrollGroupId { get; set; }
        public long EmployeeId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmDefPayrollGroup PayrollGroup { get; set; }
    }
}
