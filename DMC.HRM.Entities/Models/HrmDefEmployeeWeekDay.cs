﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefEmployeeWeekDay
    {
        public long EmployeeWeekDaysId { get; set; }
        public string EmplyeeWeekDayEn { get; set; }
        public string EmplyeeWeekDayAr { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
