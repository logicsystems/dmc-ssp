﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefFlightClass
    {
        public HrmDefFlightClass()
        {
            HrmTranEmployeeTicketServiceRequestDetails = new HashSet<HrmTranEmployeeTicketServiceRequestDetail>();
        }

        public long FlightClassId { get; set; }
        public string FlightClassNameAr { get; set; }
        public string FlightClassNameEn { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmTranEmployeeTicketServiceRequestDetail> HrmTranEmployeeTicketServiceRequestDetails { get; set; }
    }
}
