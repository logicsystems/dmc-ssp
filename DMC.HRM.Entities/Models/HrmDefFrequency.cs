﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefFrequency
    {
        public long FrequencyId { get; set; }
        public string FrequencyNameEn { get; set; }
        public string FrequencyNameAr { get; set; }
        public string FrequencyDescription { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
        public int? DurationInDays { get; set; }
        public int? DurationInMonth { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
