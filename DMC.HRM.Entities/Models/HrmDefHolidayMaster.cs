﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefHolidayMaster
    {
        public HrmDefHolidayMaster()
        {
            HrmDefListOfHolidays = new HashSet<HrmDefListOfHoliday>();
        }

        public int HolidayCalanderId { get; set; }
        public string CalanderNameEn { get; set; }
        public string CalanderNameAr { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<HrmDefListOfHoliday> HrmDefListOfHolidays { get; set; }
    }
}
