﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefInsuranceClass
    {
        public HrmDefInsuranceClass()
        {
            HrmTranInsurances = new HashSet<HrmTranInsurance>();
        }

        public long InsuranceClassId { get; set; }
        public string InsuranceClassNameEn { get; set; }
        public string InsuranceClassNameAr { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmTranInsurance> HrmTranInsurances { get; set; }
    }
}
