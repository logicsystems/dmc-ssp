﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefInsuranceProvider
    {
        public HrmDefInsuranceProvider()
        {
            HrmTranInsurances = new HashSet<HrmTranInsurance>();
        }

        public long InsuranceProviderId { get; set; }
        public string InsuranceProviderNameEn { get; set; }
        public string InsuranceProviderNameAr { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmTranInsurance> HrmTranInsurances { get; set; }
    }
}
