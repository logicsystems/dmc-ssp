﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefLineManager
    {
        public long LineManagerId { get; set; }
        public string LineManagerNameEn { get; set; }
        public string LineManagerNameAr { get; set; }
        public int IsActive { get; set; }
        public int IsSystem { get; set; }
        public int? CreatedBy { get; set; }
        public int? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public int? ModifiedDate { get; set; }
    }
}
