﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefListOfHoliday
    {
        public long HolidayId { get; set; }
        public int HolidayCalanderId { get; set; }
        public string HolidayDescriptionEn { get; set; }
        public string HolidayDescriptionAr { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? HolidayDate { get; set; }
        public bool? IsSystem { get; set; }

        public virtual HrmDefHolidayMaster HolidayCalander { get; set; }
    }
}
