﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefMenu
    {
        public HrmDefMenu()
        {
            HrmDefRoleMenuMappings = new HashSet<HrmDefRoleMenuMapping>();
            InverseParentMenu = new HashSet<HrmDefMenu>();
        }

        public long MenuId { get; set; }
        public string MenuLabelEn { get; set; }
        public string MenuLabelAr { get; set; }
        public string MenuDescription { get; set; }
        public long? ParentMenuId { get; set; }
        public long? ScreenId { get; set; }
        public int? MenuType { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CssClass { get; set; }

        public virtual HrmDefMenu ParentMenu { get; set; }
        public virtual HrmSysScreen Screen { get; set; }
        public virtual ICollection<HrmDefRoleMenuMapping> HrmDefRoleMenuMappings { get; set; }
        public virtual ICollection<HrmDefMenu> InverseParentMenu { get; set; }
    }
}
