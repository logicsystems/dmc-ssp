﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefOutofOfficeReason
    {
        public HrmDefOutofOfficeReason()
        {
            HrmTranEmployeeOutofOfficeReasonDetails = new HashSet<HrmTranEmployeeOutofOfficeReasonDetail>();
        }

        public long OutofOfficeReasonId { get; set; }
        public string OutofOfficeReasonNameEn { get; set; }
        public string OutofOfficeReasonNameAr { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmTranEmployeeOutofOfficeReasonDetail> HrmTranEmployeeOutofOfficeReasonDetails { get; set; }
    }
}
