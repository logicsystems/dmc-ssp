﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefPackage
    {
        public HrmDefPackage()
        {
            HrmTranPayscales = new HashSet<HrmTranPayscale>();
        }

        public long PackageId { get; set; }
        public string PackageNameEn { get; set; }
        public string PackageNameAr { get; set; }
        public string PackageDescription { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<HrmTranPayscale> HrmTranPayscales { get; set; }
    }
}
