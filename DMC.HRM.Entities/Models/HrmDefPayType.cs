﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefPayType
    {
        public HrmDefPayType()
        {
            HrmDefPayTypeFormulaes = new HashSet<HrmDefPayTypeFormulae>();
            HrmDefPayTypeSettings = new HashSet<HrmDefPayTypeSetting>();
        }

        public long PayTypeId { get; set; }
        public string PayTypeNameEn { get; set; }
        public string PayTypeNameAr { get; set; }
        public string PayTypeDescription { get; set; }
        public int? PayTypeGroupId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
        public bool? IsFixed { get; set; }
        public bool? IsCompulsory { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmDefPayTypeFormulae> HrmDefPayTypeFormulaes { get; set; }
        public virtual ICollection<HrmDefPayTypeSetting> HrmDefPayTypeSettings { get; set; }
    }
}
