﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefPayTypeFormulae
    {
        public int FormulaeId { get; set; }
        public int? PayTypeGroupId { get; set; }
        public long? PayTypeId { get; set; }
        public long? FormulaPayTypeId { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmDefPayType PayType { get; set; }
        public virtual HrmDefPayTypeGroup PayTypeGroup { get; set; }
    }
}
