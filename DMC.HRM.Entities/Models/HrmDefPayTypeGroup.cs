﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefPayTypeGroup
    {
        public HrmDefPayTypeGroup()
        {
            HrmDefPayTypeFormulaes = new HashSet<HrmDefPayTypeFormulae>();
            HrmDefPayTypeSettings = new HashSet<HrmDefPayTypeSetting>();
        }

        public int PayTypeGroupId { get; set; }
        public string PayTypeGroupNameEn { get; set; }
        public string PayTypeGroupNameAr { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmDefPayTypeFormulae> HrmDefPayTypeFormulaes { get; set; }
        public virtual ICollection<HrmDefPayTypeSetting> HrmDefPayTypeSettings { get; set; }
    }
}
