﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefPayTypeSetting
    {
        public long PaytypeSettingId { get; set; }
        public int? PayTypeGroupId { get; set; }
        public long? PayTypeId { get; set; }
        public decimal? Multiplier { get; set; }
        public bool? IsSystem { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmDefPayType PayType { get; set; }
        public virtual HrmDefPayTypeGroup PayTypeGroup { get; set; }
    }
}
