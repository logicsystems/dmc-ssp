﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefPaymentMethod
    {
        public int PaymentMethodId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string AccountNumber { get; set; }
        public int? CompanyId { get; set; }
        public string Provider { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        public int? PaymentTypeId { get; set; }
        public bool? IsDefaultConfig { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
