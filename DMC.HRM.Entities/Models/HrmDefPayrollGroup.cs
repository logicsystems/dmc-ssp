﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefPayrollGroup
    {
        public long PayrollGroupId { get; set; }
        public string PayrollGroupNameEn { get; set; }
        public string PayrollGroupNameAr { get; set; }
        public bool? IsActive { get; set; }
        public string Remarks { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? CountryId { get; set; }
        public long? CompanyId { get; set; }
        public long? SiteId { get; set; }
        public long? ProjectId { get; set; }
        public long? BusinessUnitId { get; set; }
        public long? DivisionId { get; set; }
        public long? BranchId { get; set; }
        public long? DepartmentId { get; set; }
        public bool? IsForAllEmployee { get; set; }
        public bool? IsForFutureEmployee { get; set; }
        public DateTime? StartPayRollDate { get; set; }
        public DateTime? EndPayRollDate { get; set; }
        public DateTime? CurrentPayRollMonth { get; set; }
        public DateTime? CurrentPayRollYear { get; set; }
    }
}
