﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefPayrollPeriod
    {
        public long PeriodId { get; set; }
        public DateTime? PeriodMonth { get; set; }
        public bool? IsOpen { get; set; }
        public bool? IsClose { get; set; }
        public bool? IsActive { get; set; }
        public string Remarks { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
