﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefProduct
    {
        public int ProductId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public int? CompanyId { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public int? ProductTypeId { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? CostPrice { get; set; }
        public int? UnitTypeId { get; set; }
        public string Barcode { get; set; }
        public bool? IsDefaultConfig { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
