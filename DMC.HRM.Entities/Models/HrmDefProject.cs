﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefProject
    {
        public long ProjectId { get; set; }
        public string ProjectNameEn { get; set; }
        public string ProjectNameAr { get; set; }
        public long? ProjectSiteId { get; set; }
        public string ProjectDescription { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
