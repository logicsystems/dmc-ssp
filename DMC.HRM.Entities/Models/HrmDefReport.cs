﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefReport
    {
        public HrmDefReport()
        {
            HrmDefReportFilters = new HashSet<HrmDefReportFilter>();
            HrmDefReportMappingByRoles = new HashSet<HrmDefReportMappingByRole>();
            HrmDefReportOutputParams = new HashSet<HrmDefReportOutputParam>();
        }

        public long ReportId { get; set; }
        public string ReportNameEn { get; set; }
        public string ReportNameAr { get; set; }
        public string Description { get; set; }
        public int? DataModelType { get; set; }
        public string DataModel { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public long? ModifiedDate { get; set; }
        public bool? AcceptCommonFilters { get; set; }

        public virtual ICollection<HrmDefReportFilter> HrmDefReportFilters { get; set; }
        public virtual ICollection<HrmDefReportMappingByRole> HrmDefReportMappingByRoles { get; set; }
        public virtual ICollection<HrmDefReportOutputParam> HrmDefReportOutputParams { get; set; }
    }
}
