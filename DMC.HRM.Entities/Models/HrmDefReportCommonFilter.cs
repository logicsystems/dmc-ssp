﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefReportCommonFilter
    {
        public long CommonFilterId { get; set; }
        public string DisplayLabelNameEn { get; set; }
        public int? ParameterControlType { get; set; }
        public string ParameterColumnName { get; set; }
        public int? ParameterColumnDataType { get; set; }
        public string ConditionQuery { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsCompulsory { get; set; }
        public long? RelationFilterId { get; set; }
        public int? ConditionQueryType { get; set; }
        public string DisplayLabelNameAr { get; set; }
    }
}
