﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefReportMappingByRole
    {
        public long ReportMappingByRoleId { get; set; }
        public long? ReportId { get; set; }
        public long? RoleId { get; set; }
        public bool? IsActive { get; set; }
        public long? UserId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmDefReport Report { get; set; }
    }
}
