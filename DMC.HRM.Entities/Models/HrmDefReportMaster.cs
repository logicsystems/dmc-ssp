﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefReportMaster
    {
        public long Id { get; set; }
        public int? ModuleId { get; set; }
        public string ReportName { get; set; }
        public string StoreProcName { get; set; }
        public long? RoleId { get; set; }
        public long? IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }

        public virtual HrmSysModule Module { get; set; }
    }
}
