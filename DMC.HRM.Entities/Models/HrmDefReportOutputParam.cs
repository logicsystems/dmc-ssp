﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefReportOutputParam
    {
        public long Id { get; set; }
        public long ReportId { get; set; }
        public string OutputParamName { get; set; }
        public bool IsActive { get; set; }

        public virtual HrmDefReport Report { get; set; }
    }
}
