﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefRole
    {
        public HrmDefRole()
        {
            HrmDefRoleMenuMappings = new HashSet<HrmDefRoleMenuMapping>();
            HrmDefUsers = new HashSet<HrmDefUser>();
        }

        public long RoleId { get; set; }
        public string RoleNameEn { get; set; }
        public string RoleNameAr { get; set; }
        public string RoleDescription { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? Type { get; set; }

        public virtual ICollection<HrmDefRoleMenuMapping> HrmDefRoleMenuMappings { get; set; }
        public virtual ICollection<HrmDefUser> HrmDefUsers { get; set; }
    }
}
