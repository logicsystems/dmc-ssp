﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefRoleMenuMapping
    {
        public long RoleMenuMappingId { get; set; }
        public long? RoleId { get; set; }
        public long? MenuId { get; set; }
        public int? ScreenPermission { get; set; }
        public string Remarks { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmDefMenu Menu { get; set; }
        public virtual HrmDefRole Role { get; set; }
    }
}
