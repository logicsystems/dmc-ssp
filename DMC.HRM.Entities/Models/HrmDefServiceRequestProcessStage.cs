﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefServiceRequestProcessStage
    {
        public long ServiceRequestProcessStageId { get; set; }
        public int ServiceRequestTypeId { get; set; }
        public int? RuleOrder { get; set; }
        public string ApplicableCompany { get; set; }
        public string ApplicableDepartment { get; set; }
        public string ApplicableBranch { get; set; }
        public string ApplicableEmployee { get; set; }
        public int? ApprovalAuthorityId { get; set; }
        public long? ApprovalEmployeeId { get; set; }
        public int? ExpectedDuration { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }
}
