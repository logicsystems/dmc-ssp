﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefServiceRequestType
    {
        public long ServiceRequestTypeId { get; set; }
        public string ServiceRequestTypeNameEn { get; set; }
        public string ServiceRequestTypeNameAr { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string CssClass { get; set; }
    }
}
