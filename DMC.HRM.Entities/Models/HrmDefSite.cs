﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefSite
    {
        public long SiteId { get; set; }
        public string SiteNameEn { get; set; }
        public string SiteNameAr { get; set; }
        public long? ProjectId { get; set; }
        public string SiteDescription { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
