﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefTax
    {
        public int TaxId { get; set; }
        public int? CompanyId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string Symbol { get; set; }
        public decimal? TaxTariffPercentage { get; set; }
        public bool? IsDefault { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
