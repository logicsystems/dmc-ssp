﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefTicketBalanceSetup
    {
        public long TicketBalanceSetupId { get; set; }
        public long? EmployeeId { get; set; }
        public long? DependantTypeId { get; set; }
        public long? EmployeeDependantId { get; set; }
        public int? NumberOfTickets { get; set; }
        public int? TicketStatus { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? IsActive { get; set; }
        public long? ModifiedBy { get; set; }
        public long? ModifiedDate { get; set; }
        public string Remarks { get; set; }

        public virtual HrmSysDependantType DependantType { get; set; }
        public virtual HrmTranEmployee Employee { get; set; }
        public virtual HrmTranEmployeeDependant EmployeeDependant { get; set; }
    }
}
