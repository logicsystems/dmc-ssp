﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefTicketFrequency
    {
        public long TicketFrequencyId { get; set; }
        public string TicketFrequencyNameEn { get; set; }
        public string TicketFrequencyNameAr { get; set; }
        public int? NumberOfTickets { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
