﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefTicketSetup
    {
        public long TicketId { get; set; }
        public string TicketNameEn { get; set; }
        public string TicketNameAr { get; set; }
        public string TicketDescription { get; set; }
        public long? TicketCostRate { get; set; }
        public long? FromCityId { get; set; }
        public long? ToCityId { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
