﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefTravelType
    {
        public HrmDefTravelType()
        {
            HrmTranEmployeeTicketServiceRequestDetails = new HashSet<HrmTranEmployeeTicketServiceRequestDetail>();
        }

        public long TravelTypeId { get; set; }
        public string TravelTypeNameEn { get; set; }
        public string TravelTypeNameAr { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmTranEmployeeTicketServiceRequestDetail> HrmTranEmployeeTicketServiceRequestDetails { get; set; }
    }
}
