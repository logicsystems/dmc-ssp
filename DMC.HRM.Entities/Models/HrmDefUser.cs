﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefUser
    {
        public long UserId { get; set; }
        public string ProfileName { get; set; }
        public string UserDescription { get; set; }
        public string ProfilePicture { get; set; }
        public int? UserType { get; set; }
        public long? RoleId { get; set; }
        public long? EmployeeId { get; set; }
        public int? CompanyId { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public DateTime? PasswordExpirationDate { get; set; }
        public int? IncorrectPasswordAttempts { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileNameAr { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
        public virtual HrmDefRole Role { get; set; }
    }
}
