﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefVacationType
    {
        public long VacationTypeId { get; set; }
        public string VacationTypeNameEn { get; set; }
        public string VacationTypeNameAr { get; set; }
        public int NoOfDays { get; set; }
        public int NoOfMonths { get; set; }
        public decimal PerMonthVacationBalanceValue { get; set; }
        public string VacationTypeDescription { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
