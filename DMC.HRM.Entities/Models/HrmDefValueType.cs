﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmDefValueType
    {
        public long ValueTypeId { get; set; }
        public string ValueTypeNameEn { get; set; }
        public string ValueTypeNameAr { get; set; }
        public string ValueTypeDescription { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
    }
}
