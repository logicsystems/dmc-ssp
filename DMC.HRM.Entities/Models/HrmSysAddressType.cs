﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysAddressType
    {
        public HrmSysAddressType()
        {
            HrmTranEmployeeAddresses = new HashSet<HrmTranEmployeeAddress>();
        }

        public long AddressTypeId { get; set; }
        public string AddressTypeNameEn { get; set; }
        public string AddressTypeNameAr { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmTranEmployeeAddress> HrmTranEmployeeAddresses { get; set; }
    }
}
