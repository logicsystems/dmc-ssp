﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysApprovalAuthority
    {
        public int ApprovalAuthorityId { get; set; }
        public string ApprovalAuthorityNameEn { get; set; }
        public string ApprovalAuthorityNameAr { get; set; }
        public string RelationColumn { get; set; }
    }
}
