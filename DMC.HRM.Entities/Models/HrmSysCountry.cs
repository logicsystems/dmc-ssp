﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysCountry
    {
        public HrmSysCountry()
        {
            HrmTranEmployeeAddresses = new HashSet<HrmTranEmployeeAddress>();
            HrmTranEmployeePhones = new HashSet<HrmTranEmployeePhone>();
        }

        public long CountryId { get; set; }
        public string CountryNameEn { get; set; }
        public string CountryNameAr { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmTranEmployeeAddress> HrmTranEmployeeAddresses { get; set; }
        public virtual ICollection<HrmTranEmployeePhone> HrmTranEmployeePhones { get; set; }
    }
}
