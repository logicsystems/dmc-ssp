﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysDependantType
    {
        public HrmSysDependantType()
        {
            HrmDefTicketBalanceSetups = new HashSet<HrmDefTicketBalanceSetup>();
        }

        public long DependantTypeId { get; set; }
        public string DependantTypeNameEn { get; set; }
        public string DependantTypeNameAr { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmDefTicketBalanceSetup> HrmDefTicketBalanceSetups { get; set; }
    }
}
