﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysEndOfServiceType
    {
        public long EndofServiceTypeId { get; set; }
        public string EndofServiceTypeNameEn { get; set; }
        public string EndofServiceTypeNameAr { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
