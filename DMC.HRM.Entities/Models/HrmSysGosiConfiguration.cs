﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysGosiConfiguration
    {
        public long GosiConfigurationId { get; set; }
        public string Formula { get; set; }
        public int? PercentageValue { get; set; }
        public int? GosiFlagId { get; set; }
        public int? CompanyPercentage { get; set; }
    }
}
