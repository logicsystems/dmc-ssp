﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysLeaveType
    {
        public HrmSysLeaveType()
        {
            HrmTranEmployeeVacationServiceRequestLeaveDetails = new HashSet<HrmTranEmployeeVacationServiceRequestLeaveDetail>();
        }

        public int LeaveTypesId { get; set; }
        public string LeaveTypeEn { get; set; }
        public string LeaveTypeAr { get; set; }
        public bool? IsReduceFromVacation { get; set; }
        public bool? IsSalaryPaid { get; set; }
        public bool? IsActive { get; set; }

        public virtual ICollection<HrmTranEmployeeVacationServiceRequestLeaveDetail> HrmTranEmployeeVacationServiceRequestLeaveDetails { get; set; }
    }
}
