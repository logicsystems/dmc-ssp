﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysModule
    {
        public HrmSysModule()
        {
            HrmDefReportMasters = new HashSet<HrmDefReportMaster>();
        }

        public int Id { get; set; }
        public string ModuleName { get; set; }
        public string Remarks { get; set; }
        public bool? IsActive { get; set; }

        public virtual ICollection<HrmDefReportMaster> HrmDefReportMasters { get; set; }
    }
}
