﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysPhoneType
    {
        public HrmSysPhoneType()
        {
            HrmTranEmployeePhones = new HashSet<HrmTranEmployeePhone>();
        }

        public long PhoneTypeId { get; set; }
        public string PhoneTypeNameEn { get; set; }
        public string PhoneTypeNameAr { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<HrmTranEmployeePhone> HrmTranEmployeePhones { get; set; }
    }
}
