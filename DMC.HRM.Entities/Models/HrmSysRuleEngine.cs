﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysRuleEngine
    {
        public long RuleEngineId { get; set; }
        public string GroupKey { get; set; }
        public string PropertyName { get; set; }
        public string Condition { get; set; }
        public string ConditionValue { get; set; }
        public string ErrorMessageEn { get; set; }
        public string ErrorMessageAr { get; set; }
        public bool? IsDependent { get; set; }
        public long ScreenId { get; set; }
        public string DependentPropertyName { get; set; }
        public long? DependentKeyId { get; set; }
        public bool? IsRegularExpression { get; set; }
        public string RegularExpression { get; set; }
        public bool? IsRequired { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public string Language { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmSysScreen Screen { get; set; }
    }
}
