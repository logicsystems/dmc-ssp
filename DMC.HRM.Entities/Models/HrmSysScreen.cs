﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysScreen
    {
        public HrmSysScreen()
        {
            HrmDefMenus = new HashSet<HrmDefMenu>();
            HrmSysRuleEngines = new HashSet<HrmSysRuleEngine>();
        }

        public long ScreenId { get; set; }
        public string ScreenNameEn { get; set; }
        public string ScreenNameAr { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public bool? IsActive { get; set; }
        public int? ScreenType { get; set; }
        public string AdditionalData { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual ICollection<HrmDefMenu> HrmDefMenus { get; set; }
        public virtual ICollection<HrmSysRuleEngine> HrmSysRuleEngines { get; set; }
    }
}
