﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmSysSystemConfiguration
    {
        public long SystemConfigurationId { get; set; }
        public string ModuleName { get; set; }
        public string ModuleNameLabel { get; set; }
        public string SectionName { get; set; }
        public string SectionNameLabel { get; set; }
        public string ParameterName { get; set; }
        public string ParameterLabel { get; set; }
        public string ParameterValue { get; set; }
        public bool? IsEditable { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsActive { get; set; }
        public long? ModuleId { get; set; }
        public long? SectionId { get; set; }
    }
}
