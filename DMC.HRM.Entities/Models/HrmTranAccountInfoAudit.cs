﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranAccountInfoAudit
    {
        public long AccountInfoAuditId { get; set; }
        public int? BankId { get; set; }
        public string BankName { get; set; }
        public int? AccountNo { get; set; }
        public string Ibanno { get; set; }
        public string AccountHolder { get; set; }
        public string BankBranch { get; set; }
        public string BankCode { get; set; }
        public bool? IsSalaryAccount { get; set; }
        public string Remarks { get; set; }
        public long? EmployeeId { get; set; }
        public DateTime? LogDate { get; set; }
        public string UserAction { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
