﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranContactInfoAudit
    {
        public long AuditContactInfoId { get; set; }
        public long? ContactInfoId { get; set; }
        public string PrimaryMobileNumber { get; set; }
        public string Email { get; set; }
        public long? EmployeeId { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? LogDateTime { get; set; }
        public string UserAction { get; set; }
    }
}
