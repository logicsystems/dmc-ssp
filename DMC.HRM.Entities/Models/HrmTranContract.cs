﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranContract
    {
        public long ContractInformationId { get; set; }
        public string DateOfJoiningAr { get; set; }
        public DateTime? DateOfJoiningEn { get; set; }
        public string DateOfHiringAr { get; set; }
        public DateTime? DateOfHiringEn { get; set; }
        public long? CurrentJobId { get; set; }
        public long? PositionId { get; set; }
        public long? LegalEntityId { get; set; }
        public long? SponsorId { get; set; }
        public long? BusinessUnitId { get; set; }
        public long? CompanyId { get; set; }
        public long? BranchId { get; set; }
        public long? DivisionId { get; set; }
        public long? DepartmentId { get; set; }
        public long? CostCenterId { get; set; }
        public long? GradeId { get; set; }
        public long? ProjectId { get; set; }
        public long? SiteId { get; set; }
        public long? LineManagerId { get; set; }
        public long? ContractTypeId { get; set; }
        public string ContractStartDateAr { get; set; }
        public DateTime? ContractStartDateEn { get; set; }
        public string ContractEndDateAr { get; set; }
        public DateTime? ContractEndDateEn { get; set; }
        public long? NoticePeriodId { get; set; }
        public string ProbationPeriodEn { get; set; }
        public long? VacationTypeId { get; set; }
        public long? TicketTypeId { get; set; }
        public long? NumberOfTickets { get; set; }
        public long? TicketFrequencyId { get; set; }
        public string TicketClass { get; set; }
        public decimal? MaxTicketRate { get; set; }
        public string TicketStartRoute { get; set; }
        public string TicketEndRoute { get; set; }
        public string TicketRemarks { get; set; }
        public string MaximumPerdiem { get; set; }
        public string Gosinumber { get; set; }
        public decimal? Gosipoll { get; set; }
        public decimal? GosipollEmployee { get; set; }
        public decimal? GosipollCompany { get; set; }
        public long? EmployeeId { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ProbationPeriodId { get; set; }
        public bool? VacationSalaryAdvance { get; set; }
        public bool? Allowances { get; set; }
        public bool? ExitReEntryVisa { get; set; }
        public bool? CompanyCarForBussinessTrip { get; set; }
        public int? GosiFlagId { get; set; }
        public bool? IsApplied { get; set; }
        public int? EmployeeStatusId { get; set; }
        public bool? EmployeePayrollStatus { get; set; }
        public string PayrollHoldComments { get; set; }
        public bool? IsEmployeeEligibleForTicket { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
    }
}
