﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranDocumentsInfoAudit
    {
        public long DocumentAuditId { get; set; }
        public long? DocumentId { get; set; }
        public long? DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public long? EmployeeId { get; set; }
        public string NameInDocument { get; set; }
        public string IssueLocation { get; set; }
        public long? DocumentOwnerId { get; set; }
        public string ProfessionInIqama { get; set; }
        public long? InsuranceTypeId { get; set; }
        public long? InsuranceProviderId { get; set; }
        public long? InsuranceClassId { get; set; }
        public string InsurancePolicyNo { get; set; }
        public DateTime? IssueDateEn { get; set; }
        public string IssueDateAr { get; set; }
        public DateTime? ExpiryDateEn { get; set; }
        public string ExpiryDateAr { get; set; }
        public string Costing { get; set; }
        public string DocumentStatus { get; set; }
        public string ScannedDocument { get; set; }
        public string Remarks { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? LogDate { get; set; }
        public string UserAction { get; set; }
    }
}
