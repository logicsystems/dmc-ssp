﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEducationInfoAudit
    {
        public long EducationInfoAuditId { get; set; }
        public long? EducationId { get; set; }
        public long? EmployeeId { get; set; }
        public long? CurrentDegreeId { get; set; }
        public DateTime? YearOfCertification { get; set; }
        public long? DegreeId { get; set; }
        public string Qualification { get; set; }
        public string YearOfCertificationAr { get; set; }
        public DateTime? YearOfCertificationEn { get; set; }
        public string CollegeOrUniversity { get; set; }
        public long? CountryId { get; set; }
        public long? CourseTypeId { get; set; }
        public DateTime? LogDate { get; set; }
        public string UserAction { get; set; }
        public string Remarks { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
