﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployee
    {
        public HrmTranEmployee()
        {
            HrmDefTicketBalanceSetups = new HashSet<HrmDefTicketBalanceSetup>();
            HrmDefUsers = new HashSet<HrmDefUser>();
            HrmTranAccountInfos = new HashSet<HrmTranAccountInfo>();
            HrmTranContactInfos = new HashSet<HrmTranContactInfo>();
            HrmTranContracts = new HashSet<HrmTranContract>();
            HrmTranDocumentsInfos = new HashSet<HrmTranDocumentsInfo>();
            HrmTranEducationInfos = new HashSet<HrmTranEducationInfo>();
            HrmTranEmployeeAssetMaps = new HashSet<HrmTranEmployeeAssetMap>();
            HrmTranEmployeeDependants = new HashSet<HrmTranEmployeeDependant>();
            HrmTranEmployeeMonthTimeSheets = new HashSet<HrmTranEmployeeMonthTimeSheet>();
            HrmTranEmployeePayrollPackages = new HashSet<HrmTranEmployeePayrollPackage>();
            HrmTranEmployeePhones = new HashSet<HrmTranEmployeePhone>();
            HrmTranEmployeeServiceRequests = new HashSet<HrmTranEmployeeServiceRequest>();
            HrmTranEmployeeTimeSheetCalculations = new HashSet<HrmTranEmployeeTimeSheetCalculation>();
            HrmTranEmployeeVacationBalanceUpdates = new HashSet<HrmTranEmployeeVacationBalanceUpdate>();
            HrmTranHroverViews = new HashSet<HrmTranHroverView>();
            HrmTranInsurances = new HashSet<HrmTranInsurance>();
            HrmTranMapToContactInfos = new HashSet<HrmTranMapToContactInfo>();
            HrmTranPayrollGroupMapDeletes = new HashSet<HrmTranPayrollGroupMapDelete>();
            HrmTranPersonalInfos = new HashSet<HrmTranPersonalInfo>();
        }

        public long EmployeeId { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeNumber { get; set; }

        public virtual ICollection<HrmDefTicketBalanceSetup> HrmDefTicketBalanceSetups { get; set; }
        public virtual ICollection<HrmDefUser> HrmDefUsers { get; set; }
        public virtual ICollection<HrmTranAccountInfo> HrmTranAccountInfos { get; set; }
        public virtual ICollection<HrmTranContactInfo> HrmTranContactInfos { get; set; }
        public virtual ICollection<HrmTranContract> HrmTranContracts { get; set; }
        public virtual ICollection<HrmTranDocumentsInfo> HrmTranDocumentsInfos { get; set; }
        public virtual ICollection<HrmTranEducationInfo> HrmTranEducationInfos { get; set; }
        public virtual ICollection<HrmTranEmployeeAssetMap> HrmTranEmployeeAssetMaps { get; set; }
        public virtual ICollection<HrmTranEmployeeDependant> HrmTranEmployeeDependants { get; set; }
        public virtual ICollection<HrmTranEmployeeMonthTimeSheet> HrmTranEmployeeMonthTimeSheets { get; set; }
        public virtual ICollection<HrmTranEmployeePayrollPackage> HrmTranEmployeePayrollPackages { get; set; }
        public virtual ICollection<HrmTranEmployeePhone> HrmTranEmployeePhones { get; set; }
        public virtual ICollection<HrmTranEmployeeServiceRequest> HrmTranEmployeeServiceRequests { get; set; }
        public virtual ICollection<HrmTranEmployeeTimeSheetCalculation> HrmTranEmployeeTimeSheetCalculations { get; set; }
        public virtual ICollection<HrmTranEmployeeVacationBalanceUpdate> HrmTranEmployeeVacationBalanceUpdates { get; set; }
        public virtual ICollection<HrmTranHroverView> HrmTranHroverViews { get; set; }
        public virtual ICollection<HrmTranInsurance> HrmTranInsurances { get; set; }
        public virtual ICollection<HrmTranMapToContactInfo> HrmTranMapToContactInfos { get; set; }
        public virtual ICollection<HrmTranPayrollGroupMapDelete> HrmTranPayrollGroupMapDeletes { get; set; }
        public virtual ICollection<HrmTranPersonalInfo> HrmTranPersonalInfos { get; set; }
    }
}
