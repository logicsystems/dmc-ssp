﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeAddressAudit
    {
        public long EmployeeAddressAuditId { get; set; }
        public long? EmployeeAddressId { get; set; }
        public long? AddressTypeId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public long? CountryId { get; set; }
        public string Pobox { get; set; }
        public long? EmployeeId { get; set; }
        public DateTime? LogDate { get; set; }
        public string UserAction { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
