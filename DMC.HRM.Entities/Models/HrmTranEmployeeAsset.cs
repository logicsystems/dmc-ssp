﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeAsset
    {
        public HrmTranEmployeeAsset()
        {
            HrmTranEmployeeAssetMaps = new HashSet<HrmTranEmployeeAssetMap>();
        }

        public long EmployeeAssetId { get; set; }
        public long? AssetId { get; set; }
        public DateTime? DateOfIssueEn { get; set; }
        public string DateOfIssueAr { get; set; }
        public string GeneralRemarks { get; set; }
        public string SharedRemarks { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmDefAsset Asset { get; set; }
        public virtual ICollection<HrmTranEmployeeAssetMap> HrmTranEmployeeAssetMaps { get; set; }
    }
}
