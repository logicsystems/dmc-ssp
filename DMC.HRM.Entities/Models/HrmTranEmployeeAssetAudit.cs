﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeAssetAudit
    {
        public long EmployeeAssetAuditId { get; set; }
        public long? EmployeeAssetId { get; set; }
        public long? AssetId { get; set; }
        public DateTime? DateOfIssueEn { get; set; }
        public string DateOfIssueAr { get; set; }
        public string GeneralRemarks { get; set; }
        public string SharedRemarks { get; set; }
        public DateTime? LogDate { get; set; }
        public string UserAction { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
