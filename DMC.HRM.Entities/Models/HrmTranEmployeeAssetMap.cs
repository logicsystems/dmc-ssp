﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeAssetMap
    {
        public long EmployeeAssetMapId { get; set; }
        public long? EmployeeAssetId { get; set; }
        public long? EmployeeId { get; set; }
        public bool? IsShared { get; set; }
        public bool? IsReturned { get; set; }
        public long? IssuedBy { get; set; }
        public long? RecievedBy { get; set; }
        public string ReasonOfReturn { get; set; }
        public DateTime? DateOfReturnEn { get; set; }
        public string DateOfReturnAr { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
        public virtual HrmTranEmployeeAsset EmployeeAsset { get; set; }
    }
}
