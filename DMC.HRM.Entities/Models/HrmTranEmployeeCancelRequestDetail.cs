﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeCancelRequestDetail
    {
        public long? EmployeeServiceRequestId { get; set; }
        public long? CancelServiceRequestId { get; set; }
        public int? CancelServiceRequestTypeId { get; set; }
        public long? ServiceRequestProcessStageId { get; set; }
        public string CancelServiceRequestReferenceNo { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmTranEmployeeServiceRequest EmployeeServiceRequest { get; set; }
    }
}
