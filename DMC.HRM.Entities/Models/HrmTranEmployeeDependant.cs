﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeDependant
    {
        public HrmTranEmployeeDependant()
        {
            HrmDefTicketBalanceSetups = new HashSet<HrmDefTicketBalanceSetup>();
        }

        public long EmployeeDependantId { get; set; }
        public long? DependantTypeId { get; set; }
        public string FirstNameEn { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameEn { get; set; }
        public string LastNameAr { get; set; }
        public long? GenderId { get; set; }
        public DateTime? DateOfBirthEn { get; set; }
        public string DateOfBirthAr { get; set; }
        public long? PhoneNumber { get; set; }
        public bool? EmergencyContact { get; set; }
        public long? EmployeeAddressId { get; set; }
        public long? AddressTypeId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string IqamaNumber { get; set; }
        public string NameInIqamaEn { get; set; }
        public string NameInIqamaAr { get; set; }
        public DateTime? IqamaExpiryDateEn { get; set; }
        public string IqamaExpiryDateAr { get; set; }
        public DateTime? PassportExpiryDateEn { get; set; }
        public string PassportExpiryDateAr { get; set; }
        public bool? EligibleForTicket { get; set; }
        public bool? EligibleForExitReentry { get; set; }
        public bool? EligibleForInsurance { get; set; }
        public bool? EligibleForSchooling { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? EmployeeId { get; set; }
        public bool? UseEmployeeAddress { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
        public virtual ICollection<HrmDefTicketBalanceSetup> HrmDefTicketBalanceSetups { get; set; }
    }
}
