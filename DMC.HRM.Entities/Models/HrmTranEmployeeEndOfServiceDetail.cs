﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeEndOfServiceDetail
    {
        public long EmployeeEndOfServiceDetailsId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public long? EndofServiceTypeId { get; set; }
        public long? OutofOfficeReasonId { get; set; }
        public long? ServiceRequestProcessStageId { get; set; }
        public DateTime? LastDateOfDuty { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
