﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeLoanRequestDetail
    {
        public long EmployeeLoanRequestDetailsId { get; set; }
        public long? ServiceRequestProcessStageId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public decimal? LoanAmount { get; set; }
        public DateTime? ExpectedDateOfLoan { get; set; }
        public DateTime? RepaymentStartFrom { get; set; }
        public DateTime? RepaymentEndOfDate { get; set; }
        public decimal? MonthlyRepaymentAmount { get; set; }
        public int? NoOfRepayments { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
