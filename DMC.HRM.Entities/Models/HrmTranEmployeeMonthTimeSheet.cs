﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeMonthTimeSheet
    {
        public HrmTranEmployeeMonthTimeSheet()
        {
            HrmTranEmployeeTimeSheetCalculations = new HashSet<HrmTranEmployeeTimeSheetCalculation>();
        }

        public string EmployeeNumber { get; set; }
        public long EmployeeMonthTimeSheetId { get; set; }
        public long? PayrollGroupId { get; set; }
        public int? PayrollMonth { get; set; }
        public int? PayrollStartDate { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Day1 { get; set; }
        public string Day2 { get; set; }
        public string Day3 { get; set; }
        public string Day4 { get; set; }
        public string Day5 { get; set; }
        public string Day6 { get; set; }
        public string Day7 { get; set; }
        public string Day8 { get; set; }
        public string Day9 { get; set; }
        public string Day10 { get; set; }
        public string Day11 { get; set; }
        public string Day12 { get; set; }
        public string Day13 { get; set; }
        public string Day14 { get; set; }
        public string Day15 { get; set; }
        public string Day16 { get; set; }
        public string Day17 { get; set; }
        public string Day18 { get; set; }
        public string Day19 { get; set; }
        public string Day20 { get; set; }
        public string Day21 { get; set; }
        public string Day22 { get; set; }
        public string Day23 { get; set; }
        public string Day24 { get; set; }
        public string Day25 { get; set; }
        public string Day26 { get; set; }
        public string Day27 { get; set; }
        public string Day28 { get; set; }
        public string Day29 { get; set; }
        public string Day30 { get; set; }
        public string Day31 { get; set; }
        public string Notes { get; set; }
        public int? Bonous { get; set; }
        public string Doubleduty { get; set; }
        public short? AbsAbsent { get; set; }
        public short? NhHired { get; set; }
        public short? RResigned { get; set; }
        public short? ExeExcuse { get; set; }
        public short? UvUnpaidVacation { get; set; }
        public short? Ex2Excuse { get; set; }
        public short? VacVacation { get; set; }
        public short? SicSickLeave { get; set; }
        public short? OtOverTime { get; set; }
        public short? WiWithdraw { get; set; }
        public short? UtTransfer { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
        public virtual ICollection<HrmTranEmployeeTimeSheetCalculation> HrmTranEmployeeTimeSheetCalculations { get; set; }
    }
}
