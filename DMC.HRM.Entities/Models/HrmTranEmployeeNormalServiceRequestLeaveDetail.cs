﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeNormalServiceRequestLeaveDetail
    {
        public long EmployeeNormalServiceRequestLeaveDetailsId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public int? LeaveTypesId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? NoOfDays { get; set; }
        public DateTime? ReturnOn { get; set; }
        public int? ServiceRequestProcessStageId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
