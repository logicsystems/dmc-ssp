﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeOutofOfficeReasonDetail
    {
        public long EmployeeOutofOfficeReasonId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public long? OutofOfficeReasonId { get; set; }
        public long? ServiceRequestProcessStageId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmDefOutofOfficeReason OutofOfficeReason { get; set; }
    }
}
