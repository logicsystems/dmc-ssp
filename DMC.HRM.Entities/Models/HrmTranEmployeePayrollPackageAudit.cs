﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeePayrollPackageAudit
    {
        public long PayrollPackageAuditId { get; set; }
        public long? PayrollPackageId { get; set; }
        public long? PackageId { get; set; }
        public long? PayTypeId { get; set; }
        public long? ValueTypeId { get; set; }
        public long? FrequencyId { get; set; }
        public long? EmployeeId { get; set; }
        public string Formulae { get; set; }
        public decimal? FormulaeValue { get; set; }
        public string ShortName { get; set; }
        public decimal? Amount { get; set; }
        public string Remarks { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? LogDate { get; set; }
        public string UserAction { get; set; }
    }
}
