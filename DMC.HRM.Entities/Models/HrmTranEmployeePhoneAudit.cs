﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeePhoneAudit
    {
        public long EmployeePhoneAuditId { get; set; }
        public long? PhoneId { get; set; }
        public long? PhoneTypeId { get; set; }
        public long? CountryId { get; set; }
        public string AreaCode { get; set; }
        public long? PhoneNumber { get; set; }
        public long? EmployeeId { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? LogDate { get; set; }
        public string UserAction { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
