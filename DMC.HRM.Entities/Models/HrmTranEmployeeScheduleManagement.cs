﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeScheduleManagement
    {
        public long EmployeeSmid { get; set; }
        public long? EmployeeId { get; set; }
        public long? EmployeeDaysId { get; set; }
        public string Day { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public TimeSpan? FromTime { get; set; }
        public TimeSpan? ToTime { get; set; }
        public bool? IsHoliday { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsSpecial { get; set; }
    }
}
