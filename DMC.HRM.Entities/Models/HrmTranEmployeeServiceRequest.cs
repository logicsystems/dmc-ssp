﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeServiceRequest
    {
        public HrmTranEmployeeServiceRequest()
        {
            HrmTranServiceRequestDocumentDetails = new HashSet<HrmTranServiceRequestDocumentDetail>();
        }

        public long EmployeeServiceRequestId { get; set; }
        public string ServiceRequestRefNo { get; set; }
        public int? ServiceRequestTypeId { get; set; }
        public long? EmployeeId { get; set; }
        public int? EscalationLevel { get; set; }
        public DateTime? NextEscalatonTime { get; set; }
        public int? ServiceRequestProcessStageId { get; set; }
        public int? ReferenceId { get; set; }
        public DateTime? ReferenceDate { get; set; }
        public string ReferenceEmailId { get; set; }
        public string ReferenceMobile { get; set; }
        public DateTime? CreationDate { get; set; }
        public long? CreationBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsApproved { get; set; }
        public string Remarks { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
        public virtual ICollection<HrmTranServiceRequestDocumentDetail> HrmTranServiceRequestDocumentDetails { get; set; }
    }
}
