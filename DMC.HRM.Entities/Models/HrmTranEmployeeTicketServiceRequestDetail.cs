﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeTicketServiceRequestDetail
    {
        public long EmployeeTicketServiceRequestLeaveDetailsId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public long? FlightClassId { get; set; }
        public long? ServiceRequestProcessStageId { get; set; }
        public long? TravelTypeId { get; set; }
        public long? DependantTypeId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? ExpectedDepartureDate { get; set; }
        public string SpecialComments { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmDefFlightClass FlightClass { get; set; }
        public virtual HrmDefTravelType TravelType { get; set; }
    }
}
