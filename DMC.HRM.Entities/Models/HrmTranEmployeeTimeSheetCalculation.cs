﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeTimeSheetCalculation
    {
        public long EtcalculationId { get; set; }
        public short? AbsAbsent { get; set; }
        public short? NhHired { get; set; }
        public short? R { get; set; }
        public short? Exe { get; set; }
        public short? Uv { get; set; }
        public short? Ex2 { get; set; }
        public short? Vac { get; set; }
        public short? Sic { get; set; }
        public short? Ot { get; set; }
        public short? Wi { get; set; }
        public short? Ut { get; set; }
        public long? EmployeeId { get; set; }
        public long? EmployeeMonthTimeSheetId { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
        public virtual HrmTranEmployeeMonthTimeSheet EmployeeMonthTimeSheet { get; set; }
    }
}
