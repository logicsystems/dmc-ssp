﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeTravelRequest
    {
        public long? EmployeeServiceRequestId { get; set; }
        public long? PurpouseId { get; set; }
        public bool? IsDomestic { get; set; }
        public bool? IsInternational { get; set; }
        public long? ServiceRequestProcessStageId { get; set; }
        public DateTime? CreationDate { get; set; }
        public long? CreationBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
