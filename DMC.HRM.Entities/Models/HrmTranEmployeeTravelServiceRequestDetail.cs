﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeTravelServiceRequestDetail
    {
        public long EmployeeTravelRequestDetailsId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public long? ServiceRequestProcessStageId { get; set; }
        public long? FlightClassId { get; set; }
        public long? DependantTypeId { get; set; }
        public long? FromCityId { get; set; }
        public long? ToCityId { get; set; }
        public string DepartureDateTime { get; set; }
        public string Remarks { get; set; }
        public DateTime? CreationDate { get; set; }
        public long? CreationBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
