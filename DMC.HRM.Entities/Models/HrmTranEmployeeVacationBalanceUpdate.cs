﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeVacationBalanceUpdate
    {
        public long VacationBalanceId { get; set; }
        public long? EmployeeId { get; set; }
        public decimal? Days { get; set; }
        public byte? EntryType { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public long? PayrollId { get; set; }
        public long? EntryUserId { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
    }
}
