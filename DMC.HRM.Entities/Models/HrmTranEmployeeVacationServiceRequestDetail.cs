﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeVacationServiceRequestDetail
    {
        public long? EmployeeServiceRequestId { get; set; }
        public bool? SalaryInAdvance { get; set; }
        public bool? ExistRentryVisa { get; set; }
        public bool? TicketsForDependants { get; set; }
        public bool? CompanyCar { get; set; }
        public bool? Allowances { get; set; }
        public string TrafficViolation { get; set; }
        public DateTime? CreationDate { get; set; }
        public long? CreationBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? EscalationLevel { get; set; }
        public DateTime? NextEscalatonTime { get; set; }
        public long? ServiceRequestProcessStageId { get; set; }
        public DateTime? ReturnDate { get; set; }
        public long? ReplacementEmployeeId { get; set; }

        public virtual HrmTranEmployeeServiceRequest EmployeeServiceRequest { get; set; }
    }
}
