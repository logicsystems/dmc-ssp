﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeVacationServiceRequestLeaveDetail
    {
        public long EmployeeVacationServiceRequestLeaveDetailsId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public int? LeaveTypesId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? NoOfDays { get; set; }
        public DateTime? ReturnDate { get; set; }

        public virtual HrmSysLeaveType LeaveTypes { get; set; }
    }
}
