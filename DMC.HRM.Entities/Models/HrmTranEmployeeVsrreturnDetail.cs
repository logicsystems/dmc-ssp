﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranEmployeeVsrreturnDetail
    {
        public long EmployeeServiceRequestId { get; set; }
        public DateTime? ReturnOn { get; set; }
        public int? ServiceRequestProcessStageId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmTranEmployeeServiceRequest EmployeeServiceRequest { get; set; }
    }
}
