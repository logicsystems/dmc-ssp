﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranExitReEntyServiceRequestDetail
    {
        public long ExitReEntryId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }
        public DateTime? StartDateOfTravel { get; set; }
        public DateTime? ReturnDateOfTravel { get; set; }
        public DateTime RequiredExitReEntryDuration { get; set; }
        public int? ServiceRequestProcessStageId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
