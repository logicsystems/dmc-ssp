﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranInsurance
    {
        public long InsuranceId { get; set; }
        public long? InsuranceTypeId { get; set; }
        public long? InsuranceProviderId { get; set; }
        public string PolicyHolderName { get; set; }
        public long? InsuranceClassId { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? IssueDateEn { get; set; }
        public string IssueDateAr { get; set; }
        public DateTime? ExpiryDateEn { get; set; }
        public string ExpiryDateAr { get; set; }
        public decimal? YearlyCost { get; set; }
        public long? EmployeeId { get; set; }
        public bool? IsActive { get; set; }
        public string Remarks { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
        public virtual HrmDefInsuranceClass InsuranceClass { get; set; }
        public virtual HrmDefInsuranceProvider InsuranceProvider { get; set; }
        public virtual HrmDefInsuranceType InsuranceType { get; set; }
    }
}
