﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranInvoice
    {
        public long InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime? InvoiceDueDate { get; set; }
        public int? CurrencyId { get; set; }
        public int? CompanyId { get; set; }
        public string Ponumber { get; set; }
        public string PaymentTerms { get; set; }
        public long? CustomerId { get; set; }
        public int? BankId { get; set; }
        public int? InvoiceStatusId { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? AmountBeforeTax { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TotalPayment { get; set; }
        public decimal? AmountDue { get; set; }
        public bool? IsDefaultConfig { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public decimal? TaxTariffPercentage { get; set; }
    }
}
