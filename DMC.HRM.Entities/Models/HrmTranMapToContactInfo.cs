﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranMapToContactInfo
    {
        public long MapContactInfoId { get; set; }
        public string ContactPersonName { get; set; }
        public long? ConatactNumber { get; set; }
        public bool? IsFamily { get; set; }
        public bool? IsEmergency { get; set; }
        public string ContactAddress { get; set; }
        public long? EmployeeId { get; set; }
        public string Remarks { get; set; }
        public long? ContactInfoId { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
    }
}
