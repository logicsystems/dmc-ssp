﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranPayroll
    {
        public long PayrollId { get; set; }
        public DateTime? PayrollMonth { get; set; }
    }
}
