﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranPayrollEntry
    {
        public long PayrollEntryId { get; set; }
        public long? PreparationId { get; set; }
        public long? EmployeeId { get; set; }
        public DateTime? PayrollMonth { get; set; }
        public decimal? Amount { get; set; }
        public bool? IsAdd { get; set; }
        public bool? IsDeduct { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsPosted { get; set; }
        public long? PostedBy { get; set; }
        public DateTime? PostedDate { get; set; }
    }
}
