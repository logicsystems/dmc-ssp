﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranPayrollGroupMapDelete
    {
        public long PayrollGroupMapId { get; set; }
        public long? Id { get; set; }
        public long? PayrollGroupId { get; set; }
        public long? EmployeeId { get; set; }
        public long? BranchId { get; set; }
        public long? DepartmentId { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmTranEmployee Employee { get; set; }
    }
}
