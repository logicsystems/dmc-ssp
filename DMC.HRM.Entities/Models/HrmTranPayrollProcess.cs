﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranPayrollProcess
    {
        public long PayrollProcessId { get; set; }
        public long? EmployeeId { get; set; }
        public decimal? TotalEarnings { get; set; }
        public decimal? TotalDeductions { get; set; }
        public decimal? NetPay { get; set; }
        public long? PayrollPeriodId { get; set; }
        public long? PayrollGroupId { get; set; }
        public long? CountryId { get; set; }
        public long? CompanyId { get; set; }
        public long? SiteId { get; set; }
        public long? ProjectId { get; set; }
        public long? BusinessUnitId { get; set; }
        public long? DivisionId { get; set; }
        public long? BranchId { get; set; }
        public long? DepartmentId { get; set; }
        public bool? IsGroupProcess { get; set; }
        public bool? IsProcessed { get; set; }
        public bool? IsPosted { get; set; }
        public long? ProcessBy { get; set; }
        public DateTime? ProcessDate { get; set; }
        public long? PostedBy { get; set; }
        public DateTime? PostedDate { get; set; }
    }
}
