﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranPayrollProcessDetail
    {
        public long PayrollProcessDetailsId { get; set; }
        public long? PayrollProcessId { get; set; }
        public bool? IsEarning { get; set; }
        public bool? IsDeduction { get; set; }
        public bool? IsAdjustment { get; set; }
        public long? PayTypeId { get; set; }
        public decimal? Amount { get; set; }
        public long? EmployeeId { get; set; }
        public long? PayrollGroupId { get; set; }
    }
}
