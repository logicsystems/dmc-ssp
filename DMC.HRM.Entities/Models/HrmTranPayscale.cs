﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranPayscale
    {
        public long PayscaleId { get; set; }
        public long? PackageId { get; set; }
        public int? ValueTypeId { get; set; }
        public int? PayTypeId { get; set; }
        public int? FrequencyId { get; set; }
        public string Formulae { get; set; }
        public decimal? FormulaeValue { get; set; }
        public decimal? PayValue { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual HrmDefPackage Package { get; set; }
    }
}
