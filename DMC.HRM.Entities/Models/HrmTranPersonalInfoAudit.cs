﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranPersonalInfoAudit
    {
        public long AuditPersonalInfoId { get; set; }
        public long? PersonalInfoId { get; set; }
        public long? EmployeeId { get; set; }
        public long? GenderId { get; set; }
        public long? TitleId { get; set; }
        public string FirstNameEn { get; set; }
        public string FirstNameAr { get; set; }
        public string MiddleNameEn { get; set; }
        public string MiddleNameAr { get; set; }
        public string LastNameEn { get; set; }
        public string LastNameAr { get; set; }
        public string FullNameEn { get; set; }
        public string FullNameAr { get; set; }
        public long? PersonTypeId { get; set; }
        public long? MaritalStatusId { get; set; }
        public DateTime? DateOfBirthEn { get; set; }
        public string DateOfBirthAr { get; set; }
        public long? NationalityId { get; set; }
        public long? ReligionId { get; set; }
        public long? BloodGroupId { get; set; }
        public string EmployeePicture { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? LogDateTime { get; set; }
        public string UserAction { get; set; }
    }
}
