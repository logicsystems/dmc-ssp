﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranPreparation
    {
        public long PreparationId { get; set; }
        public bool? IsAdd { get; set; }
        public bool? IsDeduct { get; set; }
        public long? PayTypeId { get; set; }
        public long? FrequencyId { get; set; }
        public int? PaymentMethodId { get; set; }
        public long? PayrollPeriodId { get; set; }
        public decimal? AdjustmentAmount { get; set; }
        public DateTime? ReferenceDate { get; set; }
        public long? EmployeeId { get; set; }
        public DateTime? UntilDate { get; set; }
        public decimal? DaysofBasic { get; set; }
        public decimal? PercentageOfBasic { get; set; }
        public decimal? HoursofBasic { get; set; }
        public DateTime? StartDate { get; set; }
        public bool? IsAdvance { get; set; }
        public int? NoOfMonths { get; set; }
        public string Remarks { get; set; }
        public int? Status { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
