﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class HrmTranServiceRequestDocumentDetail
    {
        public long ServiceRequestDocumentsId { get; set; }
        public long? DocumentTypeId { get; set; }
        public string DocumentName { get; set; }
        public string FilePath { get; set; }
        public DateTime? UploadedDate { get; set; }
        public long? UploadedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public long? ServiceRequestStageId { get; set; }
        public long? EmployeeServiceRequestId { get; set; }

        public virtual HrmDefDocumentType DocumentType { get; set; }
        public virtual HrmTranEmployeeServiceRequest EmployeeServiceRequest { get; set; }
    }
}
