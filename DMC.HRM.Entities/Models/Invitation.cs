﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class Invitation
    {
        public int Id { get; set; }
        public int RequestingSessionId { get; set; }
        public int ReceivingMemberId { get; set; }
        public DateTime CreationDateTime { get; set; }
        public byte StatusId { get; set; }
        public DateTime? ResponseDateTime { get; set; }
    }
}
