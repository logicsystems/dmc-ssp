﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class PracticeTable
    {
        public long EmployeeId { get; set; }
        public bool? IsEmergency { get; set; }
        public long? AssetTypeId { get; set; }
        public DateTime? DateOfBirthEn { get; set; }
        public string DateOfBirthAr { get; set; }
        public string EmployeeName { get; set; }
        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual HrmDefAssetType AssetType { get; set; }
    }
}
