﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class Table2
    {
        public int? Id { get; set; }
        public string Value { get; set; }
    }
}
