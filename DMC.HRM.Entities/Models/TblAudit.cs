﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class TblAudit
    {
        public int AuditId { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string Pkvalue { get; set; }
        public string CurrentValue { get; set; }
        public string NewValue { get; set; }
        public string LoginId { get; set; }
        public string UpdatedOn { get; set; }
    }
}
