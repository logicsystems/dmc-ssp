﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class TestForPractice
    {
        public long CountryId { get; set; }
        public string CountryNameEn { get; set; }
        public string DocumentOwnerNameAr { get; set; }
        public bool? IsFamily { get; set; }
        public DateTime? DateOfBirthEn { get; set; }
        public long? AssetTypeId { get; set; }
        public string DateOfBirthAr { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual HrmDefAssetType AssetType { get; set; }
    }
}
