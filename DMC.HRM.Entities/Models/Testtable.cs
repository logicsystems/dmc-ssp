﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DMC.HRM.Entities.Models
{
    public partial class Testtable
    {
        public int Id { get; set; }
        public string Col1 { get; set; }
        public string Col2 { get; set; }
    }
}
