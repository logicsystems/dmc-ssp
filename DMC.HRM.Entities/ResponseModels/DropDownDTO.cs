﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.Entities.ResponseModels
{
    [DataContract(Name = "DropDownDTO")]
    public class DropDownDTO
    {
        [DataMember(Name = "Value")]
        public string Value { get; set; }
        [DataMember(Name = "Text")]
        public string Text { get; set; }
    }
}
