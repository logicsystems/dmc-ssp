﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.Entities.ResponseModels
{
    [DataContract(Name = "EducationInfoModel")]
    public class EducationInfoModel
    {
        public EducationInfoModel()
        {
            AddressTypeDropDown = new List<DropDownDTO>();
            CountryDropDown = new List<DropDownDTO>();
            DegreeDropDown = new List<DropDownDTO>();
            CourseTypeDropDown = new List<DropDownDTO>();
        }
        [DataMember(Name = "SNo")]
        public int SNo { get; set; }
        [DataMember(Name = "EducationId")]
        public long EducationId { get; set; }
        [DataMember(Name = "EmployeeId")]
        public long? EmployeeId { get; set; }
        [DataMember(Name = "CurrentDegreeId")]
        public long? CurrentDegreeId { get; set; }
        [DataMember(Name = "CurrentDegree")]
        public string CurrentDegree { get; set; }
        [DataMember(Name = "DegreeId")]
        [Required(ErrorMessage = "*Required")]
        public long DegreeId { get; set; }
        [DataMember(Name = "DegreeName")]
        public string DegreeName { get; set; }
        [DataMember(Name = "Qualification")]
        [Required(ErrorMessage = "*Required")]
        public string Qualification { get; set; }
        [DataMember(Name = "YearOfCertification_AR")]
        public string YearOfCertification_AR { get; set; }
        [DataMember(Name = "YearOfCertification_EN")]
        [Required(ErrorMessage = "*Required")]
        public string YearOfCertification_EN { get; set; }
        [DataMember(Name = "CollegeOrUniversity")]
        [Required(ErrorMessage = "*Required")]
        public string CollegeOrUniversity { get; set; }
        [DataMember(Name = "CountryID")]
        [Required(ErrorMessage = "*Required")]
        public long CountryID { get; set; }
        [DataMember(Name = "CountryName")]
        public string CountryName { get; set; }
        [DataMember(Name = "CourseTypeID")]
        [Required(ErrorMessage = "*Required")]
        public long? CourseTypeID { get; set; }
        [DataMember(Name = "CourseTypeName")]
        public string CourseTypeName { get; set; }
        [DataMember(Name = "Remarks")]
        public string Remarks { get; set; }
        [DataMember(Name = "CourseTypeDropDown")]
        public List<DropDownDTO> CourseTypeDropDown { get; set; }
        [DataMember(Name = "AddressTypeDropDown")]
        public List<DropDownDTO> AddressTypeDropDown { get; set; }
        [DataMember(Name = "CountryDropDown")]
        public List<DropDownDTO> CountryDropDown { get; set; }
        [DataMember(Name = "DegreeDropDown")]
        public List<DropDownDTO> DegreeDropDown { get; set; } 
        [DataMember(Name = "YearOfCertification")]
        public string YearOfCertification { get; set; }

    }
}
