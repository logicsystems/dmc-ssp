﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.Entities.ResponseModels
{
    public class HrmTranContactInfoModel
    {
        public long ContactInfoId { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string PrimaryMobileNumber { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string Email { get; set; }
        public long? EmployeeId { get; set; }
    }
}
