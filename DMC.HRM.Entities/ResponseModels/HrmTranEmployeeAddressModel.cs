﻿using DMC.HRM.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace DMC.HRM.Entities.ResponseModels
{
    [DataContract(Name = "HrmTranEmployeeAddressModel")]
    public class HrmTranEmployeeAddressModel
    {
        
        public HrmTranEmployeeAddressModel()
        {
            Countries = new List<DropDownDTO>();
            AddressTypes = new List<DropDownDTO>();
        }
        [DataMember(Name = "SNo")]
        public int SNo { get; set; }
        [DataMember(Name = "EmployeeNumber")]
        public string EmployeeNumber { get; set; }
        [DataMember(Name = "EmployeeAddressId")]

        public long EmployeeAddressId { get; set; }
        [DataMember(Name = "AddressTypeId")]
        [Required(ErrorMessage = "*Required")]
        public long? AddressTypeId { get; set; }
        [DataMember(Name = "AddressLine1")]
        [Required(ErrorMessage = "*Required")]
        public string AddressLine1 { get; set; }
        [DataMember(Name = "AddressTypeName")]
        public string AddressTypeName{ get; set; }
        [DataMember(Name = "AddressLine2")]
        public string AddressLine2 { get; set; }
        [DataMember(Name = "City")]
        [Required(ErrorMessage = "*Required")]
        public string City { get; set; }
        [DataMember(Name = "CountryId")]
        [Required(ErrorMessage = "*Required")]
        public long? CountryId { get; set; }
        [DataMember(Name = "CountryName")]
        public string CountryName { get; set; }
        [DataMember(Name = "Pobox")]
        public string Pobox { get; set; }
        [DataMember(Name = "EmployeeId")]
        public long? EmployeeId { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [DataMember(Name = "Countries")]
        public List<DropDownDTO> Countries { get; set; }
        [DataMember(Name = "AddressTypes")]

        public List<DropDownDTO> AddressTypes { get; set; }
      
    }
}

