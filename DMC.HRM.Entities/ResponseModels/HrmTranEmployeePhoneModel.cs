﻿using DMC.HRM.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace DMC.HRM.Entities.ResponseModels
{
    [DataContract(Name = "HrmTranEmployeePhoneModel")]
    public class HrmTranEmployeePhoneModel
    {
        public HrmTranEmployeePhoneModel()
        {
            Countries = new List<DropDownDTO>();
            PhoneTypes = new List<DropDownDTO>();
        }
        [DataMember(Name = "SNo")]
        public int SNo { get; set; }
        [DataMember(Name = "PhoneId")]
        public long PhoneId { get; set; }
        [DataMember(Name = "PhoneTypeId")]
        [Required(ErrorMessage = "*Required")]
        public long? PhoneTypeId { get; set; }
        [DataMember(Name = "PhoneTypeName")]
        public string PhoneTypeName { get; set; }
        [DataMember(Name = "CountryId")]
        [Required(ErrorMessage = "*Required")]
        public long? CountryId { get; set; }
        [DataMember(Name = "CountryName")]
        public string CountryName { get; set; }
        [DataMember(Name = "AreaCode")]
        public string AreaCode { get; set; }
        [DataMember(Name = "PhoneNumber")]
        [Required(ErrorMessage = "*Required")]
        public long? PhoneNumber { get; set; }
        [DataMember(Name = "EmployeeId")]
        public long? EmployeeId { get; set; }
        [DataMember(Name = "Countries")]
        public List<DropDownDTO> Countries { get; set; }
        [DataMember(Name = "PhoneTypes")]
        public List<DropDownDTO> PhoneTypes { get; set; }
    }
}
