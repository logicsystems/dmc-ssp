﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.Entities.ResponseModels
{
    public class HrmTranMapToContactInfoModel
    {
        public int SNo { get; set; }
        public long MapContactInfoId { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string ContactPersonName { get; set; }
        public string EmployeeNumber { get; set; }
        [Required(ErrorMessage = "*Required")]
        public long? ConatactNumber { get; set; }
        public bool? IsFamily { get; set; }
        public bool? IsEmergency { get; set; }
        public string ContactAddress { get; set; }
        public long? EmployeeId { get; set; }
        public string Remarks { get; set; }
        public long? ContactInfoId { get; set; }
    }
}
