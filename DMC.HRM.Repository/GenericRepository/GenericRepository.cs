﻿using DMC.HRM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.Repository.Repository
{
    public class GenericRepository
    {
        private DCMHrmContext _context;
        public GenericRepository(DCMHrmContext context)
        {
            _context = context;
        }

        public DCMHrmContext Context
        {
            get { return _context; }
        }
    }
}
