﻿using DMC.HRM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.Repository.Repository
{
    public interface IBranchRepository
    {
        Task<IEnumerable<HrmDefBranch>> GetAllBranchesAsync();
    }
}
