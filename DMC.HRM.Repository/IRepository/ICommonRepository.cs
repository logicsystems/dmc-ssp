﻿using DMC.HRM.Entities.Models;
using DMC.HRM.Entities.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.Repository.Repository
{
    public interface ICommonRepository
    {
        Task<List<HrmSysGender>> GetAllGenders();
        Task<List<HrmSysNationality>> GetAllNationalities();
        Task<List<DropDownDTO>> GetAllCountries(int languageID);
        Task<List<DropDownDTO>> GetAllDegrees(int languageID);
        Task<List<DropDownDTO>> GetAllCourseTypes(int languageID);
        Task<List<DropDownDTO>> GetAllPhoneTypes(int languageID);

        Task<List<DropDownDTO>> GetAllAddressTypes(int languageID);
        Task<List<HrmDefPersonType>> GetAllPersonTypes();
        Task<List<HrmSysReligion>> GetAllReligions();
        Task<List<HrmSysTitle>> GetAllTitles();
        Task<List<HrmSysBloodGroup>> GetAllBloodGroups();
        Task<List<HrmSysMaritalStatus>> GetAllMaritalStatus();
        Task<List<HrmSysDependantType>> GetAllDependantTypeList();
        Task<List<HrmSysAddressType>> GetAllAddressTypes();
        Task<List<HrmDefDocumentType>> GetAllDocumentTypes();
        Task<List<HrmSysDocumentOwner>> GetAllDocumentOwners();
    }
}
