﻿using DMC.HRM.Entities;
using DMC.HRM.Entities.Models;
using DMC.HRM.Entities.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.Repository.Repository
{
    public interface IEmployeeRepository
    {
        Task<HrmTranPersonalInfo> GetPersonalInfo(HrmDefUser hrmDefUser);
        Task<HrmTranEmployee> GetEmployeeByID(long employeeID);
        Task<HrmTranEmployeePhoneModel> GetEmployeePhoneByID(long phoneID, int languageID);
        Task<EducationInfoModel> GetEmployeeEducationByID(long educationId, int languageID);
        Task<List<HrmTranEmployeePhoneModel>> GetEmployeePhoneList(long employeeID, int languageID);

        Task<HrmTranEmployeeAddressModel> GetEmployeeAddressByID(long employeeAddressID);
        Task<List<HrmTranEmployeeAddressModel>> GetEmployeeAddressList(long employeeID, string language);
        Task<int> UpdateEmployeeAddress(HrmTranEmployeeAddressModel hrmTranEmployeeAddressModel);
        //Task<HrmTranEmployeeAddress> GetEmployeeAddresses(long employeeID);
        Task<List<EducationInfoModel>> GetEmployeeEducationList(long employeeID, int languageID);
        Task<int> UpdatePersonalInfo(HrmTranPersonalInfo hrmTranPersonalInfo);

        #region EmployeeContactInfo
        Task<HrmTranContactInfo> GetEmployeeContactInfo(long employeeID);
        Task<List<HrmTranMapToContactInfo>> GetEmployeeMapToContactInfo(long employeeID);
        Task<int> UpdateContactInfo(HrmTranContactInfo hrmTranContactInfo);
        Task<int> SaveContactInfo(HrmTranContactInfo hrmTranContactInfo);
        Task<HrmTranMapToContactInfo> GetEmployeeMapToContactInfoByID(long employeeID, long MapContactInfoId);
        Task<int> SaveEmployeeMapToContactInfo(HrmTranMapToContactInfo hrmTranMapToContactInfo);
        Task<int> UpdateEmployeeMapToContactInfo(HrmTranMapToContactInfo hrmTranMapToContactInfo);

        #endregion

        #region EmployeeDependant
        Task<int> AddUpdateEmployeeDependant(HrmTranEmployeeDependantModel hrmTranEmployeeDependantModel);
        Task<List<HrmTranEmployeeDependantModel>> GetHrmTranEmployeeDependantData(long employeeID, string language);
        Task<HrmTranEmployeeDependantModel> GetHrmTranEmployeeDependantDataById(long Id);

        #endregion

        #region EmployeeDocuments
        Task<List<HrmTranDocumentsInfo>> GetEmployeeDocumentsInfo(long employeeID);
        Task<HrmTranDocumentsInfo> GetEmployeeDocumentsInfoByID(long employeeID, long documentID);
        Task<int> SaveEmployeeDocumentsInfo(HrmTranDocumentsInfo hrmTranDocumentsInfo);
        Task<int> UpdateEmployeeDocumentsInfo(HrmTranDocumentsInfo hrmTranDocumentsInfo);

        #endregion

        Task<int> UpdateEmployeePhoneInfo(HrmTranEmployeePhoneModel hrmTranEmployeePhoneModel);
        Task<int> UpdateEmployeeEducationInfo(EducationInfoModel educationInfoModel);
    }
}
