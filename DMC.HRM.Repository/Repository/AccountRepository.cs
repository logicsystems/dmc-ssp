﻿using DMC.HRM.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.Repository.Repository
{
    public class AccountRepository : GenericRepository, IAccountRepository
    {
        public AccountRepository(DCMHrmContext context) : base(context)
        {
        }

        public async Task<HrmDefUser> ValidateUser(string UserName, string UserPassword)
        {
            var user = await Context.HrmDefUsers.FromSqlRaw("SELECT  tblUser.UserID, tblUser.ProfileName, tblUser.ProfileName_AR, "
                + "tblUser.UserDescription, tblUser.ProfilePicture, tblUser.UserType, tblUser.RoleID, tblUser.EmployeeID, tblUser.UserName, "
                + "tblUser.UserPassword, tblUser.PasswordExpirationDate, tblUser.IncorrectPasswordAttempts, tblUser.IsActive, "
                + "tblUser.IsSystem, tblUser.CreatedBy, tblUser.CreatedDate, tblUser.ModifiedBy, tblUser.ModifiedDate, "
                + "tblRole.RoleName_EN as RoleName, tblUserCreatedBy.ProfileName as CreatedByName, tblUserModifiedBy.ProfileName as ModifiedByName, "
                + "tblUserCreatedBy.ProfilePicture as CreatedByImage,tblUserModifiedBy.ProfilePicture as ModifiedByImage, tblUser.CompanyId "
                + "FROM HRM_DEF_User tblUser LEFT JOIN  HRM_DEF_User tblUserCreatedBy  ON tblUser.CreatedBy = tblUserCreatedBy.UserId "
                + "LEFT JOIN HRM_DEF_User tblUserModifiedBy ON tblUser.ModifiedBy = tblUserModifiedBy.UserId "
                + "LEFT JOIN HRM_DEF_Role  tblRole ON tblRole.RoleID = tblUser.RoleID "
                + "WHERE tblUser.UserName = {0} AND tblUser.UserPassword = {1} AND tblUser.IsActive = 1", UserName, UserPassword)
                .FirstOrDefaultAsync();

            return user;
        }
    }
}
