﻿using DMC.HRM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DMC.HRM.Repository.Repository
{
    public class BranchRepository : GenericRepository, IBranchRepository
    {
        public BranchRepository(DCMHrmContext context) : base(context) { }
        public async Task<IEnumerable<HrmDefBranch>> GetAllBranchesAsync()
        {
            return await Context.HrmDefBranches.FromSqlRaw("SELECT * FROM dbo.HRM_DEF_Branch").ToListAsync();
        }
    }
}
