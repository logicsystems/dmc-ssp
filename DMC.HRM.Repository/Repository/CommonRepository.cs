﻿using DMC.HRM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DMC.HRM.Entities.ResponseModels;

namespace DMC.HRM.Repository.Repository
{
    public class CommonRepository:GenericRepository, ICommonRepository
    {
        public CommonRepository(DCMHrmContext context) : base(context)
        {
                
        }

        public async Task<List<HrmSysGender>> GetAllGenders()
        {
            var genders = await Context.HrmSysGenders.Where(a => a.IsActive == true).ToListAsync();
            return genders;
        }

        public async Task<List<HrmSysNationality>> GetAllNationalities()
        {
            var nationalities = await Context.HrmSysNationalities.Where(a => a.IsActive == true).ToListAsync();
            return nationalities;
        }
        public async Task<List<DropDownDTO>> GetAllCountries(int languageID = 1)
        {
            var countryList = await Context.HrmSysCountries.
                                Where(a => a.IsActive == true)
                                .Select(x => new DropDownDTO { Text = languageID == 1 ? x.CountryNameEn : x.CountryNameAr, Value = x.CountryId.ToString() })
                                .ToListAsync();
            return countryList;
        }
        public async Task<List<DropDownDTO>> GetAllDegrees(int languageID = 1)
        {
            var countryList = await Context.HrmSysEducationDegrees.
                                Where(a => a.IsActive == true)
                                .Select(x => new DropDownDTO { Text = languageID == 1 ? x.DegreeNameEn : x.DegreeNameAr, Value = x.DegreeId.ToString() })
                                .ToListAsync();
            return countryList;
        }
        public async Task<List<DropDownDTO>> GetAllCourseTypes(int languageID = 1)
        {
            var countryList = await Context.HrmSysCourseTypes.
                                Where(a => a.IsActive == true)
                                .Select(x => new DropDownDTO { Text = languageID == 1 ? x.CourseTypeNameEn : x.CourseTypeNameAr, Value = x.CourseTypeId.ToString() })
                                .ToListAsync();
            return countryList;
        }
        public async Task<List<DropDownDTO>> GetAllPhoneTypes(int languageID=1)
        {
            var phoneTypes = await Context.HrmSysPhoneTypes
                              .Where(a => a.IsActive == true)
                              .Select(x=>new DropDownDTO { Text= languageID ==1?x.PhoneTypeNameEn:x.PhoneTypeNameAr,Value=x.PhoneTypeId.ToString()})
                               .ToListAsync();
            return phoneTypes;
        }

        public async Task<List<DropDownDTO>> GetAllAddressTypes(int languageID = 1)
        {
            var addressTypes = await Context.HrmSysAddressTypes
                              .Where(a => a.IsActive == true)
                              .Select(x => new DropDownDTO { Text = languageID == 1 ? x.AddressTypeNameEn : x.AddressTypeNameAr, Value = x.AddressTypeId.ToString() })
                               .ToListAsync();
            return addressTypes;
        }

        public async Task<List<HrmDefPersonType>> GetAllPersonTypes()
        {
            var personTypes = await Context.HrmDefPersonTypes.Where(a => a.IsActive == true).ToListAsync();
            return personTypes;
        }

        public async Task<List<HrmSysReligion>> GetAllReligions()
        {
            var religions = await Context.HrmSysReligions.Where(a => a.IsActive == true).ToListAsync();
            return religions;
        }

        public async Task<List<HrmSysTitle>> GetAllTitles()
        {
            var titles = await Context.HrmSysTitles.Where(a => a.IsActive == true).ToListAsync();
            return titles;
        }

        public async Task<List<HrmSysBloodGroup>> GetAllBloodGroups()
        {
            var bloodGroups = await Context.HrmSysBloodGroups.Where(a => a.IsActive == true).ToListAsync();
            return bloodGroups;
        }
        public async Task<List<HrmSysMaritalStatus>> GetAllMaritalStatus()
        {
            var maritalStatus = await Context.HrmSysMaritalStatuses.Where(a => a.IsActive == true).ToListAsync();
            return maritalStatus;
        }

        public async Task<List<HrmSysDependantType>> GetAllDependantTypeList()
        {
            var dependants = await Context.HrmSysDependantTypes.Where(a => a.IsActive == true).ToListAsync();
            return dependants;
        }
        public async Task<List<HrmSysAddressType>> GetAllAddressTypes()
        {
            var adTypes = await Context.HrmSysAddressTypes.Where(a => a.IsActive == true).ToListAsync();
            return adTypes;
        }
        public async Task<List<HrmDefDocumentType>> GetAllDocumentTypes()
        {
            var documentTypes = await Context.HrmDefDocumentTypes.Where(a => a.IsActive == true).ToListAsync();
            return documentTypes;
        }
        public async Task<List<HrmSysDocumentOwner>> GetAllDocumentOwners()
        {
            var documentOwners = await Context.HrmSysDocumentOwners.Where(a => a.IsActive == true).ToListAsync();
            return documentOwners;
        }
    }
}
