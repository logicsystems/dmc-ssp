﻿using DMC.HRM.Entities;
using DMC.HRM.Entities.Models;
using DMC.HRM.Entities.ResponseModels;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.Repository.Repository
{
    public class EmployeeRepository : GenericRepository, IEmployeeRepository
    {
        public EmployeeRepository(DCMHrmContext context) : base(context)
        {

        }

        public async Task<HrmTranPersonalInfo> GetPersonalInfo(HrmDefUser hrmDefUser)
        {
            HrmTranPersonalInfo response = new HrmTranPersonalInfo();
            try
            {
                #region FromSqlRaw
                //    var parameters = new[] {
                //new SqlParameter("@Language", SqlDbType.VarChar) { Direction = ParameterDirection.Input, Value = "en-US" },
                //new SqlParameter("@UserID", SqlDbType.BigInt) { Direction = ParameterDirection.Input, Value = hrmDefUser.UserId },
                //new SqlParameter("@EmployeeID", SqlDbType.BigInt) { Direction = ParameterDirection.Input, Value = hrmDefUser.EmployeeId },
                //new SqlParameter("@RoleID", SqlDbType.BigInt) { Direction = ParameterDirection.Input, Value = hrmDefUser.RoleId },
                //new SqlParameter("@spResult", SqlDbType.Int) { Direction = ParameterDirection.InputOutput, Value = 0 },
                //new SqlParameter("@spResultMessage", SqlDbType.VarChar) { Direction = ParameterDirection.InputOutput, Value = 0 }
                //};
                //var result = await Context.HrmTranPersonalInfos.FromSqlRaw("EXEC [dbo].[HRM_uspGetPersonalInfoByID] @Language, @UserID, @EmployeeID, @RoleID, @spResult OUTPUT, @spResultMessage OUTPUT", parameters)
                //    .ToListAsync<HrmTranPersonalInfo>();
                #endregion

                var result = await Context.HrmTranPersonalInfos
                    .Where(a => a.EmployeeId == hrmDefUser.EmployeeId)
                    .ToListAsync();

                response = result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return response;
        }
        public async Task<HrmTranEmployee> GetEmployeeByID(long employeeID)
        {
            HrmTranEmployee response = new HrmTranEmployee();
            try
            {
                response = await Context.HrmTranEmployees
                    .Where(a => a.EmployeeId == employeeID)
                    .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return response;
        }
        public async Task<HrmTranEmployeePhoneModel> GetEmployeePhoneByID(long phoneID, int languageID = 1)
        {
            HrmTranEmployeePhoneModel response = new HrmTranEmployeePhoneModel();
            try
            {
                response = await Context.HrmTranEmployeePhones
                    .Where(a => a.PhoneId == phoneID)
                    .Select(x => new HrmTranEmployeePhoneModel()
                    {
                        CountryId = x.CountryId,
                        AreaCode = x.AreaCode,
                        CountryName = languageID == 1 ? x.Country.CountryNameEn : x.Country.CountryNameAr,
                        PhoneTypeName = languageID == 1 ? x.PhoneType.PhoneTypeNameEn : x.PhoneType.PhoneTypeNameAr,
                        PhoneNumber = x.PhoneNumber,
                        PhoneTypeId = x.PhoneTypeId
                    }).SingleOrDefaultAsync();
            }
            catch (Exception ex)
            {
                //string message = ex.Message;
            }
            return response;
        }
        public async Task<EducationInfoModel> GetEmployeeEducationByID(long educationId, int languageID)
        {
            EducationInfoModel response = new EducationInfoModel();
            try
            {
                response = await Context.HrmTranEducationInfos
                    .Where(a => a.EducationId == educationId)
                    .Select(x => new EducationInfoModel()
                    {
                        CountryID = Convert.ToInt64(x.CountryId),
                        Qualification = x.Qualification,
                        DegreeId = Convert.ToInt64(x.DegreeId),
                        CollegeOrUniversity = x.CollegeOrUniversity,
                        YearOfCertification_EN = x.YearOfCertificationEn.ToString(),
                        CourseTypeID=x.CourseTypeId,
                        Remarks=x.Remarks,
                        EmployeeId = x.EmployeeId,
                        EducationId=x.EducationId
                    }).SingleOrDefaultAsync();
            }
            catch (Exception ex)
            {
                //string message = ex.Message;
            }
            return response;
        }
        public async Task<List<HrmTranEmployeePhoneModel>> GetEmployeePhoneList(long employeeID, int languageID)
        {
            List<HrmTranEmployeePhoneModel> response = new List<HrmTranEmployeePhoneModel>();
            try
            {
                response = await Context.HrmTranEmployeePhones
                       .Where(a => a.EmployeeId == employeeID)
                       .Select(x => new HrmTranEmployeePhoneModel()
                       {
                           CountryId = x.CountryId,
                           AreaCode = x.AreaCode,
                           CountryName = languageID == 1 ? x.Country.CountryNameEn : x.Country.CountryNameAr,
                           PhoneTypeName = languageID == 1 ? x.PhoneType.PhoneTypeNameEn : x.PhoneType.PhoneTypeNameAr,
                           PhoneNumber = x.PhoneNumber,
                           PhoneTypeId = x.PhoneTypeId,
                           PhoneId = x.PhoneId,
                           EmployeeId=x.EmployeeId
                       }).ToListAsync();

            }
            catch (Exception ex)
            {
                //string message = ex.Message;
            }
            return response;
        }
        public async Task<List<EducationInfoModel>> GetEmployeeEducationList(long employeeID, int languageID)
        {
            List<EducationInfoModel> response = new List<EducationInfoModel>();
            try
            {
                response = await Context.HrmTranEducationInfos
                       .Where(a => a.EmployeeId == employeeID)
                       .Select(x => new EducationInfoModel()
                       {
                           EducationId=x.EducationId,
                           CountryID = Convert.ToInt64(x.CountryId),
                           YearOfCertification=x.YearOfCertificationEn.ToString(),
                           Qualification = x.Qualification,
                           CountryName = languageID == 1 ? Context.HrmSysCountries.SingleOrDefault(k => k.CountryId == x.CountryId).CountryNameEn : Context.HrmSysCountries.SingleOrDefault(n => n.CountryId == x.CountryId).CountryNameAr,
                           DegreeName = languageID == 1 ? Context.HrmSysEducationDegrees.SingleOrDefault(k=>k.DegreeId==x.DegreeId).DegreeNameEn:Context.HrmSysEducationDegrees.SingleOrDefault(n=>n.DegreeId==x.DegreeId).DegreeNameAr,
                           DegreeId = Convert.ToInt64(x.DegreeId),
                           CollegeOrUniversity = x.CollegeOrUniversity,
                           YearOfCertification_EN = x.YearOfCertificationEn.ToString(),
                           EmployeeId = x.EmployeeId,
                           CourseTypeID = x.CourseTypeId,
                           Remarks = x.Remarks,
                           CourseTypeName= languageID == 1 ? Context.HrmSysCourseTypes.SingleOrDefault(k => k.CourseTypeId == x.CourseTypeId).CourseTypeNameEn : Context.HrmSysCourseTypes.SingleOrDefault(n => n.CourseTypeId == x.CourseTypeId).CourseTypeNameAr
                       }).ToListAsync();

            }
            catch (Exception ex)
            {
                //string message = ex.Message;
            }
            return response;
        }

        
     # region For Address Code
        public async Task<HrmTranEmployeeAddressModel>GetEmployeeAddressByID(long employeeAddressID)
        {

            HrmTranEmployeeAddressModel response = new HrmTranEmployeeAddressModel();
            try
            {
                response = await Context.HrmTranEmployeeAddresses
                    .Where(a => a.EmployeeAddressId == employeeAddressID)
                    .Select(x => new HrmTranEmployeeAddressModel()
                    {
                        AddressTypeId=x.AddressTypeId,
                        CountryId = x.CountryId,
                        AddressTypeName=x.AddressType.AddressTypeNameEn,
                        CountryName=x.Country.CountryNameEn,
                        AddressLine1 = x.AddressLine1,
                        AddressLine2 = x.AddressLine2,
                        City = x.City,
                        Pobox = x.Pobox
                    }).SingleOrDefaultAsync();
            }
            catch (Exception ex)
            {
                //string message = ex.Message;
            }
            return response;
        }
        public async Task<List<HrmTranEmployeeAddressModel>> GetEmployeeAddressList(long employeeID, string language)
        {
            List<HrmTranEmployeeAddressModel> result = new List<HrmTranEmployeeAddressModel>();

            try
            {

                result = await Context.HrmTranEmployeeAddresses
                   .Where(a => a.EmployeeId == employeeID)
                   .Select(x => new HrmTranEmployeeAddressModel()
                   {

                       EmployeeAddressId = x.EmployeeAddressId,
                       AddressLine1 = x.AddressLine1,
                       AddressLine2 = x.AddressLine2,
                       AddressTypeName = (language == "en") ? x.AddressType.AddressTypeNameEn : x.AddressType.AddressTypeNameAr,
                       CountryName = (language == "en") ? x.Country.CountryNameEn : x.Country.CountryNameAr,
                       City = x.City,
                       Pobox = x.Pobox,
                       EmployeeId = x.EmployeeId
                   }).ToListAsync();
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return result;
        }
        public async Task<int> UpdateEmployeeAddress(HrmTranEmployeeAddressModel hrmTranEmployeeAddressModel)
        {
            int status = 0;
            try
            {
                HrmTranEmployeeAddress hrmTranEmployeeAddress = new HrmTranEmployeeAddress();
                if (hrmTranEmployeeAddressModel.EmployeeAddressId > 0)
                    hrmTranEmployeeAddress = await Context.HrmTranEmployeeAddresses.SingleOrDefaultAsync(x => x.EmployeeAddressId == hrmTranEmployeeAddressModel.EmployeeAddressId);

                hrmTranEmployeeAddress.AddressTypeId = hrmTranEmployeeAddressModel.AddressTypeId;
                hrmTranEmployeeAddress.CountryId = hrmTranEmployeeAddressModel.CountryId;
                hrmTranEmployeeAddress.AddressLine1 = hrmTranEmployeeAddressModel.AddressLine1;
                hrmTranEmployeeAddress.AddressLine2 = hrmTranEmployeeAddressModel.AddressLine2;
                hrmTranEmployeeAddress.City = hrmTranEmployeeAddressModel.City;
                hrmTranEmployeeAddress.Pobox = hrmTranEmployeeAddressModel.Pobox;
                hrmTranEmployeeAddress.EmployeeId = hrmTranEmployeeAddressModel.EmployeeId;
                hrmTranEmployeeAddress.CreatedBy = hrmTranEmployeeAddressModel.CreatedBy;
                hrmTranEmployeeAddress.CreatedDate = DateTime.Now;
                if (hrmTranEmployeeAddressModel.EmployeeAddressId == 0)
                    await Context.HrmTranEmployeeAddresses.AddAsync(hrmTranEmployeeAddress);
                Context.SaveChanges();
                status = 1;
            }
            catch (Exception ex)
            {

            }
            return status;
        }
        #endregion

        public async Task<int> UpdatePersonalInfo(HrmTranPersonalInfo hrmTranPersonalInfo)
        {
            int personalInfoStatus, employeeStatus, status = 0;

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var PersonalInfoID = new SqlParameter("@PersonalInfoID", hrmTranPersonalInfo.PersonalInfoId);
                    var GenderID = new SqlParameter("@GenderID", hrmTranPersonalInfo.GenderId ?? (object)DBNull.Value);
                    var TitleID = new SqlParameter("@TitleID", hrmTranPersonalInfo.TitleId ?? (object)DBNull.Value);
                    var EmployeeID = new SqlParameter("@EmployeeID", hrmTranPersonalInfo.EmployeeId ?? (object)DBNull.Value);
                    var FirstName_EN = new SqlParameter("@FirstName_EN", hrmTranPersonalInfo.FirstNameEn ?? (object)DBNull.Value);
                    var FirstName_AR = new SqlParameter("@FirstName_AR", hrmTranPersonalInfo.FirstNameAr ?? (object)DBNull.Value);
                    var MiddleName_EN = new SqlParameter("@MiddleName_EN", hrmTranPersonalInfo.MiddleNameEn ?? (object)DBNull.Value);
                    var MiddleName_AR = new SqlParameter("@MiddleName_AR", hrmTranPersonalInfo.MiddleNameAr ?? (object)DBNull.Value);
                    var LastName_EN = new SqlParameter("@LastName_EN", hrmTranPersonalInfo.LastNameEn ?? (object)DBNull.Value);
                    var LastName_AR = new SqlParameter("@LastName_AR", hrmTranPersonalInfo.LastNameAr ?? (object)DBNull.Value);
                    var FullName_EN = new SqlParameter("@FullName_EN", hrmTranPersonalInfo.FullNameEn ?? (object)DBNull.Value);
                    var FullName_AR = new SqlParameter("@FullName_AR", hrmTranPersonalInfo.FullNameAr ?? (object)DBNull.Value);
                    var PersonTypeID = new SqlParameter("@PersonTypeID", hrmTranPersonalInfo.PersonTypeId ?? (object)DBNull.Value);
                    var MaritalStatusID = new SqlParameter("@MaritalStatusID", hrmTranPersonalInfo.MaritalStatusId ?? (object)DBNull.Value);
                    var DateOfBirth_EN = new SqlParameter("@DateOfBirth_EN", hrmTranPersonalInfo.DateOfBirthEn ?? (object)DBNull.Value);
                    var DateOfBirth_AR = new SqlParameter("@DateOfBirth_AR", hrmTranPersonalInfo.DateOfBirthAr ?? (object)DBNull.Value);
                    var NationalityID = new SqlParameter("@NationalityID", hrmTranPersonalInfo.NationalityId ?? (object)DBNull.Value);
                    var ReligionID = new SqlParameter("@ReligionID", hrmTranPersonalInfo.ReligionId ?? (object)DBNull.Value);
                    var BloodGroupID = new SqlParameter("@BloodGroupID", hrmTranPersonalInfo.BloodGroupId ?? (object)DBNull.Value);
                    var EmployeePicture = new SqlParameter("@EmployeePicture", hrmTranPersonalInfo.EmployeePicture ?? (object)DBNull.Value);
                    var ModifiedBy = new SqlParameter("@ModifiedBy", hrmTranPersonalInfo.ModifiedBy ?? (object)DBNull.Value);
                    var ModifiedDate = new SqlParameter("@ModifiedDate", hrmTranPersonalInfo.ModifiedDate ?? (object)DBNull.Value);

                    //Save personal Info
                    personalInfoStatus = await Context.Database.ExecuteSqlRawAsync("UPDATE  HRM_TRAN_PersonalInfo SET GenderID = @GenderID,TitleID = @TitleID,FirstName_EN = @FirstName_EN, " +
                                     "FirstName_AR = @FirstName_AR, MiddleName_EN = @MiddleName_EN, MiddleName_AR = @MiddleName_AR, " +
                                     "LastName_EN = @LastName_EN, LastName_AR = @LastName_AR, FullName_EN = @FullName_EN, FullName_AR = @FullName_AR, " +
                                     "EmployeeID = @EmployeeID, PersonTypeID = @PersonTypeID, MaritalStatusID = @MaritalStatusID, " +
                                     "DateOfBirth_EN = @DateOfBirth_EN, DateOfBirth_AR = @DateOfBirth_AR, NationalityID = @NationalityID, " +
                                     "ReligionID = @ReligionID, BloodGroupID = @BloodGroupID, EmployeePicture = @EmployeePicture, ModifiedBy = @ModifiedBy," +
                                     "ModifiedDate = @ModifiedDate WHERE PersonalInfoID = @PersonalInfoID", GenderID, TitleID, FirstName_EN, FirstName_AR,
                                     MiddleName_EN, MiddleName_AR, LastName_EN, LastName_AR, FullName_EN, FullName_AR, EmployeeID, PersonTypeID,
                                     MaritalStatusID, DateOfBirth_EN, DateOfBirth_AR, NationalityID, ReligionID, BloodGroupID, EmployeePicture,
                                     ModifiedBy, ModifiedDate, PersonalInfoID);

                    if (personalInfoStatus == 1)
                    {
                        //Save Employee details
                        employeeStatus = await Context.Database.ExecuteSqlRawAsync("UPDATE  HRM_TRAN_Employee SET EmployeeName = @FullName_EN, " +
                            "ModifiedBy = @ModifiedBy, ModifiedDate = GETDATE() WHERE EmployeeID = @EmployeeID", FullName_EN, ModifiedBy,
                            ModifiedDate, FirstName_AR, EmployeeID);

                        if (employeeStatus == 1)
                        {
                            status = 1;
                            transaction.Commit();
                        }
                        else
                            transaction.Rollback();
                    }
                    else
                        transaction.Rollback();
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    transaction.Rollback();
                }
            }
            return status;
        }

        #region EmployeeContactInfo
        public async Task<HrmTranContactInfo> GetEmployeeContactInfo(long employeeID)
        {
            HrmTranContactInfo response = new HrmTranContactInfo();
            try
            {
                var result = await Context.HrmTranContactInfos
                    .Where(a => a.EmployeeId == employeeID)
                    .ToListAsync();

                response = result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return response;
        }

        public async Task<List<HrmTranMapToContactInfo>> GetEmployeeMapToContactInfo(long employeeID)
        {
            List<HrmTranMapToContactInfo> response = new List<HrmTranMapToContactInfo>();
            try
            {
                response = await Context.HrmTranMapToContactInfos
                   .Where(a => a.EmployeeId == employeeID)
                   .ToListAsync();

            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return response;
        }

        public async Task<int> UpdateContactInfo(HrmTranContactInfo hrmTranContactInfo)
        {
            int status = 0;
            try
            {
                var PrimaryMobileNumber = new SqlParameter("@PrimaryMobileNumber", hrmTranContactInfo.PrimaryMobileNumber);
                var EmployeeID = new SqlParameter("@EmployeeID", hrmTranContactInfo.EmployeeId ?? (object)DBNull.Value);
                var Email = new SqlParameter("@Email", hrmTranContactInfo.Email);
                var ModifiedBy = new SqlParameter("@ModifiedBy", hrmTranContactInfo.ModifiedBy ?? (object)DBNull.Value);
                var ModifiedDate = new SqlParameter("@ModifiedDate", hrmTranContactInfo.ModifiedDate ?? (object)DBNull.Value);
                var ContactInfoId = new SqlParameter("@ContactInfoId", hrmTranContactInfo.ContactInfoId);

                //Save personal Info
                status = await Context.Database.ExecuteSqlRawAsync("UPDATE HRM_TRAN_ContactInfo SET	" +
                    "PrimaryMobileNumber = @PrimaryMobileNumber, EmployeeID = @EmployeeID, Email = @Email, " +
                    "ModifiedBy = @ModifiedBy, ModifiedDate = @ModifiedDate WHERE ContactInfoId = @ContactInfoId",
                    PrimaryMobileNumber, EmployeeID, Email, ModifiedBy, ModifiedDate, ContactInfoId);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return status;
        }

        public async Task<int> SaveContactInfo(HrmTranContactInfo hrmTranContactInfo)
        {
            int status = 0;
            try
            {
                var PrimaryMobileNumber = new SqlParameter("@PrimaryMobileNumber", hrmTranContactInfo.PrimaryMobileNumber);
                var EmployeeID = new SqlParameter("@EmployeeID", hrmTranContactInfo.EmployeeId ?? (object)DBNull.Value);
                var Email = new SqlParameter("@Email", hrmTranContactInfo.Email);
                var CreatedBy = new SqlParameter("@CreatedBy", hrmTranContactInfo.CreatedBy ?? (object)DBNull.Value);
                var CreatedDate = new SqlParameter("@CreatedDate", hrmTranContactInfo.CreatedDate ?? (object)DBNull.Value);

                //Save personal Info
                status = await Context.Database.ExecuteSqlRawAsync("INSERT INTO HRM_TRAN_ContactInfo(PrimaryMobileNumber, Email, EmployeeID, " +
                    "IsActive, CreatedBy, CreatedDate) VALUES(@PrimaryMobileNumber, @Email, @EmployeeID, 1, @CreatedBy," +
                    " GETDATE(), NULL, NULL)", PrimaryMobileNumber, Email, EmployeeID, CreatedBy, CreatedDate);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return status;
        }

        public async Task<HrmTranMapToContactInfo> GetEmployeeMapToContactInfoByID(long employeeID, long MapContactInfoId)
        {
            HrmTranMapToContactInfo response = new HrmTranMapToContactInfo();
            try
            {
                response = await Context.HrmTranMapToContactInfos
                   .Where(a => a.EmployeeId == employeeID && a.MapContactInfoId == MapContactInfoId)
                   .FirstOrDefaultAsync();

            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return response;
        }
        public async Task<int> UpdateEmployeeMapToContactInfo(HrmTranMapToContactInfo hrmTranMapToContactInfo)
        {
            int status = 0;
            try
            {
                var ContactPersonName = new SqlParameter("@ContactPersonName", hrmTranMapToContactInfo.ContactPersonName);
                var ConatactNumber = new SqlParameter("@ConatactNumber", hrmTranMapToContactInfo.ConatactNumber ?? (object)DBNull.Value);
                var IsFamily = new SqlParameter("@IsFamily", hrmTranMapToContactInfo.IsFamily);
                var ContactAddress = new SqlParameter("@ContactAddress", hrmTranMapToContactInfo.ContactAddress);
                var IsEmergency = new SqlParameter("@IsEmergency", hrmTranMapToContactInfo.IsEmergency ?? (object)DBNull.Value);
                var Remarks = new SqlParameter("@Remarks", hrmTranMapToContactInfo.Remarks);
                var MapContactInfoId = new SqlParameter("@MapContactInfoID", hrmTranMapToContactInfo.MapContactInfoId);

                status = await Context.Database.ExecuteSqlRawAsync("UPDATE HRM_TRAN_MapToContactInfo SET ContactPersonName = @ContactPersonName ," +
                    "ConatactNumber = @ConatactNumber, IsFamily = @IsFamily, ContactAddress = @ContactAddress, IsEmergency = @IsEmergency, " +
                    "Remarks = @Remarks WHERE  MapContactInfoID = @MapContactInfoId",
                    ContactPersonName, ConatactNumber, IsFamily, ContactAddress, IsEmergency, Remarks, MapContactInfoId);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return status;
        }

        public async Task<int> SaveEmployeeMapToContactInfo(HrmTranMapToContactInfo hrmTranMapToContactInfo)
        {
            int status = 0;
            try
            {
                var ContactPersonName = new SqlParameter("@ContactPersonName", hrmTranMapToContactInfo.ContactPersonName);
                var ConatactNumber = new SqlParameter("@ConatactNumber", hrmTranMapToContactInfo.ConatactNumber ?? (object)DBNull.Value);
                var EmployeeID = new SqlParameter("@EmployeeID", hrmTranMapToContactInfo.EmployeeId ?? (object)DBNull.Value);
                var IsFamily = new SqlParameter("@IsFamily", hrmTranMapToContactInfo.IsFamily ?? (object)DBNull.Value);
                var ContactAddress = new SqlParameter("@ContactAddress", hrmTranMapToContactInfo.ContactAddress);
                var IsEmergency = new SqlParameter("@IsEmergency", hrmTranMapToContactInfo.IsEmergency ?? (object)DBNull.Value);
                var Remarks = new SqlParameter("@Remarks", hrmTranMapToContactInfo.Remarks);
                var ContactInfoID = new SqlParameter("@ContactInfoID", hrmTranMapToContactInfo.ContactInfoId ?? (object)DBNull.Value);

                status = await Context.Database.ExecuteSqlRawAsync("INSERT INTO  HRM_TRAN_MapToContactInfo (ContactPersonName, ConatactNumber, " +
                    "EmployeeID, IsFamily, IsEmergency, ContactAddress, Remarks, ContactInfoID) Values(@ContactPersonName ," +
                    " @ConatactNumber, @EmployeeID, @IsFamily, @IsEmergency, @ContactAddress, @Remarks, @ContactInfoID)",
                    ContactPersonName, ConatactNumber, EmployeeID, IsFamily, IsEmergency, ContactAddress, Remarks, ContactInfoID);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return status;
        }

        #endregion

        #region EmployeeDependant

        private bool? HasValue(int? value) => value.HasValue ? value > 0 ? true : false : null;
        private int? HasValue(bool? value) => value.HasValue ? value.Value ? 1 : 0 : null;

        public async Task<int> AddUpdateEmployeeDependant(HrmTranEmployeeDependantModel input)
        {
            HrmTranEmployeeDependant obj = new();
            if (input.EmployeeDependantId > 0)
                obj = await Context.HrmTranEmployeeDependants.FirstOrDefaultAsync(e => e.EmployeeDependantId == input.EmployeeDependantId);


            obj.EmployeeId = input.EmployeeId;
            obj.DependantTypeId = input.DependantTypeId;
            obj.AddressTypeId = input.AddressTypeId;
            obj.IqamaNumber = input.IqamaNumber;
            obj.IqamaExpiryDateEn = input.IqamaExpiryDateEn;
            obj.IqamaExpiryDateAr = input.IqamaExpiryDateAr;
            obj.AddressLine1 = input.AddressLine1;
            obj.AddressLine2 = input.AddressLine2;
            obj.GenderId = input.GenderId;
            obj.PhoneNumber = input.PhoneNumber;
            obj.FirstNameEn = input.FirstNameEn;
            obj.NameInIqamaEn = input.NameInIqamaEn;
            obj.NameInIqamaAr = input.NameInIqamaAr;
            obj.PassportExpiryDateAr = input.PassportExpiryDateAr;
            obj.PassportExpiryDateEn = input.PassportExpiryDateEn;
            obj.DateOfBirthAr = input.DateOfBirthAr;
            obj.DateOfBirthEn = input.DateOfBirthEn;
            obj.LastNameAr = input.LastNameAr;
            obj.LastNameEn = input.LastNameEn;
            obj.UseEmployeeAddress = HasValue(input.UseEmployeeAddress);
            obj.EligibleForExitReentry = HasValue(input.EligibleForExitReentry);
            obj.EligibleForTicket = HasValue(input.EligibleForTicket);
            obj.EligibleForSchooling = HasValue(input.EligibleForSchooling);
            obj.EligibleForInsurance = HasValue(input.EligibleForInsurance);

            if (input.EmployeeDependantId > 0)
            {
                Context.HrmTranEmployeeDependants.Update(obj);
            }
            else
            {
                await Context.HrmTranEmployeeDependants.AddAsync(obj);
            }

            await Context.SaveChangesAsync();
            return 1;

        }

        public async Task<List<HrmTranEmployeeDependantModel>> GetHrmTranEmployeeDependantData(long employeeID, string language)
        {
            try
            {
                var genders = Context.HrmSysGenders.Where(a => a.IsActive == true);
                var dependants = Context.HrmSysDependantTypes.Select(e => new { e.DependantTypeId, e.DependantTypeNameAr, e.DependantTypeNameEn });
                var list = await (from D in Context.HrmTranEmployeeDependants
                                  join T in dependants on D.DependantTypeId equals T.DependantTypeId
                                  where D.EmployeeId == employeeID
                                  select new HrmTranEmployeeDependantModel
                                  {
                                      EmployeeId = D.EmployeeId.Value,
                                      EmployeeDependantId = D.EmployeeDependantId,
                                      SNo = (int)D.EmployeeDependantId,
                                      FirstNameEn = (language == "en") ? D.FirstNameEn : D.FirstNameAr,
                                      LastNameEn = (language == "en") ? D.LastNameEn : D.LastNameAr,
                                      GenderNameEn = genders.Where(e => e.GenderId == D.GenderId).Select(e => (language == "en") ? e.GenderNameEn : e.GenderNameAr).FirstOrDefault(),
                                      DependantTypeNameEn = (language == "en") ? T.DependantTypeNameEn : T.DependantTypeNameAr,
                                      PhoneNumber = D.PhoneNumber,
                                      EligibleForTicketBool = D.EligibleForTicket,
                                      NameInIqamaEn = (language == "en") ? D.NameInIqamaEn : D.NameInIqamaAr,
                                  }).ToListAsync();
                return list;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<HrmTranEmployeeDependantModel> GetHrmTranEmployeeDependantDataById(long Id)
        {
            try
            {
                var D = await Context.HrmTranEmployeeDependants.Where(e => e.EmployeeDependantId == Id).FirstOrDefaultAsync();

                var data = new HrmTranEmployeeDependantModel
                {
                    EmployeeId = D.EmployeeId.Value,
                    EmployeeDependantId = D.EmployeeDependantId,
                    SNo = (int)D.EmployeeDependantId,
                    DependantTypeId = D.DependantTypeId,
                    AddressTypeId = D.AddressTypeId,
                    IqamaNumber = D.IqamaNumber,
                    IqamaExpiryDateEn = D.IqamaExpiryDateEn,
                    IqamaExpiryDateAr = D.IqamaExpiryDateAr,
                    AddressLine1 = D.AddressLine1,
                    AddressLine2 = D.AddressLine2,
                    GenderId = D.GenderId,
                    PhoneNumber = D.PhoneNumber,
                    FirstNameAr = D.FirstNameAr,
                    NameInIqamaEn = D.NameInIqamaEn,
                    NameInIqamaAr = D.NameInIqamaAr,
                    PassportExpiryDateAr = D.PassportExpiryDateAr,
                    PassportExpiryDateEn = D.PassportExpiryDateEn,
                    DateOfBirthAr = D.DateOfBirthAr,
                    DateOfBirthEn = D.DateOfBirthEn,
                    LastNameAr = D.LastNameAr,
                    LastNameEn = D.LastNameEn,
                    UseEmployeeAddress = HasValue(D.UseEmployeeAddress),
                    EligibleForExitReentry = HasValue(D.EligibleForExitReentry),
                    EligibleForTicket = HasValue(D.EligibleForTicket),
                    EligibleForSchooling = HasValue(D.EligibleForSchooling),
                    EligibleForInsurance = HasValue(D.EligibleForInsurance),
                    FirstNameEn = D.FirstNameEn,

                    //GenderNameEn = genders.FirstOrDefault(e => e.GenderId == D.GenderId).GenderNameEn,
                };

                return data;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<int> UpdateEmployeePhoneInfo(HrmTranEmployeePhoneModel hrmTranEmployeePhoneModel)
        {
            int status = 0;
            try
            {
                HrmTranEmployeePhone hrmTranEmployeePhone = new HrmTranEmployeePhone();
                if (hrmTranEmployeePhoneModel.PhoneId > 0)
                    hrmTranEmployeePhone = await Context.HrmTranEmployeePhones.SingleOrDefaultAsync(x => x.PhoneId == hrmTranEmployeePhoneModel.PhoneId);
                hrmTranEmployeePhone.PhoneNumber = hrmTranEmployeePhoneModel.PhoneNumber;
                hrmTranEmployeePhone.PhoneTypeId = hrmTranEmployeePhoneModel.PhoneTypeId;
                hrmTranEmployeePhone.CountryId = hrmTranEmployeePhoneModel.CountryId;
                hrmTranEmployeePhone.AreaCode = hrmTranEmployeePhoneModel.AreaCode;
                hrmTranEmployeePhone.EmployeeId = hrmTranEmployeePhoneModel.EmployeeId;
                if (hrmTranEmployeePhoneModel.PhoneId == 0)
                    await Context.HrmTranEmployeePhones.AddAsync(hrmTranEmployeePhone);
                Context.SaveChanges();
                status = 1;
            }
            catch (Exception ex)
            {

            }
            return status;
        }
        public async Task<int> UpdateEmployeeEducationInfo(EducationInfoModel educationInfoModel)
        {
            int status = 0;
            try
            {
                HrmTranEducationInfo hrmTranEducationInfo = new HrmTranEducationInfo();
                if (educationInfoModel.EducationId > 0)
                    hrmTranEducationInfo = await Context.HrmTranEducationInfos.SingleOrDefaultAsync(x => x.EducationId == educationInfoModel.EducationId);
                hrmTranEducationInfo.Qualification = educationInfoModel.Qualification;
                hrmTranEducationInfo.DegreeId = educationInfoModel.DegreeId;
                hrmTranEducationInfo.CountryId = educationInfoModel.CountryID;
                hrmTranEducationInfo.CollegeOrUniversity = educationInfoModel.CollegeOrUniversity;
                hrmTranEducationInfo.CourseTypeId = educationInfoModel.CourseTypeID;
                hrmTranEducationInfo.Remarks = educationInfoModel.Remarks;
                hrmTranEducationInfo.YearOfCertificationEn = Convert.ToDateTime(educationInfoModel.YearOfCertification_EN);
                hrmTranEducationInfo.YearOfCertificationAr = Convert.ToString(educationInfoModel.YearOfCertification_AR);
                hrmTranEducationInfo.EmployeeId = educationInfoModel.EmployeeId;
                if (educationInfoModel.EducationId == 0)
                    await Context.HrmTranEducationInfos.AddAsync(hrmTranEducationInfo);
                Context.SaveChanges();
                status = 1;
            }
            catch (Exception ex)
            {
                //string message = ex.Message;
            }
            return status;
        }
            
        #endregion

        #region EmployeeDocumentsInfo

        public async Task<List<HrmTranDocumentsInfo>> GetEmployeeDocumentsInfo(long employeeID)
        {
            List<HrmTranDocumentsInfo> response = new List<HrmTranDocumentsInfo>();

            var result = await Context.HrmTranDocumentsInfos
                .Where(a => a.EmployeeId == employeeID)
                .ToListAsync();

            return result;
        }

        public async Task<HrmTranDocumentsInfo> GetEmployeeDocumentsInfoByID(long employeeID, long documentID)
        {
            HrmTranDocumentsInfo response = new HrmTranDocumentsInfo();
            try
            {
                response = await Context.HrmTranDocumentsInfos
                   .Where(a => a.EmployeeId == employeeID && a.DocumentId == documentID)
                   .FirstOrDefaultAsync();

            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return response;
        }

        public async Task<int> UpdateEmployeeDocumentsInfo(HrmTranDocumentsInfo hrmTranDocumentsInfo)
        {
            int status = 0;
            try
            {
                var DocumentTypeID = new SqlParameter("@DocumentTypeID", hrmTranDocumentsInfo.DocumentTypeId ?? (object)DBNull.Value);
                var DocumentNumber = new SqlParameter("@DocumentNumber", hrmTranDocumentsInfo.DocumentNumber ?? (object)DBNull.Value);
                var NameInDocument = new SqlParameter("@NameInDocument", hrmTranDocumentsInfo.NameInDocument ?? (object)DBNull.Value);
                var IssueLocation = new SqlParameter("@IssueLocation", hrmTranDocumentsInfo.IssueLocation ?? (object)DBNull.Value);
                var DocumentOwnerID = new SqlParameter("@DocumentOwnerID", hrmTranDocumentsInfo.DocumentOwnerId ?? (object)DBNull.Value);
                var ProfessionInIqama = new SqlParameter("@ProfessionInIqama", hrmTranDocumentsInfo.ProfessionInIqama ?? (object)DBNull.Value);
                var InsuranceTypeID = new SqlParameter("@InsuranceTypeID", hrmTranDocumentsInfo.InsuranceTypeId ?? (object)DBNull.Value);
                var InsuranceProviderID = new SqlParameter("@InsuranceProviderID", hrmTranDocumentsInfo.InsuranceProviderId ?? (object)DBNull.Value);
                var InsuranceClassID = new SqlParameter("@InsuranceClassID", hrmTranDocumentsInfo.InsuranceClassId ?? (object)DBNull.Value);
                var InsurancePolicyNo = new SqlParameter("@InsurancePolicyNo", hrmTranDocumentsInfo.InsurancePolicyNo ?? (object)DBNull.Value);
                var IssueDateEn = new SqlParameter("@IssueDateEn", hrmTranDocumentsInfo.IssueDateEn ?? (object)DBNull.Value);
                var IssueDateAr = new SqlParameter("@IssueDateAr", hrmTranDocumentsInfo.IssueDateAr ?? (object)DBNull.Value);
                var ExpiryDateEn = new SqlParameter("@ExpiryDateEn", hrmTranDocumentsInfo.ExpiryDateEn ?? (object)DBNull.Value);
                var ExpiryDateAr = new SqlParameter("@ExpiryDateAr", hrmTranDocumentsInfo.ExpiryDateAr ?? (object)DBNull.Value);
                var Costing = new SqlParameter("@Costing", hrmTranDocumentsInfo.Costing ?? (object)DBNull.Value);
                var DocumentStatus = new SqlParameter("@DocumentStatus", hrmTranDocumentsInfo.DocumentStatus ?? (object)DBNull.Value);
                var ScannedDocument = new SqlParameter("@ScannedDocument", hrmTranDocumentsInfo.ScannedDocument ?? (object)DBNull.Value);
                var Remarks = new SqlParameter("@Remarks", hrmTranDocumentsInfo.Remarks ?? (object)DBNull.Value);
                var ModifiedBy = new SqlParameter("@ModifiedBy", hrmTranDocumentsInfo.ModifiedBy ?? (object)DBNull.Value);
                var ModifiedDate = new SqlParameter("@ModifiedDate", hrmTranDocumentsInfo.ModifiedDate ?? (object)DBNull.Value);
                var EmployeeID = new SqlParameter("@EmployeeID", hrmTranDocumentsInfo.EmployeeId ?? (object)DBNull.Value);
                var DocumentID = new SqlParameter("@DocumentID", hrmTranDocumentsInfo.DocumentId);

                status = await Context.Database.ExecuteSqlRawAsync("UPDATE HRM_TRAN_DocumentsInfo SET DocumentTypeID = @DocumentTypeID, " +
                    "DocumentNumber = @DocumentNumber, NameInDocument = @NameInDocument, IssueLocation = @IssueLocation, " +
                    "DocumentOwnerID = @DocumentOwnerID, ProfessionInIqama = @ProfessionInIqama, InsuranceTypeID = @InsuranceTypeID, " +
                    "InsuranceProviderID = @InsuranceProviderID, InsuranceClassID = @InsuranceClassID, InsurancePolicyNo = @InsurancePolicyNo, " +
                    "IssueDate_EN = @IssueDateEn, IssueDate_AR = @IssueDateAr, ExpiryDate_EN = @ExpiryDateEn, ExpiryDate_AR = @ExpiryDateAr, " +
                    "Costing = @Costing, DocumentStatus = @DocumentStatus, ScannedDocument = @ScannedDocument, Remarks = @Remarks, " +
                    "ModifiedBy = @ModifiedBy, ModifiedDate = ModifiedDate WHERE EmployeeID = @EmployeeID AND DocumentID=@DocumentID",
                    DocumentTypeID, DocumentNumber, NameInDocument, IssueLocation, DocumentOwnerID, ProfessionInIqama, InsuranceTypeID,
                    InsuranceProviderID, InsuranceClassID, InsurancePolicyNo, IssueDateEn, IssueDateAr, ExpiryDateEn, ExpiryDateAr,
                    Costing, DocumentStatus, ScannedDocument, Remarks, ModifiedBy, ModifiedDate, EmployeeID, DocumentID);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return status;
        }

        public async Task<int> SaveEmployeeDocumentsInfo(HrmTranDocumentsInfo hrmTranDocumentsInfo)
        {
            int status = 0;
            try
            {
                var DocumentTypeID = new SqlParameter("@DocumentTypeID", hrmTranDocumentsInfo.DocumentTypeId);
                var DocumentNumber = new SqlParameter("@DocumentNumber", hrmTranDocumentsInfo.DocumentNumber ?? (object)DBNull.Value);
                var NameInDocument = new SqlParameter("@NameInDocument", hrmTranDocumentsInfo.NameInDocument ?? (object)DBNull.Value);
                var IssueLocation = new SqlParameter("@IssueLocation", hrmTranDocumentsInfo.IssueLocation ?? (object)DBNull.Value);
                var DocumentOwnerID = new SqlParameter("@DocumentOwnerID", hrmTranDocumentsInfo.DocumentOwnerId ?? (object)DBNull.Value);
                var ProfessionInIqama = new SqlParameter("@ProfessionInIqama", hrmTranDocumentsInfo.ProfessionInIqama ?? (object)DBNull.Value);
                var InsuranceTypeID = new SqlParameter("@InsuranceTypeID", hrmTranDocumentsInfo.InsuranceTypeId ?? (object)DBNull.Value);
                var InsuranceProviderID = new SqlParameter("@InsuranceProviderID", hrmTranDocumentsInfo.InsuranceProviderId ?? (object)DBNull.Value);
                var InsuranceClassID = new SqlParameter("@InsuranceClassID", hrmTranDocumentsInfo.InsuranceClassId ?? (object)DBNull.Value);
                var InsurancePolicyNo = new SqlParameter("@InsurancePolicyNo", hrmTranDocumentsInfo.InsurancePolicyNo ?? (object)DBNull.Value);
                var IssueDateEn = new SqlParameter("@IssueDateEn", hrmTranDocumentsInfo.IssueDateEn ?? (object)DBNull.Value);
                var IssueDateAr = new SqlParameter("@IssueDateAr", hrmTranDocumentsInfo.IssueDateAr ?? (object)DBNull.Value);
                var ExpiryDateEn = new SqlParameter("@ExpiryDateEn", hrmTranDocumentsInfo.ExpiryDateEn ?? (object)DBNull.Value);
                var ExpiryDateAr = new SqlParameter("@ExpiryDateAr", hrmTranDocumentsInfo.ExpiryDateAr ?? (object)DBNull.Value);
                var Costing = new SqlParameter("@Costing", hrmTranDocumentsInfo.Costing ?? (object)DBNull.Value);
                var DocumentStatus = new SqlParameter("@DocumentStatus", hrmTranDocumentsInfo.DocumentStatus ?? (object)DBNull.Value);
                var ScannedDocument = new SqlParameter("@ScannedDocument", hrmTranDocumentsInfo.ScannedDocument ?? (object)DBNull.Value);
                var Remarks = new SqlParameter("@Remarks", hrmTranDocumentsInfo.Remarks ?? (object)DBNull.Value);
                var CreatedBy = new SqlParameter("@CreatedBy", hrmTranDocumentsInfo.CreatedBy ?? (object)DBNull.Value);
                var CreatedDate = new SqlParameter("@CreatedDate", hrmTranDocumentsInfo.CreatedDate ?? (object)DBNull.Value);
                var EmployeeID = new SqlParameter("@EmployeeID", hrmTranDocumentsInfo.EmployeeId ?? (object)DBNull.Value);

                status = await Context.Database.ExecuteSqlRawAsync("INSERT INTO HRM_TRAN_DocumentsInfo (DocumentTypeID, DocumentNumber, " +
                    "EmployeeID, NameInDocument, IssueLocation, DocumentOwnerID, ProfessionInIqama, InsuranceTypeID, InsuranceProviderID, " +
                    "InsuranceClassID, InsurancePolicyNo, IssueDate_EN, IssueDate_AR, ExpiryDate_EN, ExpiryDate_AR, Costing, DocumentStatus, " +
                    "ScannedDocument, Remarks, CreatedBy, CreatedDate) VALUES (@DocumentTypeID, @DocumentNumber, @EmployeeID, @NameInDocument, " +
                    "@IssueLocation, @DocumentOwnerID, @ProfessionInIqama, @InsuranceTypeID, @InsuranceProviderID, @InsuranceClassID, " +
                    "@InsurancePolicyNo, @IssueDateEn, @IssueDateAr, @ExpiryDateEn, @ExpiryDateAr, @Costing, @DocumentStatus, @ScannedDocument, " +
                    "@Remarks, @CreatedBy, @CreatedDate)",
                    DocumentTypeID, DocumentNumber, EmployeeID, NameInDocument, IssueLocation, DocumentOwnerID, ProfessionInIqama, InsuranceTypeID,
                    InsuranceProviderID, InsuranceClassID, InsurancePolicyNo, IssueDateEn, IssueDateAr, ExpiryDateEn, ExpiryDateAr, Costing,
                    DocumentStatus, ScannedDocument, Remarks, CreatedBy, CreatedDate);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return status;
        }

       

        #endregion
    }
}
