﻿using DMC.HRM.Entities.Models;
using DMC.HRM.UI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Common
{
    public class BaseController : Controller
    {
        #region language-selection

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("CurrentCulture")))
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpContext.Session.GetString("CurrentCulture"));
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpContext.Session.GetString("CurrentCulture"));
            }
            else
            {
                HttpContext.Session.SetString("CurrentCulture", "en");
            }
        }

        // changing culture
        [AllowAnonymous]
        public ActionResult ChangeCulture(string ddlCulture)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(ddlCulture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(ddlCulture);
            HttpContext.Session.SetString("CurrentCulture", ddlCulture);
            //TempData["CurrentCulture"] = ddlCulture == "en" ? "en-US" : (ddlCulture == "en-US" ? "en-US" : ddlCulture);
            //TempData.Keep("CurrentCulture");
            return Redirect(Request.GetTypedHeaders().Referer.ToString());
        }

        #endregion

        protected HrmDefUser GetUser()
        {
            UserModel User = new();
            if (HttpContext.Session.GetString("User") != "")
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));
            return new HrmDefUser() { UserId = User.UserId, RoleId = User.RoleId, EmployeeId = User.EmployeeID };

        }

        protected Dictionary<string, string> GetIdentity()
        {
            var hrmDefUser = GetUser();
            return new Dictionary<string, string>() {
                {"employeeID",hrmDefUser.EmployeeId.ToString() }
                };
        }
    }
}
