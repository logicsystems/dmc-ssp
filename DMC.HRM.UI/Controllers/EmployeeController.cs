﻿using DMC.HRM.Entities;
using DMC.HRM.Entities.Models;
using DMC.HRM.Entities.ResponseModels;
using DMC.HRM.UI.Common;
using DMC.HRM.UI.Helpers;
using DMC.HRM.UI.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Controllers
{
    public class EmployeeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IEmailService _emailService;
        private IConfiguration _configuration;

        public EmployeeController(ILogger<HomeController> logger, IHttpClientFactory httpClientFactory, IWebHostEnvironment hostingEnvironment, IEmailService emailService, IConfiguration configuration)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _hostingEnvironment = hostingEnvironment;
            _emailService = emailService;
            _configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> GetPersonalInfo()
        {
            UserModel User = null;
            HrmTranPersonalInfoModel model = new HrmTranPersonalInfoModel();

            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");

            if (HttpContext.Session.GetString("User") != "")
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

            var hrmDefUser = new HrmDefUser() { UserId = User.UserId, RoleId = User.RoleId, EmployeeId = User.EmployeeID };

            #region Employee PersonalInfo

            StringContent content = new StringContent(JsonConvert.SerializeObject(hrmDefUser), Encoding.UTF8, "application/json");
            using (var response = await httpClient.PostAsync("Employee/GetPersonalInfo", content))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    model = JsonConvert.DeserializeObject<HrmTranPersonalInfoModel>(data);
                    HttpContext.Session.SetString("EmployeePicture", model.EmployeePicture);
                }
            }

            #endregion

            #region Employee Name & Employee Number

            var queryParams = new Dictionary<string, string>() {
                {"employeeID",hrmDefUser.EmployeeId.ToString() }
                };
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeByID", queryParams);
            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employee = JsonConvert.DeserializeObject<HrmTranEmployee>(data);
                    model.Employee = new EmployeeInfoModel();
                    model.Employee.EmployeeName = employee.EmployeeName;
                    model.Employee.EmployeeNumber = employee.EmployeeNumber;
                    model.Employee.EmployeePicture = model.EmployeePicture;
                }
            }

            #endregion

            #region Genders

            using (var response = await httpClient.GetAsync("Common/GetAllGenders"))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var genders = JsonConvert.DeserializeObject<List<HrmSysGender>>(data);
                    model.Genders = genders;
                }
            }
            #endregion

            #region Nationalities

            using (var response = await httpClient.GetAsync("Common/GetAllNationalities"))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var nationalities = JsonConvert.DeserializeObject<List<HrmSysNationality>>(data);
                    model.Nationalities = nationalities;
                }
            }
            #endregion

            #region Person Types

            using (var response = await httpClient.GetAsync("Common/GetAllPersonTypes"))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var personTypes = JsonConvert.DeserializeObject<List<HrmDefPersonType>>(data);
                    model.PersonTypes = personTypes;
                }
            }
            #endregion

            #region Religions

            using (var response = await httpClient.GetAsync("Common/GetAllReligions"))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var religions = JsonConvert.DeserializeObject<List<HrmSysReligion>>(data);
                    model.Religions = religions;
                }
            }
            #endregion

            #region Titles

            using (var response = await httpClient.GetAsync("Common/GetAllTitles"))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var titles = JsonConvert.DeserializeObject<List<HrmSysTitle>>(data);
                    model.Titiles = titles;
                }
            }
            #endregion

            #region Blood Groups

            using (var response = await httpClient.GetAsync("Common/GetAllBloodGroups"))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var bloodGroups = JsonConvert.DeserializeObject<List<HrmSysBloodGroup>>(data);
                    model.BloodGroups = bloodGroups;
                }
            }
            #endregion

            #region Marital Status

            using (var response = await httpClient.GetAsync("Common/GetAllMaritalStatus"))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var maritalStatuses = JsonConvert.DeserializeObject<List<HrmSysMaritalStatus>>(data);
                    model.MaritalStatuses = maritalStatuses;
                }
            }
            #endregion

            return View("~/Views/Employee/GetPersonalInfo.cshtml", model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePersonalInfo(HrmTranPersonalInfoModel model)
        {
            if (ModelState.IsValid)
            {
                var file = model.ImageUpload;
                if (file != null)
                {
                    Random number = new Random(1);
                    string pic = Path.GetFileName(model.FirstNameEn + "_" + number.Next(999) + file.FileName);
                    if (file.Length > 0)
                    {
                        string path = Path.Combine(_hostingEnvironment.WebRootPath, "images/ProfileImages", pic);
                        using (Stream fileStream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                        }
                    }

                    model.EmployeePicture = pic;
                }

                var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");

                UserModel User = null;
                if (HttpContext.Session.GetString("User") != "")
                    User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

                HrmTranPersonalInfo hrmTranPersonalInfo = new HrmTranPersonalInfo
                {
                    PersonalInfoId = model.PersonalInfoId,
                    GenderId = model.GenderId,
                    TitleId = model.TitleId,
                    EmployeeId = model.EmployeeId,
                    FirstNameEn = model.FirstNameEn,
                    FirstNameAr = model.FirstNameAr,
                    MiddleNameEn = model.MiddleNameEn,
                    MiddleNameAr = model.MiddleNameAr,
                    LastNameEn = model.LastNameEn,
                    LastNameAr = model.LastNameAr,
                    FullNameEn = model.FullNameEn,
                    FullNameAr = model.FullNameAr,
                    PersonTypeId = model.PersonTypeId,
                    MaritalStatusId = model.MaritalStatusId,
                    DateOfBirthEn = model.DateOfBirthEn,
                    DateOfBirthAr = model.DateOfBirthAr,
                    NationalityId = model.NationalityId,
                    ReligionId = model.ReligionId,
                    BloodGroupId = model.BloodGroupId,
                    EmployeePicture = model.EmployeePicture,
                    ModifiedBy = User.UserId,
                    ModifiedDate = DateTime.Now
                };

                StringContent content = new StringContent(JsonConvert.SerializeObject(hrmTranPersonalInfo), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("Employee/UpdatePersonalInfo", content))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        model = JsonConvert.DeserializeObject<HrmTranPersonalInfoModel>(data);

                        MailRequestModel mailRequest = new MailRequestModel();
                        mailRequest.To = Convert.ToString(_configuration["EmailConfig:To"]);
                        mailRequest.Subject = User.UserName+ " has updated his Personal Info.";
                        mailRequest.Body = "<h1>This is to inform that " + User.UserName + " has Updated his Personal details.</h1>";
                        await _emailService.SendEmailAsync(mailRequest);

                        TempData["SuccessMessage"] = "Successfully Updated !";
                    }
                    else
                        TempData["ErroMessage"] = "Record not Updated !";
                }
            }
            return RedirectToAction("GetPersonalInfo");
        }

        [HttpGet]
        public async Task<IActionResult> GetEmployeePhoneInfo()
        {
            UserModel User = null;
            var language = HttpContext.Session.GetString("CurrentCulture") == "en" ? "1" : "2";

            HrmTranEmployeePhoneBaseModel model = new HrmTranEmployeePhoneBaseModel();
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            if (HttpContext.Session.GetString("User") != "")
            {
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));
            }
            #region Employee Name & Employee Number
            var queryParams = new Dictionary<string, string>() {
                {"employeeID",User.EmployeeID.ToString() }
                };
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeByID", queryParams);
            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employee = JsonConvert.DeserializeObject<HrmTranEmployee>(data);
                    model.EmployeeId = User.EmployeeID;
                    model.EmployeeName = employee.EmployeeName;
                    model.EmployeeNumber = User.EmployeeNumber;
                    model.EmployeePicture = User.EmployeePicture;
                }
            }
            #endregion
            var EmployeePhones = new List<HrmTranEmployeePhoneModel>();
            var employeeQueryParams = new Dictionary<string, string>() {
                {"employeeID",User.EmployeeID.ToString() },{"languageID",language }
                };
            string employeeRequestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeePhoneList", employeeQueryParams);
            using (var response = await httpClient.GetAsync(employeeRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    EmployeePhones = JsonConvert.DeserializeObject<List<HrmTranEmployeePhoneModel>>(data);
                }
            }
            model.EmployeePhones = EmployeePhones;
            return View("~/Views/Employee/GetEmployeePhoneInfo.cshtml", model);
        }
        [HttpPost]
        public async Task<IActionResult> EditEmployeePhoneInfo(long employeeID, long phoneID)
        {
            var language = HttpContext.Session.GetString("CurrentCulture") == "en" ? "1" : "2";

            HrmTranEmployeePhoneModel model = new HrmTranEmployeePhoneModel();
            model.EmployeeId = employeeID;
            model.PhoneId = phoneID;
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            if (phoneID > 0)
            {
                var queryParams = new Dictionary<string, string>() {
                {"phoneID",phoneID.ToString() },{"languageID",language}
                };
                string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeePhoneByID", queryParams);
                using (var response = await httpClient.GetAsync(requestUrl))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        model = JsonConvert.DeserializeObject<HrmTranEmployeePhoneModel>(data);
                    }
                }
            }
            var phoneQueryParams = new Dictionary<string, string>() {
                {"languageID",language}
                };
            string phoneRequestUrl = QueryHelpers.AddQueryString("Common/GetAllPhoneTypes", phoneQueryParams);
            using (var response = await httpClient.GetAsync(phoneRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    model.PhoneTypes = JsonConvert.DeserializeObject<List<DropDownDTO>>(data);
                }
            }
            var countryQueryParams = new Dictionary<string, string>() {
                {"languageID",language}
                };
            string countryRequestUrl = QueryHelpers.AddQueryString("Common/GetAllCountries", countryQueryParams);
            using (var response = await httpClient.GetAsync(countryRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    model.Countries = JsonConvert.DeserializeObject<List<DropDownDTO>>(data);
                }
            }
            return PartialView("_AddUpdateEmployeePhoneInfo", model);
        }



        [HttpPost]
        public async Task<IActionResult> UpdateEmployeePhoneInfo(HrmTranEmployeePhoneModel model)
        {
            if (ModelState.IsValid)
            {
                var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
                UserModel User = null;
                if (HttpContext.Session.GetString("User") != "")
                    User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));
                StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("Employee/UpdateEmployeePhoneInfo", content))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        int status = JsonConvert.DeserializeObject<int>(data);
                        if (status == 1)
                        {
                            MailRequestModel mailRequest = new MailRequestModel();
                            mailRequest.To = Convert.ToString(_configuration["EmailConfig:To"]);
                            mailRequest.Subject = User.UserName + " has updated his Phone details.";
                            mailRequest.Body = "<h1>This is to inform that " + User.UserName + " has updated his Phone details.</h1>";
                            await _emailService.SendEmailAsync(mailRequest);

                            TempData["SuccessMessage"] = "Successfully Updated !";
                        }
                        else
                            TempData["ErroMessage"] = "Record not Updated !";
                    }
                    else
                        TempData["ErroMessage"] = "Record not Updated !";
                }
            }
            return RedirectToAction("GetEmployeePhoneInfo");
        }

        [HttpPost]
        public async Task<IActionResult> EditEmployeeAddress(long employeeID, long employeeAddressId)
        {
            HrmTranEmployeeAddressModel model = new HrmTranEmployeeAddressModel();
            var language = HttpContext.Session.GetString("CurrentCulture") == "en" ? "1" : "2";
            model.EmployeeId = employeeID;
            model.EmployeeAddressId = employeeAddressId;
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            if (employeeAddressId > 0)
            {
                var queryParams = new Dictionary<string, string>() {
                {"employeeAddressId",employeeAddressId.ToString() }
                };
                string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeAddressByID", queryParams);
                using (var response = await httpClient.GetAsync(requestUrl))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        model = JsonConvert.DeserializeObject<HrmTranEmployeeAddressModel>(data);
                    }
                }
            }
            var addressQueryParams = new Dictionary<string, string>() {
                {"languageID",language}
                };
            string addressRequestUrl = QueryHelpers.AddQueryString("Common/GetAllAddressType", addressQueryParams);
            using (var response = await httpClient.GetAsync(addressRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    model.AddressTypes = JsonConvert.DeserializeObject<List<DropDownDTO>>(data);
                }
            }
            var countryQueryParams = new Dictionary<string, string>() {
                {"languageID",language}
                };
            string countryRequestUrl = QueryHelpers.AddQueryString("Common/GetAllCountries", countryQueryParams);
            using (var response = await httpClient.GetAsync(countryRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    model.Countries = JsonConvert.DeserializeObject<List<DropDownDTO>>(data);
                }
            }

            return PartialView("_AddUpdateEmployeeAddress", model);
        }


        [HttpPost]
        public async Task<IActionResult> UpdateEmployeeAddress(HrmTranEmployeeAddressModel model)
        {
            if (ModelState.IsValid)
            {
                var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
                UserModel User = null;
                if (HttpContext.Session.GetString("User") != "")
                    User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

                StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("Employee/UpdateEmployeeAddress", content))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        int status = JsonConvert.DeserializeObject<int>(data);
                        if (status == 1)
                        {
                            MailRequestModel mailRequest = new MailRequestModel();
                            mailRequest.To = Convert.ToString(_configuration["EmailConfig:To"]);
                            mailRequest.Subject = User.UserName + " has updated his address details.";
                            mailRequest.Body = "<h1>This is to inform that " + User.UserName + " has updated his address details.</h1>";
                            await _emailService.SendEmailAsync(mailRequest);

                            TempData["SuccessMessage"] = "Successfully Updated !";
                        }
                        else
                            TempData["ErroMessage"] = "Record not Updated !";
                    }
                    else
                        TempData["ErroMessage"] = "Record not Updated !";
                }
                model.CreatedBy = User.UserId;

            }
            return RedirectToAction("GetEmployeeAddress");
        }

        #region AddUpdateEmployeeDependant

        [HttpGet]
        public async Task<IActionResult> GetEmployeeDependants()
        {
            UserModel User = null;

            HrmTranEmployeeDependantBaseModel model = new HrmTranEmployeeDependantBaseModel();

            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");

            if (HttpContext.Session.GetString("User") != "")
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

            var hrmDefUser = new HrmDefUser() { UserId = User.UserId, RoleId = User.RoleId, EmployeeId = User.EmployeeID };

            #region Employee Name & Employee Number

            var queryParams = new Dictionary<string, string>() {
                {"employeeID",hrmDefUser.EmployeeId.ToString() }
                };
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeByID", queryParams);
            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employee = JsonConvert.DeserializeObject<HrmTranEmployee>(data);
                    model.Employee = new EmployeeInfoModel();
                    model.Employee.EmployeeName = employee.EmployeeName;
                    model.Employee.EmployeeNumber = User.EmployeeNumber;
                    model.Employee.EmployeePicture = User.EmployeePicture;
                }
                else
                {
                    model = new HrmTranEmployeeDependantBaseModel
                    {
                        Employee = new() { EmployeeId = (long)hrmDefUser.EmployeeId },
                        EmployeeDependants = new()
                    };
                }
            }

            #endregion

            model.EmployeeDependants = await GetHrmTranEmployeeDependantData(httpClient);

            return View("~/Views/Employee/GetEmployeeDependants.cshtml", model);
        }



        [HttpGet]
        public async Task<IActionResult> AddUpdateEmployeeDependant(long? id)
        {
            HrmTranEmployeeDependantModel model = new();
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            if (id is not null && id > 0)
            {
                model = await GetHrmTranEmployeeDependantDataById(httpClient, (long)id);
                model.EmployeeDependantId = (long)id;
            }
            var empInfo = await GetEmployeeInfoModel(httpClient);

            model.YesOrNoDropDown = await GetYesorNoDropDownList(httpClient);
            model.DependantTypes = await GetAllDependantTypeList(httpClient);
            model.Genders = await GetAllGenders(httpClient);
            model.AddressTypes = await GetAllAddressTypes(httpClient);
            model.EmployeeNumber = empInfo.EmployeeNumber;
            model.EmployeeId = GetUser().EmployeeId.Value;

            return PartialView("~/Views/Employee/_AddUpdateEmployeeDependant.cshtml", model);
        }


        [HttpPost]
        public async Task<IActionResult> AddUpdateEmployeeDependant(HrmTranEmployeeDependantModel model)
        {
            var data = model;
            HrmDefUser hrmDefUser = new HrmDefUser();
            hrmDefUser = GetUser();
            model.EmployeeId = hrmDefUser.EmployeeId.Value;
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            using (var response = await httpClient.PostAsync("Employee/AddUpdateEmployeeDependant", content))
            {
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    int status = JsonConvert.DeserializeObject<int>(result);
                    if (status == 1)
                    {
                        MailRequestModel mailRequest = new MailRequestModel();
                        mailRequest.To = Convert.ToString(_configuration["EmailConfig:To"]);
                        mailRequest.Subject = hrmDefUser.UserName + " has updated his dependent details.";
                        mailRequest.Body = "<h1>This is to inform that " + hrmDefUser.UserName + " has updated his dependent details.</h1>";
                        await _emailService.SendEmailAsync(mailRequest);

                        TempData["SuccessMessage"] = "Successfully Updated !";
                    }
                    else
                        TempData["ErroMessage"] = "Record not Updated !";
                }
                else
                    TempData["ErroMessage"] = "Record not Updated !";
            }
            return RedirectToAction("GetEmployeeDependants");
        }


        private async Task<HrmTranEmployeeDependantModel> GetHrmTranEmployeeDependantDataById(HttpClient httpClient, long id)
        {
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetHrmTranEmployeeDependantDataById?id=" + id, GetIdentity());

            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<HrmTranEmployeeDependantModel>(data);
                }
                return new HrmTranEmployeeDependantModel();
            }
        }


        private async Task<List<DropDownListItem>> GetYesorNoDropDownList(HttpClient httpClient)
        {
            string requestUrl = QueryHelpers.AddQueryString("Common/YesorNoDropDownList", GetIdentity());

            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<DropDownListItem>>(data);
                }
                return new List<DropDownListItem>();
            }
        }
        private async Task<List<HrmSysDependantType>> GetAllDependantTypeList(HttpClient httpClient)
        {
            string requestUrl = QueryHelpers.AddQueryString("Common/getAllDependantTypeList", GetIdentity());

            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<HrmSysDependantType>>(data);
                }
                return new List<HrmSysDependantType>();
            }
        }
        private async Task<List<HrmSysGender>> GetAllGenders(HttpClient httpClient)
        {
            string requestUrl = QueryHelpers.AddQueryString("Common/GetAllGenders", GetIdentity());

            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<HrmSysGender>>(data);
                }
                return new List<HrmSysGender>();
            }
        }
        private async Task<List<HrmSysAddressType>> GetAllAddressTypes(HttpClient httpClient)
        {
            string requestUrl = QueryHelpers.AddQueryString("Common/getAllAddressTypes", GetIdentity());

            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<HrmSysAddressType>>(data);
                }
                return new List<HrmSysAddressType>();
            }
        }

        private async Task<EmployeeInfoModel> GetEmployeeInfoModel(HttpClient httpClient)
        {
            #region Employee Name & Employee Number

            var model = new EmployeeInfoModel();
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeByID", GetIdentity());
            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employee = JsonConvert.DeserializeObject<HrmTranEmployee>(data);

                    model.EmployeeName = employee.EmployeeName;
                    model.EmployeeNumber = employee.EmployeeNumber;
                    model.EmployeePicture = HttpContext.Session.GetString("EmployeePicture") ?? "";
                }
                return model;
            }

            #endregion
        }

        private async Task<List<HrmTranEmployeeDependantModel>> GetHrmTranEmployeeDependantData(HttpClient httpClient)
        {
            var language = HttpContext.Session.GetString("CurrentCulture") ?? "en";

            HrmDefUser hrmDefUser = new HrmDefUser();
            hrmDefUser = GetUser();
            var queryParams = new Dictionary<string, string>() {
                {"employeeID",hrmDefUser.EmployeeId.ToString() },
                {"language",language }
                };
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetHrmTranEmployeeDependantData", queryParams);

            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<HrmTranEmployeeDependantModel>>(data);
                }
                return new List<HrmTranEmployeeDependantModel>();
            }
        }
        #endregion

        #region EmployeeContactInfo

        [HttpGet]
        public async Task<IActionResult> GetEmployeeContactInfo()
        {
            UserModel User = null;

            HrmTranContactInfoBaseModel model = new HrmTranContactInfoBaseModel();

            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");

            if (HttpContext.Session.GetString("User") != "")
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

            var hrmDefUser = new HrmDefUser() { UserId = User.UserId, RoleId = User.RoleId, EmployeeId = User.EmployeeID };

            var queryParams = new Dictionary<string, string>() {
                {"employeeID",hrmDefUser.EmployeeId.ToString() }
                };
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeByID", queryParams);
            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employee = JsonConvert.DeserializeObject<HrmTranEmployee>(data);
                    model.EmployeeName = employee.EmployeeName;
                    model.EmployeeNumber = User.EmployeeNumber;
                    model.EmployeePicture = User.EmployeePicture;
                    model.EmployeeId = hrmDefUser.EmployeeId.Value;
                }
            }

            using (var response = await httpClient.GetAsync(QueryHelpers.AddQueryString("Employee/GetEmployeeContactInfo", queryParams)))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employeeContactInfo = JsonConvert.DeserializeObject<HrmTranContactInfo>(data);
                    HrmTranContactInfoModel hrmTranContactInfoModel = new HrmTranContactInfoModel
                    {
                        ContactInfoId = employeeContactInfo.ContactInfoId,
                        Email = employeeContactInfo.Email,
                        EmployeeId = employeeContactInfo.EmployeeId,
                        PrimaryMobileNumber = employeeContactInfo.PrimaryMobileNumber
                    };
                    model.EmployeeContactInfo = hrmTranContactInfoModel;
                }
            }

            using (var response = await httpClient.GetAsync(QueryHelpers.AddQueryString("Employee/GetEmployeeMapToContactInfo", queryParams)))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employeeContactInfo = JsonConvert.DeserializeObject<List<HrmTranMapToContactInfo>>(data);
                    List<HrmTranMapToContactInfoModel> hrmTranMapToContactInfoModels = new List<HrmTranMapToContactInfoModel>();
                    employeeContactInfo.ForEach(item =>
                    {
                        HrmTranMapToContactInfoModel hrmTranMapToContactInfoModel = new HrmTranMapToContactInfoModel
                        {
                            ConatactNumber = item.ConatactNumber,
                            ContactAddress = item.ContactAddress,
                            ContactInfoId = item.ContactInfoId,
                            ContactPersonName = item.ContactPersonName,
                            EmployeeId = item.EmployeeId,
                            IsEmergency = item.IsEmergency,
                            IsFamily = item.IsFamily,
                            MapContactInfoId = item.MapContactInfoId,
                            Remarks = item.Remarks,
                        };
                        hrmTranMapToContactInfoModels.Add(hrmTranMapToContactInfoModel);
                    });
                    model.MapToContactInfoList = hrmTranMapToContactInfoModels;
                }
            }
            return View("~/Views/Employee/GetEmployeeContactInfo.cshtml", model);
        }


        [HttpPost]
        public async Task<IActionResult> UpdateEmployeeContactInfo(HrmTranContactInfoBaseModel model)
        {
            if (ModelState.IsValid)
            {
                var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");

                UserModel User = null;
                if (HttpContext.Session.GetString("User") != "")
                    User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

                HrmTranContactInfoModel hrmTranContactInfoModel = new HrmTranContactInfoModel();
                hrmTranContactInfoModel = model.EmployeeContactInfo;

                HrmTranContactInfo hrmTranContactInfo = new HrmTranContactInfo
                {
                    ContactInfoId = hrmTranContactInfoModel.ContactInfoId,
                    PrimaryMobileNumber = hrmTranContactInfoModel.PrimaryMobileNumber,
                    Email = hrmTranContactInfoModel.Email,
                    EmployeeId = hrmTranContactInfoModel.EmployeeId,
                    CreatedBy = User.UserId,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = User.UserId,
                    ModifiedDate = DateTime.Now
                };

                StringContent content = new StringContent(JsonConvert.SerializeObject(hrmTranContactInfo), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("Employee/SaveOrUpdateContactInfo", content))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        int status = JsonConvert.DeserializeObject<int>(data);
                        if (status > 0)
                        {
                            MailRequestModel mailRequest = new MailRequestModel();
                            mailRequest.To = Convert.ToString(_configuration["EmailConfig:To"]);
                            mailRequest.Subject = User.UserName + " has updated his contact details.";
                            mailRequest.Body = "<h1>This is to inform that " + User.UserName + " has updated his contact details.</h1>";
                            await _emailService.SendEmailAsync(mailRequest);
                            TempData["SuccessMessage"] = "Successfully Updated !";
                        }
                        else
                            TempData["ErroMessage"] = "Record not Updated !";
                    }
                    else
                        TempData["ErroMessage"] = "Record not Updated !";
                }
            }
            return RedirectToAction("GetEmployeeContactInfo");
        }

        [HttpGet]
        public async Task<IActionResult> EditEmployeeMapContactInfo(long mapContactInfoID, long contactinfoId, long employeeId)
        {
            HrmTranMapToContactInfoModel model = new HrmTranMapToContactInfoModel();
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            string employeeNumber = string.Empty;
            #region EmployeeNumber

            UserModel User = null;
            if (HttpContext.Session.GetString("User") != "")
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

            #endregion

            if (mapContactInfoID > 0)
            {

                var queryParams = new Dictionary<string, string>() {
                {"employeeID",employeeId.ToString() },
                {"MapContactInfoId",mapContactInfoID.ToString()}
                };
                using (var response = await httpClient.GetAsync(QueryHelpers.AddQueryString("Employee/GetEmployeeMapToContactInfoByID", queryParams)))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        var empMapToContactInfo = JsonConvert.DeserializeObject<HrmTranMapToContactInfo>(data);
                        if (empMapToContactInfo != null)
                        {
                            model.ConatactNumber = empMapToContactInfo.ConatactNumber;
                            model.ContactAddress = empMapToContactInfo.ContactAddress;
                            model.ContactInfoId = empMapToContactInfo.ContactInfoId;
                            model.ContactPersonName = empMapToContactInfo.ContactPersonName;
                            model.EmployeeId = empMapToContactInfo.EmployeeId;
                            model.EmployeeNumber = string.Empty;
                            model.IsEmergency = empMapToContactInfo.IsEmergency;
                            model.IsFamily = empMapToContactInfo.IsFamily;
                            model.MapContactInfoId = empMapToContactInfo.MapContactInfoId;
                            model.Remarks = empMapToContactInfo.Remarks;
                            model.EmployeeNumber = User.EmployeeNumber;
                        }
                    }
                }
            }
            else
            {
                model.EmployeeId = employeeId;
                model.ContactInfoId = contactinfoId;
                model.MapContactInfoId = mapContactInfoID;
                model.EmployeeNumber = User.EmployeeNumber;
            }
            return PartialView("~/Views/Employee/_AddEditContactInfo.cshtml", model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateMapToContactInfo(HrmTranMapToContactInfoModel model)
        {
            if (ModelState.IsValid)
            {
                var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");

                UserModel User = null;
                if (HttpContext.Session.GetString("User") != "")
                    User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

                HrmTranMapToContactInfo hrmTranMapToContactInfo = new HrmTranMapToContactInfo
                {
                    ContactPersonName = model.ContactPersonName,
                    ConatactNumber = model.ConatactNumber,
                    EmployeeId = model.EmployeeId,
                    IsFamily = model.IsFamily,
                    IsEmergency = model.IsEmergency,
                    ContactAddress = model.ContactAddress,
                    Remarks = model.Remarks,
                    ContactInfoId = model.ContactInfoId,
                    MapContactInfoId = model.MapContactInfoId
                };

                StringContent content = new StringContent(JsonConvert.SerializeObject(hrmTranMapToContactInfo), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("Employee/SaveOrUpdateMapToContactInfo", content))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        int status = JsonConvert.DeserializeObject<int>(data);
                        if (status > 0)
                        {
                            MailRequestModel mailRequest = new MailRequestModel();
                            mailRequest.To = Convert.ToString(_configuration["EmailConfig:To"]);
                            mailRequest.Subject = User.UserName + " has updated his contact details.";
                            mailRequest.Body = "<h1>This is to inform that " + User.UserName + " has updated his contact details.</h1>";
                            await _emailService.SendEmailAsync(mailRequest);
                            TempData["SuccessMessage"] = "Successfully Updated !";
                        }
                        else
                            TempData["ErroMessage"] = "Record not Updated !";
                    }
                    else
                        TempData["ErroMessage"] = "Record not Updated !";
                }
            }
            return RedirectToAction("GetEmployeeContactInfo");
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> GetEmployeeAddress()
        {
            UserModel User = null;
            HrmTranEmployeeAddressBaseModel model = new HrmTranEmployeeAddressBaseModel();
            var language = HttpContext.Session.GetString("CurrentCulture") ?? "en";
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");

            if (HttpContext.Session.GetString("User") != "")
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

            var hrmDefUser = new HrmDefUser() { UserId = User.UserId, RoleId = User.RoleId, EmployeeId = User.EmployeeID };

            #region Employee Name & Employee Number

            var queryParams = new Dictionary<string, string>() {
                {"employeeID",hrmDefUser.EmployeeId.ToString() }
                };
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeByID", queryParams);
            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employee = JsonConvert.DeserializeObject<HrmTranEmployee>(data);
                    model.EmployeeName = employee.EmployeeName;
                    model.EmployeeNumber = User.EmployeeNumber;
                    model.EmployeePicture = User.EmployeePicture;
                    model.EmployeeId = Convert.ToInt64(hrmDefUser.EmployeeId);
                }
            }

            #endregion

            var EmployeeAddresses = new List<HrmTranEmployeeAddressModel>();
            var employeeAddressParams = new Dictionary<string, string>() {
                {"employeeID",hrmDefUser.EmployeeId.ToString() },
                {"language",language }
                };
            string employeeRequestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeAddressList", employeeAddressParams);
            using (var response = await httpClient.GetAsync(employeeRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    EmployeeAddresses = JsonConvert.DeserializeObject<List<HrmTranEmployeeAddressModel>>(data);
                }
            }
            model.EmployeeAddressList = EmployeeAddresses;

            return View("~/Views/Employee/GetEmployeeAddress.cshtml", model);
        }

        [HttpGet]
        public async Task<IActionResult> GetEmployeeEducationInfo()
        {
            UserModel User = null;
            HrmTranEducationInfoBaseModel model = new HrmTranEducationInfoBaseModel();
            var language = HttpContext.Session.GetString("CurrentCulture") == "en" ? "1" : "2";
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            if (HttpContext.Session.GetString("User") != "")
            {
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));
            }
            #region Employee Name & Employee Number
            var queryParams = new Dictionary<string, string>() {
                {"employeeID",User.EmployeeID.ToString() }
                };
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeByID", queryParams);
            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employee = JsonConvert.DeserializeObject<HrmTranEmployee>(data);
                    model.EmployeeId = User.EmployeeID;
                    model.EmployeeName = employee.EmployeeName;
                    model.EmployeeNumber = User.EmployeeNumber;
                    model.EmployeePicture = User.EmployeePicture;
                }
            }
            #endregion


            var employeeEducationList = new List<EducationInfoModel>();
            var employeeQueryParams = new Dictionary<string, string>() {
                {"employeeID",User.EmployeeID.ToString() },{"languageID",language }
                };
            string employeeRequestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeEducationList", employeeQueryParams);
            using (var response = await httpClient.GetAsync(employeeRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    employeeEducationList = JsonConvert.DeserializeObject<List<EducationInfoModel>>(data);
                }
            }
            model.EmployeeEducationList = employeeEducationList;
            return View("~/Views/Employee/GetEmployeeEducationInfo.cshtml", model);
        }

        #region EmployeeDocumentInfo

        [HttpGet]
        public async Task<IActionResult> GetEmployeeDocumentsInfo()
        {
            UserModel User = null;
            HrmTranDocumentsInfoBaseModel model = new HrmTranDocumentsInfoBaseModel();
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");

            if (HttpContext.Session.GetString("User") != "")
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

            var hrmDefUser = new HrmDefUser() { UserId = User.UserId, RoleId = User.RoleId, EmployeeId = User.EmployeeID };

            #region Employee Name & Employee Number

            var queryParams = new Dictionary<string, string>() {
                {"employeeID",hrmDefUser.EmployeeId.ToString() }
                };
            string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeByID", queryParams);
            using (var response = await httpClient.GetAsync(requestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employee = JsonConvert.DeserializeObject<HrmTranEmployee>(data);
                    model.EmployeeName = employee.EmployeeName;
                    model.EmployeeNumber = User.EmployeeNumber;
                    model.EmployeePicture = User.EmployeePicture;
                    model.EmployeeId = Convert.ToInt64(hrmDefUser.EmployeeId);
                }
            }

            #endregion

            using (var response = await httpClient.GetAsync(QueryHelpers.AddQueryString("Employee/GetEmployeeDocumentsInfo", queryParams)))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var employeeDocumentsInfo = JsonConvert.DeserializeObject<List<HrmTranDocumentsInfo>>(data);

                    var documentTypes = await GetAllDocumentTypes();
                    var documentOwners = await GetAllDocumentOwners();

                    var list = (from emp in employeeDocumentsInfo
                                join doc in documentTypes on emp.DocumentTypeId equals doc.DocumentTypeId
                                join own in documentOwners on emp.DocumentOwnerId equals own.DocumentOwnerId
                                select new HrmTranDocumentsInfoModel
                                {
                                    EmployeeId = emp.EmployeeId,
                                    DocumentId = emp.DocumentId,
                                    DocumentTypeId = emp.DocumentTypeId,
                                    DocumentTypeNameEn = doc.DocumentTypeNameEn,
                                    DocumentTypeNameAr = doc.DocumentTypeNameAr,
                                    DocumentNumber = emp.DocumentNumber,
                                    NameInDocument = emp.NameInDocument,
                                    ExpiryDateAr = emp.ExpiryDateAr,
                                    ExpiryDateEn = emp.ExpiryDateEn,
                                    DocumentOwnerId = emp.DocumentOwnerId,
                                    DocumentOwnerNameEn = own.DocumentOwnerNameEn,
                                    DocumentOwnerNameAr = own.DocumentOwnerNameAr
                                }).ToList();

                    model.EmployeeDocumentsList = list;
                }
            }

            return View("~/Views/Employee/GetEmployeeDocumentsInfo.cshtml", model);
        }

        [HttpGet]
        public async Task<IActionResult> EditEmployeeDocumentsInfo(long employeeID, long documentID)
        {
            HrmTranDocumentsInfoModel model = new HrmTranDocumentsInfoModel();
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            string employeeNumber = "";

            #region EmployeeNumber

            UserModel User = null;
            if (HttpContext.Session.GetString("User") != "")
                User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

            #endregion


            if (documentID > 0)
            {
                var queryParams = new Dictionary<string, string>() {
                {"employeeID",employeeID.ToString() },
                {"documentID",documentID.ToString()}
                };
                using (var response = await httpClient.GetAsync(QueryHelpers.AddQueryString("Employee/GetEmployeeDocumentsInfoByID", queryParams)))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        var employeeDocumentsInfoByID = JsonConvert.DeserializeObject<HrmTranDocumentsInfo>(data);
                        if (employeeDocumentsInfoByID != null)
                        {
                            model = new HrmTranDocumentsInfoModel
                            {
                                DocumentId = employeeDocumentsInfoByID.DocumentId,
                                DocumentTypeId = employeeDocumentsInfoByID.DocumentTypeId,
                                DocumentNumber = employeeDocumentsInfoByID.DocumentNumber,
                                EmployeeId = employeeDocumentsInfoByID.EmployeeId,
                                NameInDocument = employeeDocumentsInfoByID.NameInDocument,
                                IssueLocation = employeeDocumentsInfoByID.IssueLocation,
                                DocumentOwnerId = employeeDocumentsInfoByID.DocumentOwnerId,
                                ProfessionInIqama = employeeDocumentsInfoByID.ProfessionInIqama,
                                InsuranceTypeId = employeeDocumentsInfoByID.InsuranceTypeId,
                                InsuranceProviderId = employeeDocumentsInfoByID.InsuranceProviderId,
                                InsuranceClassId = employeeDocumentsInfoByID.InsuranceClassId,
                                InsurancePolicyNo = employeeDocumentsInfoByID.InsurancePolicyNo,
                                IssueDateEn = employeeDocumentsInfoByID.IssueDateEn,
                                IssueDateAr = employeeDocumentsInfoByID.IssueDateAr,
                                ExpiryDateEn = employeeDocumentsInfoByID.ExpiryDateEn,
                                ExpiryDateAr = employeeDocumentsInfoByID.ExpiryDateAr,
                                Costing = employeeDocumentsInfoByID.Costing,
                                DocumentStatus = employeeDocumentsInfoByID.DocumentStatus,
                                ScannedDocument = employeeDocumentsInfoByID.ScannedDocument,
                                Remarks = employeeDocumentsInfoByID.Remarks,
                                IsActive = employeeDocumentsInfoByID.IsActive,
                                ShortScannedDocument = (!string.IsNullOrEmpty(employeeDocumentsInfoByID.ScannedDocument) ? employeeDocumentsInfoByID.ScannedDocument.Split('_')[1] : ""),
                                EmployeeNumber = User.EmployeeNumber
                            };
                        }
                    }
                }
            }
            else
                model = new HrmTranDocumentsInfoModel { EmployeeId = employeeID, EmployeeNumber = User.EmployeeNumber };

            model.DocumentTypes = await GetAllDocumentTypes();
            model.DocumentOwners = await GetAllDocumentOwners();
            return PartialView("~/Views/Employee/_AddUpdateDocuments.cshtml", model);
        }

        public async Task<List<HrmDefDocumentType>> GetAllDocumentTypes()
        {
            List<HrmDefDocumentType> documentTypes = new List<HrmDefDocumentType>();
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            using (var response = await httpClient.GetAsync("Common/GetAllDocumentTypes"))
            {
                var data = await response.Content.ReadAsStringAsync();
                documentTypes = JsonConvert.DeserializeObject<List<HrmDefDocumentType>>(data);
            }
            return documentTypes;
        }

        public async Task<List<HrmSysDocumentOwner>> GetAllDocumentOwners()
        {
            List<HrmSysDocumentOwner> documentOwners = new List<HrmSysDocumentOwner>();
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            using (var response = await httpClient.GetAsync("Common/GetAllDocumentOwners"))
            {
                var data = await response.Content.ReadAsStringAsync();
                documentOwners = JsonConvert.DeserializeObject<List<HrmSysDocumentOwner>>(data);
            }
            return documentOwners;
        }

        [HttpPost]
        public async Task<IActionResult> UpdateDocumentInfo(HrmTranDocumentsInfoModel model)
        {
            if (ModelState.IsValid)
            {
                var file = model.ImageUpload;
                if (file != null)
                {
                    Random number = new Random(1);
                    string fileName = Path.GetFileName(Guid.NewGuid().ToString("N") + "_" + number.Next(999) + file.FileName);
                    string uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "File/Documents/");
                    if (!Directory.Exists(uploadFolder))
                    {
                        Directory.CreateDirectory(uploadFolder);
                    }
                    string path = Path.Combine(uploadFolder, fileName);

                    using (Stream fileStream = new FileStream(path, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                    model.ScannedDocument = fileName;
                }
                var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");

                UserModel User = null;
                if (HttpContext.Session.GetString("User") != "")
                    User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));

                HrmTranDocumentsInfo hrmTranMapToContactInfo = new HrmTranDocumentsInfo
                {
                    DocumentTypeId = model.DocumentTypeId,
                    DocumentNumber = model.DocumentNumber,
                    NameInDocument = model.NameInDocument,
                    IssueLocation = model.IssueLocation,
                    DocumentOwnerId = model.DocumentOwnerId,
                    ProfessionInIqama = model.ProfessionInIqama,
                    InsuranceTypeId = model.InsuranceTypeId,
                    InsuranceProviderId = model.InsuranceProviderId,
                    InsuranceClassId = model.InsuranceClassId,
                    InsurancePolicyNo = model.InsurancePolicyNo,
                    IssueDateEn = model.IssueDateEn,
                    IssueDateAr = model.IssueDateAr,
                    ExpiryDateEn = model.ExpiryDateEn,
                    ExpiryDateAr = model.ExpiryDateAr,
                    Costing = model.Costing,
                    DocumentStatus = model.DocumentStatus,
                    ScannedDocument = model.ScannedDocument,
                    Remarks = model.Remarks,
                    CreatedBy = User.UserId,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = User.UserId,
                    ModifiedDate = DateTime.Now,
                    EmployeeId = User.EmployeeID,
                    DocumentId = model.DocumentId
                };

                StringContent content = new StringContent(JsonConvert.SerializeObject(hrmTranMapToContactInfo), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("Employee/SaveOrUpdateEmployeeDocumentsInfo", content))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        int status = JsonConvert.DeserializeObject<int>(data);
                        if (status > 0)
                        {
                            MailRequestModel mailRequest = new MailRequestModel();
                            mailRequest.To = Convert.ToString(_configuration["EmailConfig:To"]);
                            mailRequest.Subject = User.UserName + " has updated his document details.";
                            mailRequest.Body = "<h1>This is to inform that " + User.UserName + " has updated his document details.</h1>";
                            await _emailService.SendEmailAsync(mailRequest);
                            TempData["SuccessMessage"] = "Successfully Updated !";
                        }
                        else
                            TempData["ErroMessage"] = "Record not Updated !";
                    }
                    else
                        TempData["ErroMessage"] = "Record not Updated !";
                }
            }
            return RedirectToAction("GetEmployeeDocumentsInfo");
        }
        #endregion
        [HttpPost]
        public async Task<IActionResult> EditEducationInfo(long employeeID, long educationId)
        {
            EducationInfoModel model = new EducationInfoModel();
            var language = HttpContext.Session.GetString("CurrentCulture") == "en" ? "1" : "2";
            model.EmployeeId = employeeID;
            model.EducationId = educationId;
            var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
            if (educationId > 0)
            {
                var queryParams = new Dictionary<string, string>() {
                {"educationId",educationId.ToString() },{"languageID",language }
                };
                string requestUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeEducationByID", queryParams);
                using (var response = await httpClient.GetAsync(requestUrl))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        model = JsonConvert.DeserializeObject<EducationInfoModel>(data);
                    }
                }
            }
            var degreeQueryParams = new Dictionary<string, string>() {
                {"languageID",language}
                };
            string degreeRequestUrl = QueryHelpers.AddQueryString("Common/GetAllDegrees", degreeQueryParams);
            using (var response = await httpClient.GetAsync(degreeRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    model.DegreeDropDown = JsonConvert.DeserializeObject<List<DropDownDTO>>(data);
                }
            }

            var courseTypeQueryParams = new Dictionary<string, string>() {
                {"languageID",language}
                };
            string courseTypeRequestUrl = QueryHelpers.AddQueryString("Common/GetAllCourseTypes", courseTypeQueryParams);
            using (var response = await httpClient.GetAsync(courseTypeRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    model.CourseTypeDropDown = JsonConvert.DeserializeObject<List<DropDownDTO>>(data);
                }
            }

            var countryQueryParams = new Dictionary<string, string>() {
                {"languageID",language}
                };
            string countryRequestUrl = QueryHelpers.AddQueryString("Common/GetAllCountries", countryQueryParams);
            using (var response = await httpClient.GetAsync(countryRequestUrl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    model.CountryDropDown = JsonConvert.DeserializeObject<List<DropDownDTO>>(data);
                }
            }
            return PartialView("_AddUpdateEducationInfo", model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateEmployeeEducationInfo(EducationInfoModel model)
        {
            if (ModelState.IsValid)
            {
                var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
                UserModel User = null;
                if (HttpContext.Session.GetString("User") != "")
                    User = JsonConvert.DeserializeObject<UserModel>(HttpContext.Session.GetString("User"));
                StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("Employee/UpdateEmployeeEducationInfo", content))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        int status = JsonConvert.DeserializeObject<int>(data);
                        if (status == 1)
                        {
                            MailRequestModel mailRequest = new MailRequestModel();
                            mailRequest.To = Convert.ToString(_configuration["EmailConfig:To"]);
                            mailRequest.Subject = User.UserName + " has updated his education details.";
                            mailRequest.Body = "<h1>This is to inform that " + User.UserName + " has updated his education details.</h1>";
                            await _emailService.SendEmailAsync(mailRequest);

                            TempData["SuccessMessage"] = "Successfully Updated !";
                        }
                        else
                            TempData["ErroMessage"] = "Record not Updated !";
                    }
                    else
                        TempData["ErroMessage"] = "Record not Updated !";
                }
            }
            return RedirectToAction("GetEmployeeEducationInfo");
        }
    }
}
