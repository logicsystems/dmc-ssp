﻿using DMC.HRM.Entities.Models;
using DMC.HRM.UI.Common;
using DMC.HRM.UI.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpClientFactory _httpClientFactory;

        public HomeController(ILogger<HomeController> logger, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            HrmDefUserModel hrmDefUser = new HrmDefUserModel();
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("CurrentCulture")))
            {
                hrmDefUser.ddlCulture = HttpContext.Session.GetString("CurrentCulture");
            }
            return View(hrmDefUser);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Index")]
        public async Task<IActionResult> Index(HrmDefUserModel hrmDefUserModel)
        {
            if (ModelState.IsValid)
            {
                var httpClient = _httpClientFactory.CreateClient("DMCHRMApi");
                var queryParams = new Dictionary<string, string>() {
                    { "UserName", hrmDefUserModel.UserName },
                    { "UserPassword", hrmDefUserModel.UserPassword }
                };
                string requestUrl = QueryHelpers.AddQueryString("Account/ValidateUser", queryParams);
                var response = await httpClient.GetAsync(requestUrl);
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var model = JsonConvert.DeserializeObject<HrmDefUserModel>(data);

                    var user = JsonConvert.DeserializeObject<UserModel>(data);

                    //get Employee Picture
                    using (var personalInfo = await httpClient.PostAsync("Employee/GetPersonalInfo", response.Content))
                    {
                        if (personalInfo.IsSuccessStatusCode)
                        {
                            var personalData = await personalInfo.Content.ReadAsStringAsync();
                            HrmTranPersonalInfoModel hrmTranPersonalInfoModel = JsonConvert.DeserializeObject<HrmTranPersonalInfoModel>(personalData);
                            user.EmployeePicture = hrmTranPersonalInfoModel.EmployeePicture;
                        }
                    }

                    var getEmployeeParams = new Dictionary<string, string>() {
                        {"employeeID",model.EmployeeId.ToString() }
                    };
                    string getEmployeeUrl = QueryHelpers.AddQueryString("Employee/GetEmployeeByID", getEmployeeParams);

                    //get Employee Number
                    using (var getEmployeeResponse = await httpClient.GetAsync(getEmployeeUrl))
                    {
                        if (getEmployeeResponse.IsSuccessStatusCode)
                        {
                            var employeeData = await getEmployeeResponse.Content.ReadAsStringAsync();
                            var employee = JsonConvert.DeserializeObject<HrmTranEmployee>(employeeData);
                            user.EmployeeNumber = employee.EmployeeNumber;
                        }
                    }
                    HttpContext.Session.SetString("User", JsonConvert.SerializeObject(user));
                    return RedirectToAction("DashBoard", "Home");
                }
                else
                    return View();
            }
            else
                return View();
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            HttpContext.Session.Clear();
            HrmDefUserModel model = new HrmDefUserModel();
            return View("Index", model);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        [Route("Index")]
        public async Task<IActionResult> DashBoard()
        {
            return View();
        }
    }
}
