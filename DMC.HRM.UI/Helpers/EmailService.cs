﻿using DMC.HRM.UI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Helpers
{
    public class EmailService: IEmailService
    {
        private IConfiguration _configuration;

        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task SendEmailAsync(MailRequestModel mailRequest)
        {
            string SMTPServer = Convert.ToString(_configuration["EmailConfig:SMTPServer"]);
            int SMTPPort = Convert.ToInt32(_configuration["EmailConfig:SMTPPort"]);
            string EmailAddress = Convert.ToString(_configuration["EmailConfig:EmailAddress"]);
            string Password = Convert.ToString(_configuration["EmailConfig:Password"]);
            bool SSLEnabled = Convert.ToBoolean(_configuration["EmailConfig:SSLEnabled"]);

            if (!string.IsNullOrEmpty(mailRequest.To) || !string.IsNullOrEmpty(mailRequest.Cc) || !string.IsNullOrEmpty(mailRequest.BCc))
            {
                MailMessage newmsg = new MailMessage();
                MailAddress mailfrom = new MailAddress(EmailAddress);
                if (!string.IsNullOrWhiteSpace(mailRequest.To))
                {
                    mailRequest.To = mailRequest.To.Replace("<br>", ",");
                    if (mailRequest.To.EndsWith(","))
                        mailRequest.To = mailRequest.To.Remove(mailRequest.To.Length - 1);
                    string[] _toAddress = mailRequest.To.Split(',');
                    for (int i = 0; i < _toAddress.Length; i++)
                    {
                        newmsg.To.Add(new MailAddress(_toAddress[i]));
                    }
                }
                if (!string.IsNullOrWhiteSpace(mailRequest.Cc))
                {
                    MailAddress Cc = new MailAddress(mailRequest.Cc);
                    newmsg.CC.Add(Cc);
                }
                if (!string.IsNullOrWhiteSpace(mailRequest.BCc))
                {
                    string[] bccID = mailRequest.BCc.Split(',');
                    foreach (string bcmail in bccID)
                    {
                        if (bcmail != "")
                        {
                            MailAddress bcc = new MailAddress(bcmail);
                            newmsg.Bcc.Add(bcc);
                        }
                    }
                }
                newmsg.From = mailfrom;
                newmsg.Subject = mailRequest.Subject;
                newmsg.Body = mailRequest.Body;
                newmsg.IsBodyHtml = true;
                using (SmtpClient smtp = new SmtpClient(SMTPServer, SMTPPort))
                {
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential(EmailAddress, Password);
                    smtp.EnableSsl = SSLEnabled;
                    await smtp.SendMailAsync(newmsg);
                }
            }
        }
    }
}
