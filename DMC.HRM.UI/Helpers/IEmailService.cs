﻿using DMC.HRM.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Helpers
{
    public interface IEmailService
    {
        Task SendEmailAsync(MailRequestModel mailRequest);
    }
}
