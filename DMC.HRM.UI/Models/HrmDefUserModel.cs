﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class HrmDefUserModel
    {
        public long UserId { get; set; }
        public string ProfileName { get; set; }
        public string UserDescription { get; set; }
        public string ProfilePicture { get; set; }
        public int? UserType { get; set; }
        public long? RoleId { get; set; }
        public long? EmployeeId { get; set; }
        public int? CompanyId { get; set; }
        [Required(ErrorMessage = "* Required")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "* Required")]
        public string UserPassword { get; set; }
        public DateTime? PasswordExpirationDate { get; set; }
        public int? IncorrectPasswordAttempts { get; set; }
        public bool? IsActive { get; set; }
        public int? IsSystem { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileNameAr { get; set; }
        public string ddlCulture { get; set; } = "en";
        public string ErrorMessage { get; set; }
    }
}
