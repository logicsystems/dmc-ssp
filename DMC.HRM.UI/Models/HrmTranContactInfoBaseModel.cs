﻿using DMC.HRM.Entities.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class HrmTranContactInfoBaseModel : EmployeeInfoModel
    {
        public HrmTranContactInfoModel EmployeeContactInfo { get; set; }
        public List<HrmTranMapToContactInfoModel> MapToContactInfoList { get; set; }
    }
}
