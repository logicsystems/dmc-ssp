﻿using DMC.HRM.Entities.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class HrmTranDocumentsInfoModel
    {
        public int SNo { get; set; }
        public string EmployeeNumber { get; set; }
        public long DocumentId { get; set; }
        [Required(ErrorMessage = "*Required")]
        public long? DocumentTypeId { get; set; }
        public string DocumentTypeNameEn { get; set; }
        public string DocumentTypeNameAr { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string DocumentNumber { get; set; }
        public long? EmployeeId { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string NameInDocument { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string IssueLocation { get; set; }
        [Required(ErrorMessage = "*Required")]
        public long? DocumentOwnerId { get; set; }
        public string DocumentOwnerNameEn { get; set; }
        public string DocumentOwnerNameAr { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string ProfessionInIqama { get; set; }
        public long? InsuranceTypeId { get; set; }
        public long? InsuranceProviderId { get; set; }
        public long? InsuranceClassId { get; set; }
        public string InsurancePolicyNo { get; set; }
        [Required(ErrorMessage = "*Required")]
        public DateTime? IssueDateEn { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string IssueDateAr { get; set; }
        [Required(ErrorMessage = "*Required")]
        public DateTime? ExpiryDateEn { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string ExpiryDateAr { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string Costing { get; set; }
        public string DocumentStatus { get; set; }
        public string ScannedDocument { get; set; }
        public string Remarks { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ShortScannedDocument { get; set; }
        public IFormFile ImageUpload { get; set; }
        public List<HrmDefDocumentType> DocumentTypes { get; set; }
        public List<HrmSysDocumentOwner> DocumentOwners { get; set; }
    }
}
