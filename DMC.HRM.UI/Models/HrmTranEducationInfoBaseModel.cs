﻿using DMC.HRM.Entities.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class HrmTranEducationInfoBaseModel: EmployeeInfoModel
    {
        public List<EducationInfoModel> EmployeeEducationList { get; set; }
    }
}
