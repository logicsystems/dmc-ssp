﻿using DMC.HRM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class HrmTranEducationInfoModel
    {
        public int SNo { get; set; }
        public long EducationId { get; set; }
        public long? EmployeeId { get; set; }
        public long? CurrentDegreeId { get; set; }
        public DateTime? YearOfCertification { get; set; }
        public long? DegreeId { get; set; }
        public string DegreeNameEn { get; set; }
        public string DegreeNameAr { get; set; }
        public string Qualification { get; set; }
        public string YearOfCertificationAr { get; set; }
        public DateTime? YearOfCertificationEn { get; set; }
        public string CollegeOrUniversity { get; set; }
        public long? CountryId { get; set; }
        public string CountryNameEn { get; set; }
        public string CountryNameAr { get; set; }
        public long? CourseTypeId { get; set; }
        public string CourseTypeNameEn { get; set; }
        public string CourseTypeNameAr { get; set; }
        public string Remarks { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public List<HrmSysEducationDegree> EducationDegrees { get; set; }
        public List<HrmSysCourseType> CourseTypes { get; set; }
        public List<HrmSysCountry> Countries { get; set; }
    }
}
