﻿using DMC.HRM.Entities.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class HrmTranEmployeeAddressBaseModel : EmployeeInfoModel
    {
        public List<HrmTranEmployeeAddressModel> EmployeeAddressList { get; set; }
    }
}
