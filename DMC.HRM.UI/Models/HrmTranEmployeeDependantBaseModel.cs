﻿using DMC.HRM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class HrmTranEmployeeDependantBaseModel
    {
        public EmployeeInfoModel Employee { get; set; }
        public List<HrmTranEmployeeDependantModel> EmployeeDependants { get; set; }
    }
}
