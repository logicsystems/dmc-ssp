﻿using DMC.HRM.Entities.Models;
using DMC.HRM.Entities.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class HrmTranEmployeePhoneBaseModel : EmployeeInfoModel
    {
        public List<HrmTranEmployeePhoneModel> EmployeePhones { get; set; }
    }
}
