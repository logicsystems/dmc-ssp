﻿using DMC.HRM.Entities.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class HrmTranPersonalInfoModel
    {
        public HrmTranPersonalInfoModel()
        {
            Employee = new EmployeeInfoModel();
            Genders = new List<HrmSysGender>();
            Nationalities = new List<HrmSysNationality>();
            PersonTypes = new List<HrmDefPersonType>();
            Religions = new List<HrmSysReligion>();
            Titiles = new List<HrmSysTitle>();
            BloodGroups = new List<HrmSysBloodGroup>();
            MaritalStatuses = new List<HrmSysMaritalStatus>();
        }
        public long PersonalInfoId { get; set; }
        public long? EmployeeId { get; set; }
        public long? GenderId { get; set; }
        public long? TitleId { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string FirstNameEn { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string FirstNameAr { get; set; }
        public string MiddleNameEn { get; set; }
        public string MiddleNameAr { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string LastNameEn { get; set; }
        [Required(ErrorMessage = "*Required")]
        public string LastNameAr { get; set; }
        public string FullNameEn { get; set; }
        public string FullNameAr { get; set; }
        public long? PersonTypeId { get; set; }
        public long? MaritalStatusId { get; set; }
        [Required(ErrorMessage = "*Required")]
        public DateTime? DateOfBirthEn { get; set; }
        public string DateOfBirthAr { get; set; }
        public long? NationalityId { get; set; }
        public long? ReligionId { get; set; }
        public long? BloodGroupId { get; set; }
        public string EmployeePicture { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public IFormFile ImageUpload { get; set; }
        public EmployeeInfoModel Employee { get; set; }
        public List<HrmSysGender> Genders { get; set; }
        public List<HrmSysNationality> Nationalities { get; set; }
        public List<HrmDefPersonType> PersonTypes { get; set; }
        public List<HrmSysReligion> Religions { get; set; }
        public List<HrmSysTitle> Titiles { get; set; }
        public List<HrmSysBloodGroup> BloodGroups { get; set; }
        public List<HrmSysMaritalStatus> MaritalStatuses { get; set; }
    }
}
