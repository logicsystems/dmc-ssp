﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.UI.Models
{
    public class UserModel
    {
        public Int64 UserId { get; set; }
        public Int64 EmployeeID { get; set; }
        public Int64 RoleId { get; set; }
        public string UserName { get; set; }
        public string ProfileName { get; set; }
        public string ProfileNameAr { get; set; }
        public string ProfilePicture { get; set; }
        public string EmployeePicture { get; set; }
        public string EmployeeNumber { get; set; }
    }
}
