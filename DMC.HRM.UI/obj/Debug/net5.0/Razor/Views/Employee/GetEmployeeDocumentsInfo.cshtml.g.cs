#pragma checksum "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b02a9070a35728c7e2aca91ee6b83a9c928dc6e6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Employee_GetEmployeeDocumentsInfo), @"mvc.1.0.view", @"/Views/Employee/GetEmployeeDocumentsInfo.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\_ViewImports.cshtml"
using DMC.HRM.UI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\_ViewImports.cshtml"
using DMC.HRM.UI.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b02a9070a35728c7e2aca91ee6b83a9c928dc6e6", @"/Views/Employee/GetEmployeeDocumentsInfo.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"32eb57782877160fbf970e8743327b73ec1465b5", @"/Views/_ViewImports.cshtml")]
    public class Views_Employee_GetEmployeeDocumentsInfo : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<HrmTranDocumentsInfoBaseModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "hidden", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("EmployeeID"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-control"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
  
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("<section class=\"border-contenter-inner \">\r\n    <div class=\"border-contenter-new \">\r\n        <div class=\"employee-info-box\">\r\n            ");
#nullable restore
#line 8 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
       Write(await Html.PartialAsync("~/Views/Shared/Common/_BasicEmployeeInfo.cshtml", Model.Employee));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </div>\r\n        <div class=\"container-fluid\">\r\n            <div class=\"info-box-margin\">\r\n                <h4 class=\"h4-new\">");
#nullable restore
#line 12 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                              Write(ViewResources.Resource.DocumentsInformation);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h4>\r\n                <div class=\"new-margin\">\r\n                    <p class=\"pull-right buton-h1\"><button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\"");
            BeginWriteAttribute("onclick", " onclick=\"", 661, "\"", 721, 3);
            WriteAttributeValue("", 671, "AddEditDocumentsInfo(", 671, 21, true);
#nullable restore
#line 14 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
WriteAttributeValue("", 692, Model.Employee.EmployeeId, 692, 26, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 718, ",0)", 718, 3, true);
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 14 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                                                                                                                                                                          Write(ViewResources.Resource.AddDocuments);

#line default
#line hidden
#nullable disable
            WriteLiteral("</button></p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "b02a9070a35728c7e2aca91ee6b83a9c928dc6e66510", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#nullable restore
#line 18 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => Model.Employee.EmployeeId);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n        <table class=\"table  dfilter table-bordered \">\r\n            <thead>\r\n                <tr class=\"table-bg01\">\r\n                    <th>");
#nullable restore
#line 22 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                   Write(ViewResources.Resource.SNo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</th>\r\n                    <th>");
#nullable restore
#line 23 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                   Write(ViewResources.Resource.DocumentType);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </th>\r\n                    <th>");
#nullable restore
#line 24 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                   Write(ViewResources.Resource.DocumentNumber);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </th>\r\n                    <th>");
#nullable restore
#line 25 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                   Write(ViewResources.Resource.NameInDocument);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </th>\r\n                    <th>");
#nullable restore
#line 26 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                   Write(ViewResources.Resource.ExpiryDate);

#line default
#line hidden
#nullable disable
            WriteLiteral("</th>\r\n                    <th>");
#nullable restore
#line 27 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                   Write(ViewResources.Resource.DocumentOwner);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </th>\r\n                    <th>");
#nullable restore
#line 28 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                   Write(ViewResources.Resource.Actions);

#line default
#line hidden
#nullable disable
            WriteLiteral("</th>\r\n                </tr>\r\n            </thead>\r\n            <tbody>\r\n");
#nullable restore
#line 32 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                 foreach (var item in Model.EmployeeDocumentsList)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr class=\"bg-filter\">\r\n                        <td class=\"center-td1 box-td\">");
#nullable restore
#line 35 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                                                 Write(item.SNo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 36 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                       Write(item.DocumentTypeNameEn);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 37 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                       Write(item.DocumentNumber);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 38 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                       Write(item.NameInDocument);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 39 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                       Write(item.ExpiryDateEn);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 40 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                       Write(item.DocumentOwnerNameEn);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td class=\"center-td1 box-td\">\r\n                            <a title=\"Edit\"");
            BeginWriteAttribute("onclick", " onclick=\"", 2167, "\"", 2232, 5);
            WriteAttributeValue("", 2177, "AddEditDocumentsInfo(", 2177, 21, true);
#nullable restore
#line 42 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
WriteAttributeValue("", 2198, item.EmployeeId, 2198, 16, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2214, ",", 2214, 1, true);
#nullable restore
#line 42 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
WriteAttributeValue("", 2215, item.DocumentId, 2215, 16, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2231, ")", 2231, 1, true);
            EndWriteAttribute();
            WriteLiteral(" class=\"makeActive\"><i class=\"fa fa-pencil font-icon\" aria-hidden=\"true\"></i></a>\r\n                            <a title=\"Delete\"");
            BeginWriteAttribute("onclick", " onclick=\"", 2361, "\"", 2411, 3);
            WriteAttributeValue("", 2371, "detailMaptoContactInfo(", 2371, 23, true);
#nullable restore
#line 43 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
WriteAttributeValue("", 2394, item.EmployeeId, 2394, 16, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2410, ")", 2410, 1, true);
            EndWriteAttribute();
            WriteLiteral("><i class=\"fas fa-trash-alt\" aria-hidden=\"true\"></i></a>\r\n                        </td>\r\n                    </tr>\r\n");
#nullable restore
#line 46 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"            </tbody>
        </table>
    </div>
</section>
<div class=""modal fade modal-doc-pop"" id=""addEditModel"" tabindex=""-1"" role=""dialog"" aria-hidden=""true"" data-backdrop=""static"">
    <div id=""loadAddEditModel""></div>
</div>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(""#imgInp"").change(function () {
        readURL(this);
    });
    $(document).ready(function () {
        $("".makeActive"").click(function () {
            $(this).addClass(""active"");
        });

    });
    function AddEditDocumentsInfo(employeeID, documentID) {
        var url = '");
#nullable restore
#line 76 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
              Write(Url.Action("EditDocumentsInfo", "DocumentInfo"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"' + '?employeeID=' + employeeID + '&documentID=' + documentID;
        $.post(url, function (data) {
            if (data != null) {
                $(""#loadAddEditModel"").html('');
                $('#addEditModel').modal('show');
                $(""#loadAddEditModel"").html(data);
            }
        });
    }

        function AddEditDocumentsAuditInfo(employeeID) {
        var url = '");
#nullable restore
#line 87 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
              Write(Url.Action("GetDocumentsInfoAuditList", "DocumentInfo"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"' + '?employeeID=' + employeeID;
        $.post(url, function (data) {
            if (data != null) {
                $(""#loadAddEditModel"").html('');
                $('#addEditModel').modal('show');
                $(""#loadAddEditModel"").html(data);
            }
        });
    }
    function detailMaptoContactInfo(maptoContactinfoId) {
        var url = '");
#nullable restore
#line 97 "C:\Users\Admin\Desktop\HRM_DMC_SSP\DMC.HRM.UI\Views\Employee\GetEmployeeDocumentsInfo.cshtml"
              Write(Url.Action("DetailContactInfo", "ContactInfo"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"' + '?roleId=' + roleId;
        $.post(url, function (data) {
            if (data != null) {
                $(""#loaddetailsModel"").html('');
                $('#detailModel').modal('show');
                $(""#loaddetailsModel"").html(data);
            }
        });
    }

    function emptyEdit() {
        alert(""Sorry you can't update it!"");
        //$('#emptyEditModel').modal('show');
        //$(""#loadEmptyEditModel"").html(data);
    }
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<HrmTranDocumentsInfoBaseModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
