﻿
var cal1 = new Calendar(),
    cal2 = new Calendar(true, 0, false, true);
function datePicker(gnTxtId, hjTxtId, gnDivId, hjDivId, isOne) {
    
    date1 = document.getElementById(gnTxtId);
    date2 = document.getElementById(hjTxtId);
    cal1Mode = cal1.isHijriMode();
    cal2Mode = cal2.isHijriMode();
    document.getElementById(gnDivId).appendChild(cal1.getElement());
    document.getElementById(hjDivId).appendChild(cal2.getElement());
    date1.value = cal1.getDate().getDateString();
    date2.value = cal2.getDate().getDateString();

    if (isOne === '1') {
        cal1.callback = function () {
            if (cal1Mode !== cal1.isHijriMode()) {
                cal2.disableCallback(true);
                cal2.changeDateMode();
                cal2.disableCallback(false);
                cal1Mode = cal1.isHijriMode();
                cal2Mode = cal2.isHijriMode();
            }
            else {
                cal2.setTime(cal1.getTime());
                date1.value = cal1.getDate().getDateString();
                date2.value = cal2.getDate().getDateString();

            }
        };

        //if (cal1.isHidden())
        cal1.show();
        //else
        cal2.hide();
    }
    else {
        cal2.callback = function () {
            if (cal2Mode !== cal2.isHijriMode()) {
                cal1.disableCallback(true);
                cal1.changeDateMode();
                cal1.disableCallback(false);
                cal1Mode = cal1.isHijriMode();
                cal2Mode = cal2.isHijriMode();
            }
            else {
                cal1.setTime(cal2.getTime());
                date1.value = cal1.getDate().getDateString();
                date2.value = cal2.getDate().getDateString();

            }
            cal2.hide();
        };

        //if (cal2.isHidden())
        cal2.show();
        //else
        cal1.hide();
    }
}

//Need to implement  load date picker with current date 


var cal21 = new Calendar(),
    cal22 = new Calendar(true, 0, false, true);
function datePickerWithSelection(gnTxtId, hjTxtId, gnDivId, hjDivId, isOne) {
    
    date21 = document.getElementById(gnTxtId);
    date22 = document.getElementById(hjTxtId);
    cal1Mode = cal1.isHijriMode();
    cal2Mode = cal2.isHijriMode();
    document.getElementById(gnDivId).appendChild(cal1.getElement());
    document.getElementById(hjDivId).appendChild(cal2.getElement());
    date21.value = cal21.getDate().getDateString();
    date22.value = cal22.getDate().getDateString();
    setDateFields();
    if (isOne === '1') {
        cal21.callback = function () {
            if (cal1Mode !== cal21.isHijriMode()) {
                cal22.disableCallback(true);
                cal22.changeDateMode();
                cal22.disableCallback(false);
                cal1Mode = cal21.isHijriMode();
                cal2Mode = cal22.isHijriMode();
            }
            else {
                cal22.setTime(cal21.getTime());
                date21.value = cal1.getDate().getDateString();
                date22.value = cal2.getDate().getDateString();
            }
        };

        if (cal21.isHidden()) cal21.show();
        else cal21.hide();
    }
    else {
        cal22.callback = function () {
            if (cal2Mode !== cal22.isHijriMode()) {
                cal21.disableCallback(true);
                cal21.changeDateMode();
                cal21.disableCallback(false);
                cal1Mode = cal21.isHijriMode();
                cal2Mode = cal22.isHijriMode();
            }
            else {
                cal21.setTime(cal22.getTime());
                function setDateFields() {
                    date21.value = cal21.getDate().getDateString();
                    date22.value = cal22.getDate().getDateString();
                }

            }
            cal22.hide();
        };

        if (cal22.isHidden()) {
            cal22.show();
        }
        else {
            cal22.hide();
        }
    }
}

function datePickerForArabic(gnTxtId, hjTxtId, gnDivId, hjDivId, isOne) {
    date31 = document.getElementById(gnTxtId);
    date32 = document.getElementById(hjTxtId);
    var en_date = new Date(date31.value.replace(" AD", ""));
    var cal31 = new Calendar(false, 0, 0, false, en_date.getFullYear(), en_date.getMonth(), en_date.getDate()),
        cal32 = new Calendar(true, 0, false, true);
    cal32.setTime(cal31.getTime());
    date32.value = cal32.getDate().getDateString();
    //if (cal1.isHidden())
    cal31.hide();
    //else
    cal32.hide();

}

