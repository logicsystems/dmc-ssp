﻿

$(document).ready(function () {
    // DataTable
    var table = $('.dfilter').DataTable({
        scrollX: false,
       
        scrollCollapse: true,
        paging: true,
        fixedColumns: false
        //,dom: 'Bfrtip',
        //buttons: [
        //    'copy', 'csv', 'excel', 'pdf', 'print'
        //]
    });
    // Filter event handler
    $(table.table().container()).on('keyup', 'thead.filter input', function () {
        table
            .column($(this).data('index'))
            .search(this.value)
            .draw();
    });

    $('.dfilter thead.filter th').each(function (i) {
        var title = $('.dfilter thead th').eq($(this).index()).text();
        var select = $('<select data-index="' + i + '"><option value=""></option></select>')
        if (SelectedLanguage === 'en') {

            if (title != "Is Active" && title != "Is System" && title != "Edit") {
                $(this).html('<input type="text"   placeholder="Search" class="placeholder"  data-index="' + i + '" />');
            }

            else if (title == "Is Active") {
                $(this).html(select.append('<option value="True">' + "True" + '</option>'));
                $(this).html(select.append('<option value="False">' + "False" + '</option>'));
            }
            else if (title == "Is System") {
                $(this).html(select.append('<option value="1">' + "Yes" + '</option>'));
                $(this).html(select.append('<option value="0">' + "No" + '</option>'));
            }
            select.on('change', function () {
                
                var index = "6";
                if (title == "Is Active") {
                    index = "5";
                }
                table
                    .column(index)
                    .search(this.value)
                    .draw()

            });
        }
        else {

            if (title != "نشط" && title != "النظام" && title != "تعديل") {
                $(this).html('<input type="text"   placeholder="بحث" class="placeholder"  data-index="' + i + '" />');
            }

            else if (title == "نشط") {
                $(this).html(select.append('<option value="True">' + "نعم" + '</option>'));
                $(this).html(select.append('<option value="False">' + "لا" + '</option>'));
            }
            else if (title == "النظام") {
                $(this).html(select.append('<option value="1">' + "نعم" + '</option>'));
                $(this).html(select.append('<option value="0">' + "لا" + '</option>'));
            }
            select.on('change', function () {               
                var index = "3";
                if (title === "نشط") {
                    index = "2";
                }                
                table
                    .column($(this).data('index'))
                    //.column(index)
                    .search(this.value)
                    .draw()

            });
        }
    });
});

$(document).ready(function () {
    // Popup for Add/Edit/View
    $('#myModal').on('click', 'a.popup', function (e) {
        e.preventDefault();

        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#addEditViewPopupContainer').html(data);
            $('#addEditViewPopupModal').modal('show');
        });
    })
});


// Create the new tab
var $tabPane = $('<div />', {
    'id': 'control-sidebar-theme-demo-options-tab',
    'class': 'tab-pane active'
})

// Create the tab button
var $tabButton = $('<li />', { 'class': 'active' })
    .html('<a href=\'#control-sidebar-theme-demo-options-tab\' data-toggle=\'tab\'>'
    + '<i class="fa fa-wrench"></i>'
    + '</a>')

// Add the tab button to the right sidebar tabs
$('[href="#control-sidebar-home-tab"]')
    .parent()
    .before($tabButton)

/***************** Alert Messages ******************/
    window.setTimeout(function () {
        $(".alertMessage").fadeTo(3000, 0).slideUp(500, function () {
            $(this).remove();
        });
}, 500);
    /***************** Alert Messages ******************/

/***************** Timer Control ******************/
$(document).ready(function () {
    window.onbeforeunload = function () {
        $("#divLoading").show();
    };
    $(window).bind("load", function () {
        $("#divLoading").hide();
    });
    $(document).on({
        ajaxStart: function () { $("#divLoading").show(); },
        ajaxStop: function () {
            $("#divLoading").hide(); $('.datetimepicker4').datepicker();
            $('.datepicker').datepicker(); $(".datepickertime").datetimepicker({ format: 'dd/mm/yyyy hh:ii' });
        },
        ajaxError: function () {
            $("#divLoading").hide();
            //alert('error'); 
        }
    });
    $('.chznSelectSingle').chosen();
    $(".bootstrap-dialog").css("z-index", "9999999");
    $('.list-group-item').click(function (e) {
        $(this).find('span').toggleClass('glyphicon-warning-sign').toggleClass('glyphicon-pencil');
    });
});
// Set the date we're counting down to
var isCancel = false;
var isPopup = false;
var isRestart = false;
var isSessionExpired = false;
if (isSessionExpired == 'True') { window.location.href = '/Account/LogOut'; }
var momentOfTime = new Date(); // just for example, can be any other time
myTimeSpan = parseInt(60) * 60 * 1000; // 5 minutes in milliseconds 1000
momentOfTime.setTime(momentOfTime.getTime() + myTimeSpan);

var countDownDate = momentOfTime;

// Update the count down every 1 second
var x = setInterval(function () {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    //document.getElementById("demo").innerHTML = minutes + "m " + seconds + "s ";
    var clock = document.getElementById('clockdiv');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');
    minutesSpan.innerHTML = minutes;
    secondsSpan.innerHTML = seconds;

    // If the count down is over, write some text
    if (distance < (15 * 60 * 1000) && isCancel == false) {
        if (isPopup == false) {
            $(".bootstrap-dialog").css("z-index", "9999999");
            BootstrapDialog.show({
                title: 'Confirmation Alert',
                message: 'Do you want to extend sesssion',
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        isCancel = true;
                        dialog.close();
                    }
                }, {
                    label: 'Extend',
                    cssClass: 'btn-success',
                    action: function (dialog) {
                        location.reload(true);
                        dialog.close();
                    }
                }
                ]
            });
            isPopup = true;
        }       
    }
    if (distance < 0) {
        clock.style.display = 'none';
        if (isRestart == false) {
            isSessionExpired = true;
            BootstrapDialog.show({
                title: 'Confirmation Alert',
                message: 'Your sesssion is expired need to restart',
                buttons: [{
                    label: 'Restart',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        debugger;
                        window.location.href = '/Account/LogOut';
                        dialog.close();
                    }
                }]
            });
            isRestart = true;
        }
    }
    
}, 900);
window.onload = function () {
    $('.placeholder').each(function (ev) {
        if (SelectedLanguage === 'en') {
            $(this).attr("placeholder", "Search");
        }
        else {
            $(this).attr("placeholder", "بحث");
        }

    });
};
/***************** Timer Control ******************/