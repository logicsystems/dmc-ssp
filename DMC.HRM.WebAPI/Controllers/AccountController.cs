﻿using DMC.HRM.Repository.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private IAccountRepository _accountRepository;
        public AccountController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        [HttpGet]
        [Route("ValidateUser")]
        public async Task<IActionResult> ValidateUser(string UserName, string UserPassword)
        {
            var user = await _accountRepository.ValidateUser(UserName, UserPassword);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }
    }
}
