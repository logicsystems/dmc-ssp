﻿using DMC.HRM.Entities;
using DMC.HRM.Repository.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private ICommonRepository _commonRepository;
        public CommonController(ICommonRepository commonRepository)
        {
            _commonRepository = commonRepository;
        }

        [HttpGet]
        [Route("GetAllGenders")]
        public async Task<IActionResult> GetAllGenders()
        {
            var genders = await _commonRepository.GetAllGenders();
            if (genders != null)
                return Ok(genders);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetAllNationalities")]
        public async Task<IActionResult> GetAllNationalities()
        {
            var nationalities = await _commonRepository.GetAllNationalities();
            if (nationalities != null)
                return Ok(nationalities);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetAllPersonTypes")]
        public async Task<IActionResult> GetAllPersonTypes()
        {
            var personTypes = await _commonRepository.GetAllPersonTypes();
            if (personTypes != null)
                return Ok(personTypes);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetAllReligions")]
        public async Task<IActionResult> GetAllReligions()
        {
            var religions = await _commonRepository.GetAllReligions();
            if (religions != null)
                return Ok(religions);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetAllTitles")]
        public async Task<IActionResult> GetAllTitles()
        {
            var titles = await _commonRepository.GetAllTitles();
            if (titles != null)
                return Ok(titles);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetAllBloodGroups")]
        public async Task<IActionResult> GetAllBloodGroups()
        {
            var bloodGroups = await _commonRepository.GetAllBloodGroups();
            if (bloodGroups != null)
                return Ok(bloodGroups);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetAllMaritalStatus")]
        public async Task<IActionResult> GetAllMaritalStatus()
        {
            var maritalStatus = await _commonRepository.GetAllMaritalStatus();
            if (maritalStatus != null)
                return Ok(maritalStatus);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("GetAllPhoneTypes")]
        public async Task<IActionResult> GetAllPhoneTypes(int languageID=1)
        {
            var phoneTypes = await _commonRepository.GetAllPhoneTypes(languageID);
            if (phoneTypes != null)
                return Ok(phoneTypes);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("GetAllCountries")]
        public async Task<IActionResult> GetAllCountries(int languageID = 1)
        {
            var countryList = await _commonRepository.GetAllCountries(languageID);
            if (countryList != null)
                return Ok(countryList);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("GetAllDegrees")]
        public async Task<IActionResult> GetAllDegrees(int languageID = 1)
        {
            var degreesList = await _commonRepository.GetAllDegrees(languageID);
            if (degreesList != null)
                return Ok(degreesList);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("GetAllCourseTypes")]
        public async Task<IActionResult> GetAllCourseTypes(int languageID = 1)
        {
            var courseTypeList = await _commonRepository.GetAllCourseTypes(languageID);
            if (courseTypeList != null)
                return Ok(courseTypeList);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetAllAddressType")]
        public async Task<IActionResult> GetAllAddressType(int languageID = 1)
        {
            var countryList = await _commonRepository.GetAllAddressTypes(languageID);
            if (countryList != null)
                return Ok(countryList);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("getAllAddressTypes")]
        public async Task<IActionResult> GetAllAddressTypes()
        {
            var adTypes = await _commonRepository.GetAllAddressTypes();
            if (adTypes != null)
                return Ok(adTypes);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("getAllDependantTypeList")]
        public async Task<IActionResult> GetAllDependantTypeList()
        {
            var dependants = await _commonRepository.GetAllDependantTypeList();
            if (dependants != null)
                return Ok(dependants);
            else
                return NotFound();
        }


        [HttpGet]
        [Route("YesorNoDropDownList")]
        public IActionResult YesorNoDropDownList()
        {
            //    List<DropDownListItem> yesorNoDropDownList = Language.Contains(SelectedLanguage) ? new List<DropDownListItem> {
            //        new DropDownListItem { Text = "Yes", Value = "1" },
            //        new DropDownListItem { Text = "No", Value = "0" }


            //} :

            //var list = new List<DropDownListItem> {
            //        new DropDownListItem { Text = "فعلا", Value ="1" },
            //        new DropDownListItem { Text = "لا", Value = "0" }
            //    };

            var list = new List<DropDownListItem> {
                    new DropDownListItem { Text = "Yes", Value ="1" },
                    new DropDownListItem { Text = "No", Value = "0" }
                };
            return Ok(list);
        }

        [HttpGet]
        [Route("GetAllDocumentTypes")]
        public async Task<IActionResult> GetAllDocumentTypes()
        {
            var documentTypes = await _commonRepository.GetAllDocumentTypes();
            if (documentTypes != null)
                return Ok(documentTypes);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetAllDocumentOwners")]
        public async Task<IActionResult> GetAllDocumentOwners()
        {
            var documentOwners = await _commonRepository.GetAllDocumentOwners();
            if (documentOwners != null)
                return Ok(documentOwners);
            else
                return NotFound();
        }

    }
}
