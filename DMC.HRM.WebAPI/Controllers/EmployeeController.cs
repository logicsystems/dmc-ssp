﻿using DMC.HRM.Entities;
using DMC.HRM.Entities.Models;
using DMC.HRM.Entities.ResponseModels;
using DMC.HRM.Repository.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMC.HRM.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private IEmployeeRepository _employeeRepository;
        private ICommonRepository _commonRepository;

        public EmployeeController(IEmployeeRepository employeeRepository, ICommonRepository commonRepository)
        {
            _employeeRepository = employeeRepository;
            _commonRepository = commonRepository;
        }

        #region EmployeePersonalInfo

        [HttpPost]
        [Route("GetPersonalInfo")]
        public async Task<IActionResult> GetPersonalInfo(HrmDefUser hrmDefUser)
        {
            var user = await _employeeRepository.GetPersonalInfo(hrmDefUser);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }
        //ALI
        [HttpGet]
        [Route("GetEmployeeAddressByID")]
        public async Task<IActionResult> GetEmployeeAddressByID(long employeeAddressId)
        {
            var user = await _employeeRepository.GetEmployeeAddressByID(employeeAddressId);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }
        //ALI
        [HttpGet]
        [Route("GetEmployeeAddressList")]
        public async Task<IActionResult> GetEmployeeAddressList(long employeeID, string language)
        {
            var addressList = await _employeeRepository.GetEmployeeAddressList(employeeID, language);
            if (addressList != null)
                return Ok(addressList);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetEmployeeByID")]
        public async Task<IActionResult> GetEmployeeByID(long employeeID)
        {
            var user = await _employeeRepository.GetEmployeeByID(employeeID);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("GetEmployeePhoneByID")]
        public async Task<IActionResult> GetEmployeePhoneByID(long phoneID,int languageID=1)
        {
            var user = await _employeeRepository.GetEmployeePhoneByID(phoneID, languageID);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("GetEmployeeEducationByID")]
        public async Task<IActionResult> GetEmployeeEducationByID(long educationId, int languageID=1)
        {
            var user = await _employeeRepository.GetEmployeeEducationByID(educationId, languageID);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("GetEmployeePhoneList")]
        public async Task<IActionResult> GetEmployeePhoneList(long employeeID, int languageID = 1)
        {
            var employeePhoneList = await _employeeRepository.GetEmployeePhoneList(employeeID, languageID);
            if (employeePhoneList != null)
                return Ok(employeePhoneList);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("GetEmployeeEducationList")]
        public async Task<IActionResult> GetEmployeeEducationList(long employeeID, int languageID = 1)
        {
            var employeeEducationList = await _employeeRepository.GetEmployeeEducationList(employeeID, languageID);
            if (employeeEducationList != null)
                return Ok(employeeEducationList);
            else
                return NotFound();
        }

        [HttpPost]
        [Route("UpdatePersonalInfo")]
        public async Task<IActionResult> UpdatePersonalInfo(HrmTranPersonalInfo hrmTranPersonalInfo)
        {
            var status = await _employeeRepository.UpdatePersonalInfo(hrmTranPersonalInfo);
            if (status == 1)
                return Ok();
            else
                return NotFound();
        }
        [HttpPost]
        [Route("UpdateEmployeePhoneInfo")]
        public async Task<IActionResult> UpdateEmployeePhoneInfo(HrmTranEmployeePhoneModel hrmTranEmployeePhoneModel)
        {
            var status = await _employeeRepository.UpdateEmployeePhoneInfo(hrmTranEmployeePhoneModel);
            if (status == 1)
                return Ok(status);
            else
                return NotFound();
        }
        [HttpPost]
        [Route("UpdateEmployeeEducationInfo")]
        public async Task<IActionResult> UpdateEmployeeEducationInfo(EducationInfoModel educationInfoModel)
        {
            var status = await _employeeRepository.UpdateEmployeeEducationInfo(educationInfoModel);
            if (status == 1)
                return Ok(status);
            else
                return NotFound();
        }

        [HttpPost]
        [Route("UpdateEmployeeAddress")]
        public async Task<IActionResult> UpdateEmployeeAddress(HrmTranEmployeeAddressModel hrmTranEmployeeAddressModel)
        {
            var status = await _employeeRepository.UpdateEmployeeAddress(hrmTranEmployeeAddressModel);
            if (status == 1)
                return Ok(status);
            else
                return NotFound();
        }
        
        #endregion

        #region EmployeeContactInfo

        [HttpGet]
        [Route("GetEmployeeContactInfo")]
        public async Task<IActionResult> GetEmployeeContactInfo(long employeeID)
        {
            var employeeContactInfo = await _employeeRepository.GetEmployeeContactInfo(employeeID);
            if (employeeContactInfo != null)
                return Ok(employeeContactInfo);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetEmployeeMapToContactInfo")]
        public async Task<IActionResult> GetEmployeeMapToContactInfo(long employeeID)
        {
            var employeeMapToContactInfo = await _employeeRepository.GetEmployeeMapToContactInfo(employeeID);
            if (employeeMapToContactInfo != null)
                return Ok(employeeMapToContactInfo);
            else
                return NotFound();
        }

        [HttpPost]
        [Route("SaveOrUpdateContactInfo")]
        public async Task<IActionResult> SaveOrUpdateContactInfo(HrmTranContactInfo hrmTranContactInfo)
        {
            var status = 0;
            var employeeContactInfo = await _employeeRepository.GetEmployeeContactInfo(hrmTranContactInfo.EmployeeId.Value);
            if (employeeContactInfo != null)
                status = await _employeeRepository.UpdateContactInfo(hrmTranContactInfo);
            else
                status = await _employeeRepository.SaveContactInfo(hrmTranContactInfo);

            if (status == 1)
                return Ok(status);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetEmployeeMapToContactInfoByID")]
        public async Task<IActionResult> GetEmployeeMapToContactInfoByID(long employeeID, long MapContactInfoId)
        {
            var employeeMapToContactInfoByID = await _employeeRepository.GetEmployeeMapToContactInfoByID(employeeID, MapContactInfoId);
            if (employeeMapToContactInfoByID != null)
                return Ok(employeeMapToContactInfoByID);
            else
                return NotFound();
        }

        [HttpPost]
        [Route("SaveOrUpdateMapToContactInfo")]
        public async Task<IActionResult> SaveOrUpdateMapToContactInfo(HrmTranMapToContactInfo hrmTranMapToContactInfo)
        {
            var status = 0;
            var employeeMapToContactInfo = await _employeeRepository.GetEmployeeMapToContactInfoByID(hrmTranMapToContactInfo.EmployeeId.Value, hrmTranMapToContactInfo.MapContactInfoId);
            if (employeeMapToContactInfo != null)
                status = await _employeeRepository.UpdateEmployeeMapToContactInfo(hrmTranMapToContactInfo);
            else
                status = await _employeeRepository.SaveEmployeeMapToContactInfo(hrmTranMapToContactInfo);

            if (status == 1)
                return Ok(status);
            else
                return NotFound();
        }

        #endregion

        #region EmployeeDependant

        [HttpGet]
        [Route("GetHrmTranEmployeeDependantData")]
        public async Task<IActionResult> GetHrmTranEmployeeDependantData(long employeeID, string language)
        {
            var user = await _employeeRepository.GetHrmTranEmployeeDependantData(employeeID, language);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetHrmTranEmployeeDependantDataById")]
        public async Task<IActionResult> GetHrmTranEmployeeDependantDataById(long id)
        {
            var user = await _employeeRepository.GetHrmTranEmployeeDependantDataById(id);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }

        [HttpPost]
        [Route("AddUpdateEmployeeDependant")]
        public async Task<IActionResult> AddUpdateEmployeeDependant(HrmTranEmployeeDependantModel hrmTranPersonalInfo)
        {
            var status = await _employeeRepository.AddUpdateEmployeeDependant(hrmTranPersonalInfo);
            if (status == 1)
                return Ok(status);
            else
                return NotFound();
        }

        #endregion

        #region EmployeeDocumentsInfo

        [HttpGet]
        [Route("GetEmployeeDocumentsInfo")]
        public async Task<IActionResult> GetEmployeeDocumentsInfo(long employeeID)
        {
            var employeeDocumentsInfo = await _employeeRepository.GetEmployeeDocumentsInfo(employeeID);

            #region Not required handled in UI

            //var documentTypes = await _commonRepository.GetAllDocumentTypes();
            //var documentOwners = await _commonRepository.GetAllDocumentOwners();

            //var list = (from emp in employeeDocumentsInfo
            //            join doc in documentTypes on emp.DocumentTypeId equals doc.DocumentTypeId
            //            join own in documentOwners on emp.DocumentOwnerId equals own.DocumentOwnerId
            //            where emp.EmployeeId == employeeID
            //            select new HrmTranDocumentsInfoModel
            //            {
            //                EmployeeId = emp.EmployeeId,
            //                DocumentId = emp.DocumentId,
            //                DocumentTypeId = emp.DocumentTypeId,
            //                DocumentTypeNameEn = doc.DocumentTypeNameEn,
            //                DocumentTypeNameAr = doc.DocumentTypeNameAr,
            //                DocumentNumber = emp.DocumentNumber,
            //                NameInDocument = emp.NameInDocument,
            //                ExpiryDateAr = emp.ExpiryDateAr,
            //                ExpiryDateEn = emp.ExpiryDateEn,
            //                DocumentOwnerId = emp.DocumentOwnerId,
            //                DocumentOwnerNameEn = own.DocumentOwnerNameEn,
            //                DocumentOwnerNameAr = own.DocumentOwnerNameAr
            //            }).ToList();

            #endregion

            if (employeeDocumentsInfo != null)
                return Ok(employeeDocumentsInfo);
            else
                return NotFound();
        }

        [HttpGet]
        [Route("GetEmployeeDocumentsInfoByID")]
        public async Task<IActionResult> GetEmployeeDocumentsInfoByID(long employeeID, long documentID)
        {
            var employeeDocumentsInfoByID = await _employeeRepository.GetEmployeeDocumentsInfoByID(employeeID, documentID);
            if (employeeDocumentsInfoByID != null)
                return Ok(employeeDocumentsInfoByID);
            else
                return NotFound();
        }

        [HttpPost]
        [Route("SaveOrUpdateEmployeeDocumentsInfo")]
        public async Task<IActionResult> SaveOrUpdateEmployeeDocumentsInfo(HrmTranDocumentsInfo hrmTranDocumentsInfo)
        {
            var status = 0;
            var employeeDocumentsInfoByID = await _employeeRepository.GetEmployeeDocumentsInfoByID(hrmTranDocumentsInfo.EmployeeId.Value, hrmTranDocumentsInfo.DocumentId);
            if (employeeDocumentsInfoByID != null)
                status = await _employeeRepository.UpdateEmployeeDocumentsInfo(hrmTranDocumentsInfo);
            else
                status = await _employeeRepository.SaveEmployeeDocumentsInfo(hrmTranDocumentsInfo);

            if (status == 1)
                return Ok(status);
            else
                return NotFound();
        }

        #endregion
    }
}
